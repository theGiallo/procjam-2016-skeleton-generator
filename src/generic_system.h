#ifndef GENERIC_SYSTEM_H
#define GENERIC_SYSTEM_H 1

#include "basic_types.h"

#define SYS_API_RET_IS_ERROR(r) ((r)<0)

#if DEBUG
#define DEBUG_ONLY(e) e
#else
#define DEBUG_ONLY(e)
#endif

struct
Sys_GL_Version
{
	s32 minor, major;
	bool es;
};

enum
Sys_Screen_Mode : u8
{
	SYS_SCREEN_MODE_WINDOWED           = 0x0,
	SYS_SCREEN_MODE_FULLSCREEN,
	SYS_SCREEN_MODE_FULLSCREEN_DESKTOP,
	// ---
	SYS_SCREEN_MODE_COUNT
};

struct
Sys_Thread;

typedef s32 Sys_Thread_Function( void * data );

// TODO(theGiallo, 2016-02-27): make all sys api functions that have fail cases to return a s32?

#define SYS_API_GET_ERROR_NAME( name ) const char * name( s32 error )
typedef SYS_API_GET_ERROR_NAME( Sys_API_Get_Error_Name );
#define SYS_API_GET_ERROR_DESCRIPTION( name ) const char * name( s32 error )
typedef SYS_API_GET_ERROR_DESCRIPTION( Sys_API_Get_Error_Description );
#define SYS_API_SET_VSYNC( name ) bool name( bool vsync )
typedef SYS_API_SET_VSYNC( Sys_API_Set_VSync );
#define SYS_API_SET_SCREEN_MODE( name ) bool name( s32 window_id, Sys_Screen_Mode mode )
typedef SYS_API_SET_SCREEN_MODE( Sys_API_Set_Screen_Mode );
#define SYS_API_GET_SCREEN_MODE( name ) Sys_Screen_Mode name( s32 window_id )
typedef SYS_API_GET_SCREEN_MODE( Sys_API_Get_Screen_Mode );
#define SYS_API_GET_WINDOW_SIZE( name ) void name( s32 window_id, \
                                                   u32 * width, u32 * height )
typedef SYS_API_GET_WINDOW_SIZE( Sys_API_Get_Window_Size );
#define SYS_API_SET_WINDOW_SIZE( name ) void name( s32 window_id, \
                                                   u32 width, u32 height )
typedef SYS_API_SET_WINDOW_SIZE( Sys_API_Set_Window_Size );
#define SYS_API_CREATE_WINDOW( name ) s32 name( const char * win_name,       \
                                                Sys_GL_Version gl_version,   \
                                                bool share_context,          \
                                                bool vsync,                  \
                                                Sys_Screen_Mode screen_mode, \
                                                bool max_screen_size,        \
                                                u32 width, u32 height )
typedef SYS_API_CREATE_WINDOW( Sys_API_Create_Window );
#define SYS_API_MAKE_WINDOW_CURRENT( name ) s32 name( s32 window_id )
typedef SYS_API_MAKE_WINDOW_CURRENT( Sys_API_Make_Window_Current );
#define SYS_API_SWAP_WINDOW( name ) void name( s32 window_id )
typedef SYS_API_SWAP_WINDOW( Sys_API_Swap_Window );
#define SYS_API_CLOSE_WINDOW( name ) void name( s32 window_id )
typedef SYS_API_CLOSE_WINDOW( Sys_API_Close_Window );
#define SYS_API_EXIT( name ) void name( )
typedef SYS_API_EXIT( Sys_API_Exit );
#define SYS_API_GET_FILE_MODIFIED_TIME( name ) bool name( const char * file_path, Time * modified_time )
typedef SYS_API_GET_FILE_MODIFIED_TIME( Sys_API_Get_File_Modified_Time );
#define SYS_API_GET_FILE_SIZE( name ) s64 name( const char * file_path )
typedef SYS_API_GET_FILE_SIZE( Sys_API_Get_File_Size );
#define SYS_API_READ_ENTIRE_FILE( name ) s64 name( const char * file_path, void * mem, s64 mem_size )
typedef SYS_API_READ_ENTIRE_FILE( Sys_API_Read_Entire_File );
#define SYS_API_THREAD_CREATE( f_name ) Sys_Thread * f_name( Sys_Thread_Function fun, const char * name, void * data )
typedef SYS_API_THREAD_CREATE( Sys_API_Thread_Create );
#define SYS_API_THREAD_WAIT( name ) void name( Sys_Thread * thread, s32 * status )
typedef SYS_API_THREAD_WAIT( Sys_API_Thread_Wait );
#define SYS_API_THREAD_DETACH( name ) void name( Sys_Thread * thread )
typedef SYS_API_THREAD_DETACH( Sys_API_Thread_Detach );
#define SYS_API_THREAD_ID( name ) u32 name( )
typedef SYS_API_THREAD_ID( Sys_API_Thread_ID );
#define SYS_API_WARP_MOUSE_IN_WINDOW( name ) void name( s32 window_id, u32 px_pos_x, u32 px_pos_y )
typedef SYS_API_WARP_MOUSE_IN_WINDOW( Sys_API_Warp_Mouse_In_Window );
#define SYS_API_GET_KEYBOARD_STATE( name ) const u8 * name( s32 * bytes_size )
typedef SYS_API_GET_KEYBOARD_STATE( Sys_API_Get_Keyboard_State );

struct
System_API
{
	Sys_API_Get_Error_Name         * get_error_name;
	Sys_API_Get_Error_Description  * get_error_description;
	Sys_API_Set_VSync              * set_vsync;
	Sys_API_Set_Screen_Mode        * set_screen_mode;
	Sys_API_Get_Screen_Mode        * get_screen_mode;
	Sys_API_Get_Window_Size        * get_window_size;
	Sys_API_Set_Window_Size        * set_window_size;
	Sys_API_Create_Window          * create_window;
	Sys_API_Make_Window_Current    * make_window_current;
	Sys_API_Swap_Window            * swap_window;
	Sys_API_Close_Window           * close_window;
	Sys_API_Exit                   * exit;
	Sys_API_Get_File_Modified_Time * get_file_modified_time;
	Sys_API_Get_File_Size          * get_file_size;
	Sys_API_Read_Entire_File       * read_entire_file;
	Sys_API_Thread_Create          * thread_create;
	Sys_API_Thread_Wait            * thread_wait;
	Sys_API_Thread_Detach          * thread_detach;
	Sys_API_Thread_ID              * thread_id;
	Sys_API_Warp_Mouse_In_Window   * warp_mouse_in_window;
	Sys_API_Get_Keyboard_State     * get_keyboard_state;
};

#define SYS_API_UNKNOWN_THREAD_ID 255
#define SYS_API_MAIN_THREAD_ID    0

enum
Event_Type : u32 // NOTE(theGiallo): for padding matters
{
	EVENT_WINDOW,
	EVENT_KEYBOARD,
	EVENT_MOUSE,
	EVENT_PAD,
	EVENT_GAME_SYSTEM,
	// ---
	EVENT_TYPE_COUNT,
};

struct
Event_Common
{
	Event_Type type;
	Time time_fired;
};

#define WINDOW_EVENT_EXPAND_AS_ENUM(a) a,
#define WINDOW_EVENT_EXPAND_AS_NAME_TABLE(a) LPR_TOSTRING(a),
#define WINDOW_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_WINDOW_CLOSE ) \
	ENTRY( EVENT_WINDOW_RESIZED ) \
	ENTRY( EVENT_WINDOW_SHOWN ) \
	ENTRY( EVENT_WINDOW_HIDDEN ) \
	ENTRY( EVENT_WINDOW_MOVED ) \
	ENTRY( EVENT_WINDOW_EXPOSED ) \
	ENTRY( EVENT_WINDOW_SIZE_CHANGED ) \
	ENTRY( EVENT_WINDOW_MINIMIZED ) \
	ENTRY( EVENT_WINDOW_MAXIMIZED ) \
	ENTRY( EVENT_WINDOW_RESTORED ) \
	ENTRY( EVENT_WINDOW_ENTER ) \
	ENTRY( EVENT_WINDOW_LEAVE ) \
	ENTRY( EVENT_WINDOW_FOCUS_GAINED ) \
	ENTRY( EVENT_WINDOW_FOCUS_LOST ) \
	


#if 0
#define WINDOW_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_WINDOW_CLOSE,       SDL_WINDOWEVENT_CLOSE        ) \
	ENTRY( EVENT_WINDOW_RESIZED,      SDL_WINDOWEVENT_RESIZED      ) \
	ENTRY( EVENT_WINDOW_SHOWN,        SDL_WINDOWEVENT_SHOWN        ) \
	ENTRY( EVENT_WINDOW_HIDDEN,       SDL_WINDOWEVENT_HIDDEN       ) \
	ENTRY( EVENT_WINDOW_MOVED,        SDL_WINDOWEVENT_MOVED        ) \
	ENTRY( EVENT_WINDOW_EXPOSED,      SDL_WINDOWEVENT_EXPOSED      ) \
	ENTRY( EVENT_WINDOW_SIZE_CHANGED, SDL_WINDOWEVENT_SIZE_CHANGED ) \
	ENTRY( EVENT_WINDOW_MINIMIZED,    SDL_WINDOWEVENT_MINIMIZED    ) \
	ENTRY( EVENT_WINDOW_MAXIMIZED,    SDL_WINDOWEVENT_MAXIMIZED    ) \
	ENTRY( EVENT_WINDOW_RESTORED,     SDL_WINDOWEVENT_RESTORED     ) \
	ENTRY( EVENT_WINDOW_ENTER,        SDL_WINDOWEVENT_ENTER        ) \
	ENTRY( EVENT_WINDOW_LEAVE,        SDL_WINDOWEVENT_LEAVE        ) \
	ENTRY( EVENT_WINDOW_FOCUS_GAINED, SDL_WINDOWEVENT_FOCUS_GAINED ) \
	ENTRY( EVENT_WINDOW_FOCUS_LOST,   SDL_WINDOWEVENT_FOCUS_LOST   ) \


#endif

enum
Event_Window_Type : u8
{
	WINDOW_EVENT_TABLE( WINDOW_EVENT_EXPAND_AS_ENUM )
	// ---
	EVENT_WINDOW_COUNT,
};

struct
Event_Window
{
	Event_Common common;
	s32 window_id;
	Event_Window_Type type;
	u8 _padding1;
	u8 _padding2;
	u8 _padding3;
	union
	{
		struct
		{
			s32 x, y;
		};
		struct
		{
			u32 w,h;
		};
	};
};


#define KEYBOARD_EVENT_EXPAND_AS_ENUM(a) a,
#define KEYBOARD_EVENT_EXPAND_AS_NAME_TABLE(a) LPR_TOSTRING(a),
#define KEYBOARD_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_KEYBOARD_KEY_PRESSED ) \
	ENTRY( EVENT_KEYBOARD_KEY_RELEASED ) \


enum
Event_Keyboard_Type : u8
{
	KEYBOARD_EVENT_TABLE( KEYBOARD_EVENT_EXPAND_AS_ENUM )
	// ---
	EVENT_KEYBOARD_COUNT,
};

enum
Key_Mod : u16
{
	KEY_MOD_NONE     = 0x0,
	KEY_MOD_LSHIFT   = 0x1 << 0,
	KEY_MOD_RSHIFT   = 0x1 << 1,
	KEY_MOD_LCTRL    = 0x1 << 6,
	KEY_MOD_RCTRL    = 0x1 << 7,
	KEY_MOD_LALT     = 0x1 << 8,
	KEY_MOD_RALT     = 0x1 << 9,
	KEY_MOD_LGUI     = 0x1 << 10,
	KEY_MOD_RGUI     = 0x1 << 11,
	KEY_MOD_NUM      = 0x1 << 12,
	KEY_MOD_CAPS     = 0x1 << 13,
	KEY_MOD_MODE     = 0x1 << 14,
	KEY_MOD_RESERVED = 0x1 << 15,
};

#define KEY_MOD_CTRL   (KEY_MOD_LCTRL|KEY_MOD_RCTRL)
#define KEY_MOD_SHIFT  (KEY_MOD_LSHIFT|KEY_MOD_RSHIFT)
#define KEY_MOD_ALT    (KEY_MOD_LALT|KEY_MOD_RALT)
#define KEY_MOD_GUI    (KEY_MOD_LGUI|KEY_MOD_RGUI)

enum
Phys_Key : s32
{
	PK_UNKNOWN = 0,

	/**
	 *  \name Usage page 0x07
	 *
	 *  These values are from usage page 0x07 (USB keyboard page).
	 */
	/* @{ */

	PK_A = 4,
	PK_B = 5,
	PK_C = 6,
	PK_D = 7,
	PK_E = 8,
	PK_F = 9,
	PK_G = 10,
	PK_H = 11,
	PK_I = 12,
	PK_J = 13,
	PK_K = 14,
	PK_L = 15,
	PK_M = 16,
	PK_N = 17,
	PK_O = 18,
	PK_P = 19,
	PK_Q = 20,
	PK_R = 21,
	PK_S = 22,
	PK_T = 23,
	PK_U = 24,
	PK_V = 25,
	PK_W = 26,
	PK_X = 27,
	PK_Y = 28,
	PK_Z = 29,

	PK_1 = 30,
	PK_2 = 31,
	PK_3 = 32,
	PK_4 = 33,
	PK_5 = 34,
	PK_6 = 35,
	PK_7 = 36,
	PK_8 = 37,
	PK_9 = 38,
	PK_0 = 39,

	PK_RETURN = 40,
	PK_ESCAPE = 41,
	PK_BACKSPACE = 42,
	PK_TAB = 43,
	PK_SPACE = 44,

	PK_MINUS = 45,
	PK_EQUALS = 46,
	PK_LEFTBRACKET = 47,
	PK_RIGHTBRACKET = 48,
	PK_BACKSLASH = 49, /**< Located at the lower left of the return
	                    *   key on ISO keyboards and at the right end
	                    *   of the QWERTY row on ANSI keyboards.
	                    *   Produces REVERSE SOLIDUS (backslash) and
	                    *   VERTICAL LINE in a US layout, REVERSE
	                    *   SOLIDUS and VERTICAL LINE in a UK Mac
	                    *   layout, NUMBER SIGN and TILDE in a UK
	                    *   Windows layout, DOLLAR SIGN and POUND SIGN
	                    *   in a Swiss German layout, NUMBER SIGN and
	                    *   APOSTROPHE in a German layout, GRAVE
	                    *   ACCENT and POUND SIGN in a French Mac
	                    *   layout, and ASTERISK and MICRO SIGN in a
	                    *   French Windows layout.
	                    */
	PK_NONUSHASH = 50, /**< ISO USB keyboards actually use this code
	                    *   instead of 49 for the same key, but all
	                    *   OSes I've seen treat the two codes
	                    *   identically. So, as an implementor, unless
	                    *   your keyboard generates both of those
	                    *   codes and your OS treats them differently,
	                    *   you should generate PK_BACKSLASH
	                    *   instead of this code. As a user, you
	                    *   should not rely on this code because SDL
	                    *   will never generate it with most (all?)
	                    *   keyboards.
	                    */
	PK_SEMICOLON = 51,
	PK_APOSTROPHE = 52,
	PK_GRAVE = 53, /**< Located in the top left corner (on both ANSI
	                *   and ISO keyboards). Produces GRAVE ACCENT and
	                *   TILDE in a US Windows layout and in US and UK
	                *   Mac layouts on ANSI keyboards, GRAVE ACCENT
	                *   and NOT SIGN in a UK Windows layout, SECTION
	                *   SIGN and PLUS-MINUS SIGN in US and UK Mac
	                *   layouts on ISO keyboards, SECTION SIGN and
	                *   DEGREE SIGN in a Swiss German layout (Mac:
	                *   only on ISO keyboards), CIRCUMFLEX ACCENT and
	                *   DEGREE SIGN in a German layout (Mac: only on
	                *   ISO keyboards), SUPERSCRIPT TWO and TILDE in a
	                *   French Windows layout, COMMERCIAL AT and
	                *   NUMBER SIGN in a French Mac layout on ISO
	                *   keyboards, and LESS-THAN SIGN and GREATER-THAN
	                *   SIGN in a Swiss German, German, or French Mac
	                *   layout on ANSI keyboards.
	                */
	PK_COMMA = 54,
	PK_PERIOD = 55,
	PK_SLASH = 56,

	PK_CAPSLOCK = 57,

	PK_F1 = 58,
	PK_F2 = 59,
	PK_F3 = 60,
	PK_F4 = 61,
	PK_F5 = 62,
	PK_F6 = 63,
	PK_F7 = 64,
	PK_F8 = 65,
	PK_F9 = 66,
	PK_F10 = 67,
	PK_F11 = 68,
	PK_F12 = 69,

	PK_PRINTSCREEN = 70,
	PK_SCROLLLOCK = 71,
	PK_PAUSE = 72,
	PK_INSERT = 73, /**< insert on PC, help on some Mac keyboards (but
	                 *does send code 73, not 117) */
	PK_HOME = 74,
	PK_PAGEUP = 75,
	PK_DELETE = 76,
	PK_END = 77,
	PK_PAGEDOWN = 78,
	PK_RIGHT = 79,
	PK_LEFT = 80,
	PK_DOWN = 81,
	PK_UP = 82,

	PK_NUMLOCKCLEAR = 83, /**< num lock on PC, clear on Mac keyboards */
	PK_KP_DIVIDE = 84,
	PK_KP_MULTIPLY = 85,
	PK_KP_MINUS = 86,
	PK_KP_PLUS = 87,
	PK_KP_ENTER = 88,
	PK_KP_1 = 89,
	PK_KP_2 = 90,
	PK_KP_3 = 91,
	PK_KP_4 = 92,
	PK_KP_5 = 93,
	PK_KP_6 = 94,
	PK_KP_7 = 95,
	PK_KP_8 = 96,
	PK_KP_9 = 97,
	PK_KP_0 = 98,
	PK_KP_PERIOD = 99,

	PK_NONUSBACKSLASH = 100, /**< This is the additional key that ISO
	                          *   keyboards have over ANSI ones,
	                          *   located between left shift and Y.
	                          *   Produces GRAVE ACCENT and TILDE in a
	                          *   US or UK Mac layout, REVERSE SOLIDUS
	                          *   (backslash) and VERTICAL LINE in a
	                          *   US or UK Windows layout, and
	                          *   LESS-THAN SIGN and GREATER-THAN SIGN
	                          *   in a Swiss German, German, or French
	                          *   layout. */
	PK_APPLICATION = 101, /**< windows contextual menu, compose */
	PK_POWER = 102, /**< The USB document says this is a status flag,
	                           *   not a physical key - but some Mac keyboards
	                           *   do have a power key. */
	PK_KP_EQUALS = 103,
	PK_F13 = 104,
	PK_F14 = 105,
	PK_F15 = 106,
	PK_F16 = 107,
	PK_F17 = 108,
	PK_F18 = 109,
	PK_F19 = 110,
	PK_F20 = 111,
	PK_F21 = 112,
	PK_F22 = 113,
	PK_F23 = 114,
	PK_F24 = 115,
	PK_EXECUTE = 116,
	PK_HELP = 117,
	PK_MENU = 118,
	PK_SELECT = 119,
	PK_STOP = 120,
	PK_AGAIN = 121,   /**< redo */
	PK_UNDO = 122,
	PK_CUT = 123,
	PK_COPY = 124,
	PK_PASTE = 125,
	PK_FIND = 126,
	PK_MUTE = 127,
	PK_VOLUMEUP = 128,
	PK_VOLUMEDOWN = 129,
	/* not sure whether there's a reason to enable these */
	/*     PK_LOCKINGCAPSLOCK = 130,  */
	/*     PK_LOCKINGNUMLOCK = 131, */
	/*     PK_LOCKINGSCROLLLOCK = 132, */
	PK_KP_COMMA = 133,
	PK_KP_EQUALSAS400 = 134,

	PK_INTERNATIONAL1 = 135, /**< used on Asian keyboards, see
	                                      footnotes in USB doc */
	PK_INTERNATIONAL2 = 136,
	PK_INTERNATIONAL3 = 137, /**< Yen */
	PK_INTERNATIONAL4 = 138,
	PK_INTERNATIONAL5 = 139,
	PK_INTERNATIONAL6 = 140,
	PK_INTERNATIONAL7 = 141,
	PK_INTERNATIONAL8 = 142,
	PK_INTERNATIONAL9 = 143,
	PK_LANG1 = 144, /**< Hangul/English toggle */
	PK_LANG2 = 145, /**< Hanja conversion */
	PK_LANG3 = 146, /**< Katakana */
	PK_LANG4 = 147, /**< Hiragana */
	PK_LANG5 = 148, /**< Zenkaku/Hankaku */
	PK_LANG6 = 149, /**< reserved */
	PK_LANG7 = 150, /**< reserved */
	PK_LANG8 = 151, /**< reserved */
	PK_LANG9 = 152, /**< reserved */

	PK_ALTERASE = 153, /**< Erase-Eaze */
	PK_SYSREQ = 154,
	PK_CANCEL = 155,
	PK_CLEAR = 156,
	PK_PRIOR = 157,
	PK_RETURN2 = 158,
	PK_SEPARATOR = 159,
	PK_OUT = 160,
	PK_OPER = 161,
	PK_CLEARAGAIN = 162,
	PK_CRSEL = 163,
	PK_EXSEL = 164,

	PK_KP_00 = 176,
	PK_KP_000 = 177,
	PK_THOUSANDSSEPARATOR = 178,
	PK_DECIMALSEPARATOR = 179,
	PK_CURRENCYUNIT = 180,
	PK_CURRENCYSUBUNIT = 181,
	PK_KP_LEFTPAREN = 182,
	PK_KP_RIGHTPAREN = 183,
	PK_KP_LEFTBRACE = 184,
	PK_KP_RIGHTBRACE = 185,
	PK_KP_TAB = 186,
	PK_KP_BACKSPACE = 187,
	PK_KP_A = 188,
	PK_KP_B = 189,
	PK_KP_C = 190,
	PK_KP_D = 191,
	PK_KP_E = 192,
	PK_KP_F = 193,
	PK_KP_XOR = 194,
	PK_KP_POWER = 195,
	PK_KP_PERCENT = 196,
	PK_KP_LESS = 197,
	PK_KP_GREATER = 198,
	PK_KP_AMPERSAND = 199,
	PK_KP_DBLAMPERSAND = 200,
	PK_KP_VERTICALBAR = 201,
	PK_KP_DBLVERTICALBAR = 202,
	PK_KP_COLON = 203,
	PK_KP_HASH = 204,
	PK_KP_SPACE = 205,
	PK_KP_AT = 206,
	PK_KP_EXCLAM = 207,
	PK_KP_MEMSTORE = 208,
	PK_KP_MEMRECALL = 209,
	PK_KP_MEMCLEAR = 210,
	PK_KP_MEMADD = 211,
	PK_KP_MEMSUBTRACT = 212,
	PK_KP_MEMMULTIPLY = 213,
	PK_KP_MEMDIVIDE = 214,
	PK_KP_PLUSMINUS = 215,
	PK_KP_CLEAR = 216,
	PK_KP_CLEARENTRY = 217,
	PK_KP_BINARY = 218,
	PK_KP_OCTAL = 219,
	PK_KP_DECIMAL = 220,
	PK_KP_HEXADECIMAL = 221,

	PK_LCTRL = 224,
	PK_LSHIFT = 225,
	PK_LALT = 226, /**< alt, option */
	PK_LGUI = 227, /**< windows, command (apple), meta */
	PK_RCTRL = 228,
	PK_RSHIFT = 229,
	PK_RALT = 230, /**< alt gr, option */
	PK_RGUI = 231, /**< windows, command (apple), meta */

	PK_MODE = 257,    /**< I'm not sure if this is really not covered
	                   *   by any of the above, but since there's a
	                   *   special KEY_MOD_MODE for it I'm adding it
	                   *   here */

	/* @} *//* Usage page 0x07 */

	/**
	 *  \name Usage page 0x0C
	 *
	 *  These values are mapped from usage page 0x0C (USB consumer page).
	 */
	/* @{ */

	PK_AUDIONEXT = 258,
	PK_AUDIOPREV = 259,
	PK_AUDIOSTOP = 260,
	PK_AUDIOPLAY = 261,
	PK_AUDIOMUTE = 262,
	PK_MEDIASELECT = 263,
	PK_WWW = 264,
	PK_MAIL = 265,
	PK_CALCULATOR = 266,
	PK_COMPUTER = 267,
	PK_AC_SEARCH = 268,
	PK_AC_HOME = 269,
	PK_AC_BACK = 270,
	PK_AC_FORWARD = 271,
	PK_AC_STOP = 272,
	PK_AC_REFRESH = 273,
	PK_AC_BOOKMARKS = 274,

	/* @} *//* Usage page 0x0C */

	/**
	 *  \name Walther keys
	 *
	 *  These are values that Christian Walther added (for mac keyboard?).
	 */
	/* @{ */

	PK_BRIGHTNESSDOWN = 275,
	PK_BRIGHTNESSUP = 276,
	PK_DISPLAYSWITCH = 277, /**< display mirroring/dual display
	                                   switch, video mode switch */
	PK_KBDILLUMTOGGLE = 278,
	PK_KBDILLUMDOWN = 279,
	PK_KBDILLUMUP = 280,
	PK_EJECT = 281,
	PK_SLEEP = 282,

	PK_APP1 = 283,
	PK_APP2 = 284,

	/* @} *//* Walther keys */

	/* Add any other keys here. */

	PK_COUNT = 512 /**< not a key, just marks the number of scancodes
	                    for array bounds */
};

#define PK_MASK (1<<30)
#define PK_TO_VK(pk) ( (pk) | PK_MASK )

enum
Virt_Key : s32
{
	VK_UNKNOWN = 0,

	VK_RETURN = '\r',
	VK_ESCAPE = '\033',
	VK_BACKSPACE = '\b',
	VK_TAB = '\t',
	VK_SPACE = ' ',
	VK_EXCLAIM = '!',
	VK_QUOTEDBL = '"',
	VK_HASH = '#',
	VK_PERCENT = '%',
	VK_DOLLAR = '$',
	VK_AMPERSAND = '&',
	VK_QUOTE = '\'',
	VK_LEFTPAREN = '(',
	VK_RIGHTPAREN = ')',
	VK_ASTERISK = '*',
	VK_PLUS = '+',
	VK_COMMA = ',',
	VK_MINUS = '-',
	VK_PERIOD = '.',
	VK_SLASH = '/',
	VK_0 = '0',
	VK_1 = '1',
	VK_2 = '2',
	VK_3 = '3',
	VK_4 = '4',
	VK_5 = '5',
	VK_6 = '6',
	VK_7 = '7',
	VK_8 = '8',
	VK_9 = '9',
	VK_COLON = ':',
	VK_SEMICOLON = ';',
	VK_LESS = '<',
	VK_EQUALS = '=',
	VK_GREATER = '>',
	VK_QUESTION = '?',
	VK_AT = '@',
	/*
		Skip uppercase letters
		*/
	VK_LEFTBRACKET = '[',
	VK_BACKSLASH = '\\',
	VK_RIGHTBRACKET = ']',
	VK_CARET = '^',
	VK_UNDERSCORE = '_',
	VK_BACKQUOTE = '`',
	VK_a = 'a',
	VK_b = 'b',
	VK_c = 'c',
	VK_d = 'd',
	VK_e = 'e',
	VK_f = 'f',
	VK_g = 'g',
	VK_h = 'h',
	VK_i = 'i',
	VK_j = 'j',
	VK_k = 'k',
	VK_l = 'l',
	VK_m = 'm',
	VK_n = 'n',
	VK_o = 'o',
	VK_p = 'p',
	VK_q = 'q',
	VK_r = 'r',
	VK_s = 's',
	VK_t = 't',
	VK_u = 'u',
	VK_v = 'v',
	VK_w = 'w',
	VK_x = 'x',
	VK_y = 'y',
	VK_z = 'z',

	VK_CAPSLOCK = PK_TO_VK(PK_CAPSLOCK),

	VK_F1 = PK_TO_VK(PK_F1),
	VK_F2 = PK_TO_VK(PK_F2),
	VK_F3 = PK_TO_VK(PK_F3),
	VK_F4 = PK_TO_VK(PK_F4),
	VK_F5 = PK_TO_VK(PK_F5),
	VK_F6 = PK_TO_VK(PK_F6),
	VK_F7 = PK_TO_VK(PK_F7),
	VK_F8 = PK_TO_VK(PK_F8),
	VK_F9 = PK_TO_VK(PK_F9),
	VK_F10 = PK_TO_VK(PK_F10),
	VK_F11 = PK_TO_VK(PK_F11),
	VK_F12 = PK_TO_VK(PK_F12),

	VK_PRINTSCREEN = PK_TO_VK(PK_PRINTSCREEN),
	VK_SCROLLLOCK = PK_TO_VK(PK_SCROLLLOCK),
	VK_PAUSE = PK_TO_VK(PK_PAUSE),
	VK_INSERT = PK_TO_VK(PK_INSERT),
	VK_HOME = PK_TO_VK(PK_HOME),
	VK_PAGEUP = PK_TO_VK(PK_PAGEUP),
	VK_DELETE = '\177',
	VK_END = PK_TO_VK(PK_END),
	VK_PAGEDOWN = PK_TO_VK(PK_PAGEDOWN),
	VK_RIGHT = PK_TO_VK(PK_RIGHT),
	VK_LEFT = PK_TO_VK(PK_LEFT),
	VK_DOWN = PK_TO_VK(PK_DOWN),
	VK_UP = PK_TO_VK(PK_UP),

	VK_NUMLOCKCLEAR = PK_TO_VK(PK_NUMLOCKCLEAR),
	VK_KP_DIVIDE = PK_TO_VK(PK_KP_DIVIDE),
	VK_KP_MULTIPLY = PK_TO_VK(PK_KP_MULTIPLY),
	VK_KP_MINUS = PK_TO_VK(PK_KP_MINUS),
	VK_KP_PLUS = PK_TO_VK(PK_KP_PLUS),
	VK_KP_ENTER = PK_TO_VK(PK_KP_ENTER),
	VK_KP_1 = PK_TO_VK(PK_KP_1),
	VK_KP_2 = PK_TO_VK(PK_KP_2),
	VK_KP_3 = PK_TO_VK(PK_KP_3),
	VK_KP_4 = PK_TO_VK(PK_KP_4),
	VK_KP_5 = PK_TO_VK(PK_KP_5),
	VK_KP_6 = PK_TO_VK(PK_KP_6),
	VK_KP_7 = PK_TO_VK(PK_KP_7),
	VK_KP_8 = PK_TO_VK(PK_KP_8),
	VK_KP_9 = PK_TO_VK(PK_KP_9),
	VK_KP_0 = PK_TO_VK(PK_KP_0),
	VK_KP_PERIOD = PK_TO_VK(PK_KP_PERIOD),

	VK_APPLICATION = PK_TO_VK(PK_APPLICATION),
	VK_POWER = PK_TO_VK(PK_POWER),
	VK_KP_EQUALS = PK_TO_VK(PK_KP_EQUALS),
	VK_F13 = PK_TO_VK(PK_F13),
	VK_F14 = PK_TO_VK(PK_F14),
	VK_F15 = PK_TO_VK(PK_F15),
	VK_F16 = PK_TO_VK(PK_F16),
	VK_F17 = PK_TO_VK(PK_F17),
	VK_F18 = PK_TO_VK(PK_F18),
	VK_F19 = PK_TO_VK(PK_F19),
	VK_F20 = PK_TO_VK(PK_F20),
	VK_F21 = PK_TO_VK(PK_F21),
	VK_F22 = PK_TO_VK(PK_F22),
	VK_F23 = PK_TO_VK(PK_F23),
	VK_F24 = PK_TO_VK(PK_F24),
	VK_EXECUTE = PK_TO_VK(PK_EXECUTE),
	VK_HELP = PK_TO_VK(PK_HELP),
	VK_MENU = PK_TO_VK(PK_MENU),
	VK_SELECT = PK_TO_VK(PK_SELECT),
	VK_STOP = PK_TO_VK(PK_STOP),
	VK_AGAIN = PK_TO_VK(PK_AGAIN),
	VK_UNDO = PK_TO_VK(PK_UNDO),
	VK_CUT = PK_TO_VK(PK_CUT),
	VK_COPY = PK_TO_VK(PK_COPY),
	VK_PASTE = PK_TO_VK(PK_PASTE),
	VK_FIND = PK_TO_VK(PK_FIND),
	VK_MUTE = PK_TO_VK(PK_MUTE),
	VK_VOLUMEUP = PK_TO_VK(PK_VOLUMEUP),
	VK_VOLUMEDOWN = PK_TO_VK(PK_VOLUMEDOWN),
	VK_KP_COMMA = PK_TO_VK(PK_KP_COMMA),
	VK_KP_EQUALSAS400 =
		PK_TO_VK(PK_KP_EQUALSAS400),

	VK_ALTERASE = PK_TO_VK(PK_ALTERASE),
	VK_SYSREQ = PK_TO_VK(PK_SYSREQ),
	VK_CANCEL = PK_TO_VK(PK_CANCEL),
	VK_CLEAR = PK_TO_VK(PK_CLEAR),
	VK_PRIOR = PK_TO_VK(PK_PRIOR),
	VK_RETURN2 = PK_TO_VK(PK_RETURN2),
	VK_SEPARATOR = PK_TO_VK(PK_SEPARATOR),
	VK_OUT = PK_TO_VK(PK_OUT),
	VK_OPER = PK_TO_VK(PK_OPER),
	VK_CLEARAGAIN = PK_TO_VK(PK_CLEARAGAIN),
	VK_CRSEL = PK_TO_VK(PK_CRSEL),
	VK_EXSEL = PK_TO_VK(PK_EXSEL),

	VK_KP_00 = PK_TO_VK(PK_KP_00),
	VK_KP_000 = PK_TO_VK(PK_KP_000),
	VK_THOUSANDSSEPARATOR =
		PK_TO_VK(PK_THOUSANDSSEPARATOR),
	VK_DECIMALSEPARATOR =
		PK_TO_VK(PK_DECIMALSEPARATOR),
	VK_CURRENCYUNIT = PK_TO_VK(PK_CURRENCYUNIT),
	VK_CURRENCYSUBUNIT =
		PK_TO_VK(PK_CURRENCYSUBUNIT),
	VK_KP_LEFTPAREN = PK_TO_VK(PK_KP_LEFTPAREN),
	VK_KP_RIGHTPAREN = PK_TO_VK(PK_KP_RIGHTPAREN),
	VK_KP_LEFTBRACE = PK_TO_VK(PK_KP_LEFTBRACE),
	VK_KP_RIGHTBRACE = PK_TO_VK(PK_KP_RIGHTBRACE),
	VK_KP_TAB = PK_TO_VK(PK_KP_TAB),
	VK_KP_BACKSPACE = PK_TO_VK(PK_KP_BACKSPACE),
	VK_KP_A = PK_TO_VK(PK_KP_A),
	VK_KP_B = PK_TO_VK(PK_KP_B),
	VK_KP_C = PK_TO_VK(PK_KP_C),
	VK_KP_D = PK_TO_VK(PK_KP_D),
	VK_KP_E = PK_TO_VK(PK_KP_E),
	VK_KP_F = PK_TO_VK(PK_KP_F),
	VK_KP_XOR = PK_TO_VK(PK_KP_XOR),
	VK_KP_POWER = PK_TO_VK(PK_KP_POWER),
	VK_KP_PERCENT = PK_TO_VK(PK_KP_PERCENT),
	VK_KP_LESS = PK_TO_VK(PK_KP_LESS),
	VK_KP_GREATER = PK_TO_VK(PK_KP_GREATER),
	VK_KP_AMPERSAND = PK_TO_VK(PK_KP_AMPERSAND),
	VK_KP_DBLAMPERSAND =
		PK_TO_VK(PK_KP_DBLAMPERSAND),
	VK_KP_VERTICALBAR =
		PK_TO_VK(PK_KP_VERTICALBAR),
	VK_KP_DBLVERTICALBAR =
		PK_TO_VK(PK_KP_DBLVERTICALBAR),
	VK_KP_COLON = PK_TO_VK(PK_KP_COLON),
	VK_KP_HASH = PK_TO_VK(PK_KP_HASH),
	VK_KP_SPACE = PK_TO_VK(PK_KP_SPACE),
	VK_KP_AT = PK_TO_VK(PK_KP_AT),
	VK_KP_EXCLAM = PK_TO_VK(PK_KP_EXCLAM),
	VK_KP_MEMSTORE = PK_TO_VK(PK_KP_MEMSTORE),
	VK_KP_MEMRECALL = PK_TO_VK(PK_KP_MEMRECALL),
	VK_KP_MEMCLEAR = PK_TO_VK(PK_KP_MEMCLEAR),
	VK_KP_MEMADD = PK_TO_VK(PK_KP_MEMADD),
	VK_KP_MEMSUBTRACT =
		PK_TO_VK(PK_KP_MEMSUBTRACT),
	VK_KP_MEMMULTIPLY =
		PK_TO_VK(PK_KP_MEMMULTIPLY),
	VK_KP_MEMDIVIDE = PK_TO_VK(PK_KP_MEMDIVIDE),
	VK_KP_PLUSMINUS = PK_TO_VK(PK_KP_PLUSMINUS),
	VK_KP_CLEAR = PK_TO_VK(PK_KP_CLEAR),
	VK_KP_CLEARENTRY = PK_TO_VK(PK_KP_CLEARENTRY),
	VK_KP_BINARY = PK_TO_VK(PK_KP_BINARY),
	VK_KP_OCTAL = PK_TO_VK(PK_KP_OCTAL),
	VK_KP_DECIMAL = PK_TO_VK(PK_KP_DECIMAL),
	VK_KP_HEXADECIMAL =
		PK_TO_VK(PK_KP_HEXADECIMAL),

	VK_LCTRL = PK_TO_VK(PK_LCTRL),
	VK_LSHIFT = PK_TO_VK(PK_LSHIFT),
	VK_LALT = PK_TO_VK(PK_LALT),
	VK_LGUI = PK_TO_VK(PK_LGUI),
	VK_RCTRL = PK_TO_VK(PK_RCTRL),
	VK_RSHIFT = PK_TO_VK(PK_RSHIFT),
	VK_RALT = PK_TO_VK(PK_RALT),
	VK_RGUI = PK_TO_VK(PK_RGUI),

	VK_MODE = PK_TO_VK(PK_MODE),

	VK_AUDIONEXT = PK_TO_VK(PK_AUDIONEXT),
	VK_AUDIOPREV = PK_TO_VK(PK_AUDIOPREV),
	VK_AUDIOSTOP = PK_TO_VK(PK_AUDIOSTOP),
	VK_AUDIOPLAY = PK_TO_VK(PK_AUDIOPLAY),
	VK_AUDIOMUTE = PK_TO_VK(PK_AUDIOMUTE),
	VK_MEDIASELECT = PK_TO_VK(PK_MEDIASELECT),
	VK_WWW = PK_TO_VK(PK_WWW),
	VK_MAIL = PK_TO_VK(PK_MAIL),
	VK_CALCULATOR = PK_TO_VK(PK_CALCULATOR),
	VK_COMPUTER = PK_TO_VK(PK_COMPUTER),
	VK_AC_SEARCH = PK_TO_VK(PK_AC_SEARCH),
	VK_AC_HOME = PK_TO_VK(PK_AC_HOME),
	VK_AC_BACK = PK_TO_VK(PK_AC_BACK),
	VK_AC_FORWARD = PK_TO_VK(PK_AC_FORWARD),
	VK_AC_STOP = PK_TO_VK(PK_AC_STOP),
	VK_AC_REFRESH = PK_TO_VK(PK_AC_REFRESH),
	VK_AC_BOOKMARKS = PK_TO_VK(PK_AC_BOOKMARKS),

	VK_BRIGHTNESSDOWN =
		PK_TO_VK(PK_BRIGHTNESSDOWN),
	VK_BRIGHTNESSUP = PK_TO_VK(PK_BRIGHTNESSUP),
	VK_DISPLAYSWITCH = PK_TO_VK(PK_DISPLAYSWITCH),
	VK_KBDILLUMTOGGLE =
		PK_TO_VK(PK_KBDILLUMTOGGLE),
	VK_KBDILLUMDOWN = PK_TO_VK(PK_KBDILLUMDOWN),
	VK_KBDILLUMUP = PK_TO_VK(PK_KBDILLUMUP),
	VK_EJECT = PK_TO_VK(PK_EJECT),
	VK_SLEEP = PK_TO_VK(PK_SLEEP)
};

#if 0
struct
Key_Data
{
	Phys_Key phys_key;
	Virt_Key virt_key;
	u16 mods;
	u16 _padding2;
};
#endif

struct
Event_Keyboard
{
	Event_Common common;
	s32 window_id;
	Phys_Key phys_key;
	Virt_Key virt_key;
	u16 mods;
	Event_Keyboard_Type type;
	u8 repeat;
};

enum
Mouse_Button : u32
{
	MOUSE_BUTTON_1  = 0,
	MOUSE_BUTTON_2  = 1,
	MOUSE_BUTTON_3  = 2,
	MOUSE_BUTTON_4  = 3,
	MOUSE_BUTTON_5  = 4,
	MOUSE_BUTTON_6  = 5,
	MOUSE_BUTTON_7  = 6,
	MOUSE_BUTTON_8  = 7,
	MOUSE_BUTTON_9  = 8,
	MOUSE_BUTTON_10 = 9,
	MOUSE_BUTTON_11 = 10,
	MOUSE_BUTTON_12 = 11,
	MOUSE_BUTTON_13 = 12,
	MOUSE_BUTTON_14 = 13,
	MOUSE_BUTTON_15 = 14,
	MOUSE_BUTTON_16 = 15,
	MOUSE_BUTTON_17 = 16,
	MOUSE_BUTTON_18 = 17,
	MOUSE_BUTTON_19 = 18,
	MOUSE_BUTTON_20 = 19,
	MOUSE_BUTTON_21 = 20,
	MOUSE_BUTTON_22 = 21,
	MOUSE_BUTTON_23 = 22,
	MOUSE_BUTTON_24 = 23,
	MOUSE_BUTTON_25 = 24,
	MOUSE_BUTTON_26 = 25,
	MOUSE_BUTTON_27 = 26,
	MOUSE_BUTTON_28 = 27,
	MOUSE_BUTTON_29 = 28,
	MOUSE_BUTTON_30 = 29,
	MOUSE_BUTTON_31 = 30,
	MOUSE_BUTTON_32 = 31,
};

#define MOUSE_LEFT_BUTTON   MOUSE_BUTTON_1
#define MOUSE_MIDDLE_BUTTON MOUSE_BUTTON_2
#define MOUSE_RIGHT_BUTTON  MOUSE_BUTTON_3

enum
Mouse_State : u32
{
	MOUSE_STATE_BUTTON_1  = 0x1u << 0,
	MOUSE_STATE_BUTTON_2  = 0x1u << 1,
	MOUSE_STATE_BUTTON_3  = 0x1u << 2,
	MOUSE_STATE_BUTTON_4  = 0x1u << 3,
	MOUSE_STATE_BUTTON_5  = 0x1u << 4,
	MOUSE_STATE_BUTTON_6  = 0x1u << 5,
	MOUSE_STATE_BUTTON_7  = 0x1u << 6,
	MOUSE_STATE_BUTTON_8  = 0x1u << 7,
	MOUSE_STATE_BUTTON_9  = 0x1u << 8,
	MOUSE_STATE_BUTTON_10 = 0x1u << 9,
	MOUSE_STATE_BUTTON_11 = 0x1u << 10,
	MOUSE_STATE_BUTTON_12 = 0x1u << 11,
	MOUSE_STATE_BUTTON_13 = 0x1u << 12,
	MOUSE_STATE_BUTTON_14 = 0x1u << 13,
	MOUSE_STATE_BUTTON_15 = 0x1u << 14,
	MOUSE_STATE_BUTTON_16 = 0x1u << 15,
	MOUSE_STATE_BUTTON_17 = 0x1u << 16,
	MOUSE_STATE_BUTTON_18 = 0x1u << 17,
	MOUSE_STATE_BUTTON_19 = 0x1u << 18,
	MOUSE_STATE_BUTTON_20 = 0x1u << 19,
	MOUSE_STATE_BUTTON_21 = 0x1u << 20,
	MOUSE_STATE_BUTTON_22 = 0x1u << 21,
	MOUSE_STATE_BUTTON_23 = 0x1u << 22,
	MOUSE_STATE_BUTTON_24 = 0x1u << 23,
	MOUSE_STATE_BUTTON_25 = 0x1u << 24,
	MOUSE_STATE_BUTTON_26 = 0x1u << 25,
	MOUSE_STATE_BUTTON_27 = 0x1u << 26,
	MOUSE_STATE_BUTTON_28 = 0x1u << 27,
	MOUSE_STATE_BUTTON_29 = 0x1u << 28,
	MOUSE_STATE_BUTTON_30 = 0x1u << 29,
	MOUSE_STATE_BUTTON_31 = 0x1u << 30,
	MOUSE_STATE_BUTTON_32 = 0x1u << 31,
};

#define MOUSE_STATE_LEFT_BUTTON   MOUSE_STATE_BUTTON_1
#define MOUSE_STATE_MIDDLE_BUTTON MOUSE_STATE_BUTTON_2
#define MOUSE_STATE_RIGHT_BUTTON  MOUSE_STATE_BUTTON_3

#define MOUSE_STATE_BUTTON(i) (0x1<<(i))

#define MOUSE_EVENT_EXPAND_AS_ENUM(a) a,
#define MOUSE_EVENT_EXPAND_AS_NAME_TABLE(a) LPR_TOSTRING(a),
#define MOUSE_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_MOUSE_BUTTON_PRESSED ) \
	ENTRY( EVENT_MOUSE_BUTTON_RELEASED ) \
	ENTRY( EVENT_MOUSE_MOTION ) \
	ENTRY( EVENT_MOUSE_WHEEL ) \

enum
Event_Mouse_Type : u8
{
	MOUSE_EVENT_TABLE( MOUSE_EVENT_EXPAND_AS_ENUM )
	// ---
	EVENT_MOUSE_COUNT,
};

struct
Event_Mouse
{
	Event_Common common;
	s32 windows_id;
	Event_Mouse_Type type;
	u8 _padding1;
	u8 _padding2;
	u8 _padding3;
	u32 state;
	union
	{
		struct
		{
			s32 pos_x, pos_y;
			s32 motion_x, motion_y;
		} motion;
		struct
		{
			s32 pos_x, pos_y;
			Mouse_Button button;
			u8 clicks_count;
			u8 _padding1;
			u8 _padding2;
		} button;
		struct
		{
			s32 scrolled_x, scrolled_y;
		} wheel;
	};
};


#define CONTROLLER_EVENT_EXPAND_AS_ENUM(a) a,
#define CONTROLLER_EVENT_EXPAND_AS_NAME_TABLE(a) LPR_TOSTRING(a),
#define CONTROLLER_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_CONTROLLER_AXIS ) \
	ENTRY( EVENT_CONTROLLER_BUTTON_PRESSED ) \
	ENTRY( EVENT_CONTROLLER_BUTTON_RELEASED ) \
	ENTRY( EVENT_CONTROLLER_DEVICE_ADDED ) \
	ENTRY( EVENT_CONTROLLER_DEVICE_REMOVED ) \
	ENTRY( EVENT_CONTROLLER_DEVICE_REMAPPED ) \

enum
Event_Controller_Type : u8
{
	CONTROLLER_EVENT_TABLE( CONTROLLER_EVENT_EXPAND_AS_ENUM )
	// ---
	EVENT_CONTROLLER_COUNT,
};

enum
Controller_Button : u8
{
	CONTROLLER_BUTTON_INVALID,
	CONTROLLER_BUTTON_A,
	CONTROLLER_BUTTON_B,
	CONTROLLER_BUTTON_X,
	CONTROLLER_BUTTON_Y,
	CONTROLLER_BUTTON_BACK,
	CONTROLLER_BUTTON_GUIDE,
	CONTROLLER_BUTTON_START,
	CONTROLLER_BUTTON_LEFTSTICK,
	CONTROLLER_BUTTON_RIGHTSTICK,
	CONTROLLER_BUTTON_LEFTSHOULDER,
	CONTROLLER_BUTTON_RIGHTSHOULDER,
	CONTROLLER_BUTTON_DPAD_UP,
	CONTROLLER_BUTTON_DPAD_DOWN,
	CONTROLLER_BUTTON_DPAD_LEFT,
	CONTROLLER_BUTTON_DPAD_RIGHT,
	// ---
	CONTROLLER_BUTTON_COUNT
};

enum
Controller_Axis : u8
{
	CONTROLLER_AXIS_INVALID,
	CONTROLLER_AXIS_LEFTX,
	CONTROLLER_AXIS_LEFTY,
	CONTROLLER_AXIS_RIGHTX,
	CONTROLLER_AXIS_RIGHTY,
	CONTROLLER_AXIS_TRIGGERLEFT,
	CONTROLLER_AXIS_TRIGGERRIGHT,
	// ---
	CONTROLLER_AXIS_COUNT 
};

struct
Event_Controller
{
	Event_Common common;
	u32 controller_id;
	s32 motion;
	Event_Controller_Type type;
	Controller_Axis axis;
	Controller_Button button;
	u8 _padding1;
};

#define GAME_SYSTEM_EVENT_EXPAND_AS_ENUM(a) a,
#define GAME_SYSTEM_EVENT_EXPAND_AS_NAME_TABLE(a) LPR_TOSTRING(a),
#define GAME_SYSTEM_EVENT_TABLE(ENTRY) \
	ENTRY( EVENT_GAME_API_DL_RELOADED ) \

enum
Event_Game_System_Type : u8
{
	GAME_SYSTEM_EVENT_TABLE( GAME_SYSTEM_EVENT_EXPAND_AS_ENUM )
	// ---
	EVENT_GAME_SYSTEM_COUNT,
};

struct
Event_Game_System
{
	Event_Common common;
	Event_Game_System_Type type;
	u8 _padding1;
	u8 _padding2;
	u8 _padding3;
};

union
Event
{
	Event_Type        type;
	Event_Common      common;
	Event_Window      window;
	Event_Keyboard    keyboard;
	Event_Mouse       mouse;
	Event_Controller  controller;
	Event_Game_System game_system;
};


#endif /* ifndef GENERIC_SYSTEM_H */
