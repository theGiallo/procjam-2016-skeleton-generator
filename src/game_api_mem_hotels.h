#if !defined( MEM_HOTELS_H ) || MH_EMIT_BODY
#ifndef MEM_HOTELS_H
	#define MEM_HOTELS_H 1
#else
	#if MH_EMIT_BODY
	#define MH_BODY_ONLY 1
	#endif
#endif

#define MH_STATIC 1
#include "game_api.h"

#define MH_TYPE Sound
#include "mem_hotel.inc.h"

#define MH_FLOORS_COUNT 10
#define MH_TYPE Sound
#include "mem_hotel.inc.h"

#define MH_FLOORS_COUNT 1024
#define MH_TYPE Sound
#include "mem_hotel.inc.h"

#define MH_TYPE Lpr_Texture
#include "mem_hotel.inc.h"

#define MH_TYPE ptr_u64_LL
#include "mem_hotel.inc.h"

#define MH_TYPE u32_u32_LL
#include "mem_hotel.inc.h"

#define MH_TYPE Font_LL
#include "mem_hotel.inc.h"

typedef char * charp;
#define MH_TYPE charp
#include "mem_hotel.inc.h"

#define MH_TYPE Resource_Image
#include "mem_hotel.inc.h"

#define MH_TYPE Test_2D_Entity
#include "mem_hotel.inc.h"

#endif /* ifndef MEM_HOTELS_H */
