#ifndef OPENGL_TOOLS_H
#define OPENGL_TOOLS_H 1

struct Game_Memory;
#include "game_api_lepre.h"
#include "game_api.h"
#include "basic_types.h"
#include "tgmath.h"
#include "our_gl.h"

struct
File
{
	const char * file_path;
	Time modified_time;
};
struct Shader
{
	File geom, vert, frag;
	GLuint program;
	GLint VP_loc;
	GLint V_loc;
	GLint P_loc;
	GLint color_loc;
	GLuint vertex_loc;
	GLuint vertex_color_loc;
};


struct
Camera3D
{
	Mx4 V,P,VP;
	V3  pos, up, look;
	V2  vanishing_point;
	V2  size;
	f32 near;
	f32 fov;
	f32 inverse_aspect_ratio; // NOTE(theGiallo): h / w
};

union
Controlled_Motion
{
	struct
	{
		f32 side;
		f32 straight;
		f32 vertical;
	};
	V3 v3;
};
struct
Distributed_Speed
{
	Controlled_Motion positive, negative;
};

struct
Colored_Vertex
{
	V3 vertex;
	V3 color;
};

struct
Frame_Buffer_With_Depth
{
	GLuint linear_fbo,
	       linear_color_rt,
	       depth_rt,
	       linear_fb_texture;
	V2u32 size;
};

// NOTE(theGiallo): h in [0,1)
Lpr_Rgba_f32
rgb_from_hsv( Lpr_Rgba_f32 hsv );

V3
rgb_from_v2( V2 v );

bool
create_framebuffer_with_depth( V2u32 px_size,
                               Frame_Buffer_With_Depth * fb );

inline
void
create_and_fill_ogl_buffer( GLuint * buffer, GLenum target, GLenum usage, void * data, GLsizeiptr byte_size )
{
	glGenBuffers( 1, buffer );
	glBindBuffer( target, *buffer );
	glBufferData( target, byte_size,
	              data,  usage );
	glBindBuffer( target, 0 );
}

bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length );

bool
create_program( System_API * sys_api, Game_Memory * game_mem,
                const char * geom, const char * vert, const char * frag,
                GLuint * out_program );

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         Lpr_Rgba_f32 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage );

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): camera

extern
inline
void
initialize_camera( Camera3D * camera );

extern
inline
void
resize_camera( Camera3D * camera, u32 width, u32 height );

extern
inline
void
update_view( Camera3D * camera );

extern
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, V2 size );

extern
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio );

extern
inline
V2u32
compute_window_center( Game_Memory * game_mem );

extern
inline
void
warp_mouse_to_center( System_API * sys_api, Game_Memory * game_mem );

// NOTE(theGiallo):
// dir should be positive if the key is ahead or right
// dir should be negative if the key is back or left
extern
inline
void
manage_motion_key( Game_Memory * game_mem, Phys_Key key, s32 dir, f32 * var );

// NOTE(theGiallo): ahead, right and up are supposed to be normalized
// motion values are supposed to be +1, 0 or-1
extern
inline
void
apply_controlled_motion( Controlled_Motion motion, V3 ahead, V3 right, V3 up,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time, V3 * pos );

extern
inline
void
apply_controlled_motion( Controlled_Motion motion, Camera3D * camera,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time );

extern
inline
void
per_frame_camera_controls_and_update(
   Game_Memory * game_mem, Camera3D * camera, Controlled_Motion * camera_motion,
   Distributed_Speed * distributed_camera_speed, f32 shift_speed_mod );

extern
inline
bool
fp_camera_mouse_motion( System_API * sys_api, Game_Memory * game_mem,
                        s32 pos_x, s32 pos_y,
                        s32 motion_x, s32 motion_y, Camera3D * camera );
extern
inline
void
set_ogl_camera( Shader * shader, Camera3D * camera );

#endif
