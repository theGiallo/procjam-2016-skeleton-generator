#ifndef GAME_API_UTILITY_H
#define GAME_API_UTILITY_H 1

#include "basic_types.h"
#include "game_api_lepre.h"

#define ARRAY_COUNT( a ) (sizeof(a) / sizeof((a)[0]))

// NOTE(theGiallo): the first time pass *internal_bit = 0
internal
u8
rand_bit( u64 seeds[2], u64 * internal_data,
          u8 * internal_bit_id );

internal
f32
randd_between( u64 seeds[2], f32 left, f32 right );

// NOTE(theGiallo): random u32 in [min, max] interval
internal
u32
randu_between( u64 seeds[2], u32 min, u32 max );

internal
f32
randd_01( u64 seeds[2] );

struct
Marsaglia_Gauss_Data
{
	f64 v1, v2, s, u1, u2;
	s32 phase;
	u64 seeds[2];
};
internal
f64
randd_gauss_01( Marsaglia_Gauss_Data * g_data );

inline
internal
f64
randd_gauss( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd )
{
	f64 ret = randd_gauss_01( g_data ) * sd + mean;

	return ret;
}

inline
internal
f64
randd_gauss_clamped( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd,
                     f64 min, f64 max )
{
	f64 ret;
	do
	{
		ret = randd_gauss( g_data, mean, sd );
	} while ( ret < min || ret > max );

	return ret;
}

internal
u32
u32_FNV1a( const char * str );

internal
u32
u32_FNV1a( const void * str, u32 bytes_size );

internal
inline
f32
f32_from_u32_after_radixsort( u32 uf )
{
	f32 ret;
	u32 mask = ( ( uf >> 31 ) - 1 ) | 0x80000000;
	uf = uf ^ mask;
	ret = *(f32*)&uf;

	return ret;
}
enum
Sort_Order : u8
{
	SORT_ASCENDANTLY,
	SORT_DESCENDANTLY
};

internal
void
radix_sort_f32( f32 * keys_arr, u32 keys_arr_size, u16 * ids,
                Sort_Order sort_order,
                bool restore_keys_values = false );

internal
void
sort_lpr_batch_f32( Lpr_Draw_Data * draw_data, u32 batch_id,
                    f32 * keys_arr, bool restore_keys_values = false );

internal
void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
radix_sort_mc_32_16( const u32 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
radix_sort_mc_64_16( const u64 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
internal
s32
string_min( const char * string0, const char * string1 );

internal
void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data = NULL, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
sort_lexicographically_insertion_ids_only( const char *const* strings, u32 count,
                                           u32 * ids, Sort_Order sort_order );

internal
void
sort_lexicographically_insertion_ids_only_16( const char *const* strings, u32 count,
                                              u16 * ids, Sort_Order sort_order );

internal
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order = SORT_ASCENDANTLY,
                                        u32 * data     = NULL,
                                        u32 * data_tmp = NULL,
                                        u32 * lengths_strings = NULL,
                                        u32 * lengths_tmp     = NULL,
                                        u32 char_idx = 0 );

internal
bool
sort_lexicographically_radix_insertion_ids_only( char const*const* strings, u32 count,
                                                 Sort_Order sort_order,
                                                 u32 * ids,
                                                 u32 * ids_tmp = NULL,
                                                 u32 const * lengths_strings = NULL,
                                                 u32 char_idx = 0 );

internal
bool
sort_lexicographically_radix_insertion_ids_only_16( char const*const* strings, u32 count,
                                                    Sort_Order sort_order,
                                                    u16 * ids,
                                                    u16 * ids_tmp = NULL,
                                                    u32 const * lengths_strings = NULL,
                                                    u32 char_idx = 0 );

// NOTE(theGiallo): Stable sort of UTF-8 strings.
// NOTE(theGiallo): if you provide lengths* you have to fill them.
// If not provided they will be allocated on the stack (<- IMPORTANT!)
// and their values computed (aka string traversal).
// NOTE(theGiallo): the strings, data and lengths array will be sorted after
// the call. *tmp content can be discarded.
// NOTE(theGiallo): if you won't provide tmp, it will be allocated on the stack.
internal
void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp = NULL,
                        Sort_Order sort_order = SORT_ASCENDANTLY,
                        u32 * data     = NULL,
                        u32 * data_tmp = NULL,
                        u32 * lengths_strings = NULL,
                        u32 * lengths_tmp     = NULL );

internal
void
sort_lexicographically_ids_only( char const*const* strings, u32 count,
                                 Sort_Order sort_order,
                                 u32 * ids,
                                 u32 * ids_tmp = NULL,
                                 u32 * lengths_strings = NULL );

internal
void
sort_lexicographically_ids_only_16( char const*const* strings, u32 count,
                                    Sort_Order sort_order,
                                    u16 * ids,
                                    u16 * ids_tmp = NULL,
                                    u32 * lengths_strings = NULL );

inline
u32
str_bytes_to_null( const char * string )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != U32_MAX - 1 && string[ret]; ++ret );
	}

	return ret;
}
#endif /* ifndef GAME_API_UTILITY_H */
