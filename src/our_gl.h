#ifndef OUR_GL_H
#define OUR_GL_H 1

// NOTE(theGiallo): IMPORTANT when you modify the function declarations in this
// file, you have to run the script extract_gl_functions_from_header.sh before
// compiling

#include "basic_types.h"
#include <stddef.h>

#if OUR_GL_EXTERN_POINTERS
#define FP_DEF extern
#endif

#if OUR_GL_EXPORT_POINTERS
#if WIN32
	#define FP_DEF extern "C" __declspec(dllexport)
#else
	#define FP_DEF extern "C"
#endif
#endif

#ifndef FP_DEF
#define FP_DEF
#endif

#define OPTIONAL

typedef f32 GLfloat;
typedef f32 GLclampf;
typedef f64 GLclampd;
typedef u32 GLenum;
typedef u32 GLuint;
typedef u32 GLbitfield;
typedef s8  GLbyte;
typedef u8  GLubyte;
typedef u8  GLboolean;
typedef s32 GLint;
typedef s32 GLsizei;
typedef ptrdiff_t GLsizeiptr;
typedef ptrdiff_t GLintptr;
typedef char GLchar;
typedef void GLvoid;

#define GL_FALSE                                     0
#define GL_TRUE                                      1

#define GL_POINTS                                    0x0000
#define GL_LINES                                     0x0001
#define GL_LINE_LOOP                                 0x0002
#define GL_LINE_STRIP                                0x0003
#define GL_TRIANGLES                                 0x0004
#define GL_TRIANGLE_STRIP                            0x0005
#define GL_TRIANGLE_FAN                              0x0006
#define GL_LINES_ADJACENCY                           0x000A
#define GL_LINE_STRIP_ADJACENCY                      0x000B
#define GL_TRIANGLES_ADJACENCY                       0x000C
#define GL_TRIANGLE_STRIP_ADJACENCY                  0x000D
#define GL_NEVER                                     0x0200
#define GL_LESS                                      0x0201
#define GL_EQUAL                                     0x0202
#define GL_LEQUAL                                    0x0203
#define GL_GREATER                                   0x0204
#define GL_NOTEQUAL                                  0x0205
#define GL_GEQUAL                                    0x0206
#define GL_ALWAYS                                    0x0207
#define GL_CULL_FACE                                 0x0B44
#define GL_DEPTH_TEST                                0x0B71
#define GL_DEPTH_BITS                                0x0D56
#define GL_DEPTH_CLEAR_VALUE                         0x0B73
#define GL_DEPTH_FUNC                                0x0B74
#define GL_DEPTH_RANGE                               0x0B70
#define GL_DEPTH_WRITEMASK                           0x0B72
#define GL_DEPTH_COMPONENT                           0x1902
#define GL_POINT                                     0x1B00
#define GL_LINE                                      0x1B01
#define GL_FILL                                      0x1B02
#define GL_FRAMEBUFFER                               0x8D40
#define GL_RENDERBUFFER                              0x8D41
#define GL_NEAREST                                   0x2600
#define GL_LINEAR                                    0x2601
#define GL_NEAREST_MIPMAP_NEAREST                    0x2700
#define GL_LINEAR_MIPMAP_NEAREST                     0x2701
#define GL_NEAREST_MIPMAP_LINEAR                     0x2702
#define GL_LINEAR_MIPMAP_LINEAR                      0x2703
//#define GL_CLAMP                                   0x2900
#define GL_CLAMP_TO_EDGE                             0x812F
#define GL_REPEAT                                    0x2901
#define GL_MIRRORED_REPEAT                           0x8370
#define GL_ARRAY_BUFFER                              0x8892
#define GL_STREAM_DRAW                               0x88E0
#define GL_STATIC_DRAW                               0x88E4
#define GL_DYNAMIC_DRAW                              0x88E8
#define GL_FLOAT                                     0x1406
#define GL_FRAMEBUFFER_COMPLETE                      0x8CD5
#define GL_FRAMEBUFFER_UNSUPPORTED                   0x8CDD
#define GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT 0x8CD7
#define GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER        0x8CDB
#define GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT         0x8CD6
#define GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER        0x8CDC
//#define GL_FRAMEBUFFER_                              0x
#define GL_UNSIGNED_BYTE                             0x1401
#define GL_UNSIGNED_INT                              0x1405
#define GL_CURRENT_BIT                               0x00000001
#define GL_POINT_BIT                                 0x00000002
#define GL_LINE_BIT                                  0x00000004
#define GL_POLYGON_BIT                               0x00000008
#define GL_POLYGON_STIPPLE_BIT                       0x00000010
#define GL_PIXEL_MODE_BIT                            0x00000020
#define GL_LIGHTING_BIT                              0x00000040
#define GL_FOG_BIT                                   0x00000080
#define GL_DEPTH_BUFFER_BIT                          0x00000100
#define GL_ACCUM_BUFFER_BIT                          0x00000200
#define GL_STENCIL_BUFFER_BIT                        0x00000400
#define GL_VIEWPORT_BIT                              0x00000800
#define GL_TRANSFORM_BIT                             0x00001000
#define GL_ENABLE_BIT                                0x00002000
#define GL_COLOR_BUFFER_BIT                          0x00004000
#define GL_HINT_BIT                                  0x00008000
#define GL_EVAL_BIT                                  0x00010000
#define GL_LIST_BIT                                  0x00020000
#define GL_TEXTURE_BIT                               0x00040000
#define GL_SCISSOR_BIT                               0x00080000
#define GL_ALL_ATTRIB_BIT                            0xFFFFFFFF
#define GL_CLIENT_PIXEL_STORE_BIT                    0x00000001
#define GL_CLIENT_VERTEX_ARRAY_BIT                   0x00000002
#define GL_ALL_CLIENT_ATTRIB_BIT                     0xFFFFFFFF
#define GL_CLIENT_ALL_ATTRIB_BIT                     0xFFFFFFFF
#define GL_MULTISAMPLE_BIT                           0x20000000
#define GL_COLOR_BUFFER_BIT                          0x00004000
#define GL_TEXTURE0                                  0x84C0
#define GL_TEXTURE1                                  0x84C1
#define GL_TEXTURE2                                  0x84C2
#define GL_TEXTURE3                                  0x84C3
#define GL_TEXTURE4                                  0x84C4
#define GL_TEXTURE5                                  0x84C5
#define GL_TEXTURE6                                  0x84C6
#define GL_TEXTURE7                                  0x84C7
#define GL_TEXTURE8                                  0x84C8
#define GL_TEXTURE9                                  0x84C9
#define GL_TEXTURE10                                 0x84CA
#define GL_TEXTURE11                                 0x84CB
#define GL_TEXTURE12                                 0x84CC
#define GL_TEXTURE13                                 0x84CD
#define GL_TEXTURE14                                 0x84CE
#define GL_TEXTURE15                                 0x84CF
#define GL_TEXTURE16                                 0x84D0
#define GL_TEXTURE17                                 0x84D1
#define GL_TEXTURE18                                 0x84D2
#define GL_TEXTURE19                                 0x84D3
#define GL_TEXTURE20                                 0x84D4
#define GL_TEXTURE21                                 0x84D5
#define GL_TEXTURE22                                 0x84D6
#define GL_TEXTURE23                                 0x84D7
#define GL_TEXTURE24                                 0x84D8
#define GL_TEXTURE25                                 0x84D9
#define GL_TEXTURE26                                 0x84DA
#define GL_TEXTURE27                                 0x84DB
#define GL_TEXTURE28                                 0x84DC
#define GL_TEXTURE29                                 0x84DD
#define GL_TEXTURE30                                 0x84DE
#define GL_TEXTURE31                                 0x84DF
#define GL_NONE                                      0
#define GL_ZERO                                      0
#define GL_ONE                                       1
#define GL_ONE_MINUS_SRC_COLOR                       0x0301
#define GL_ONE_MINUS_SRC_ALPHA                       0x0303
#define GL_ONE_MINUS_DST_ALPHA                       0x0305
#define GL_ONE_MINUS_DST_COLOR                       0x0307
#define GL_ONE_MINUS_CONSTANT_COLOR                  0x8002
#define GL_ONE_MINUS_CONSTANT_ALPHA                  0x8004
#define GL_DST_COLOR                                 0x0306
#define GL_FUNC_ADD                                  0x8006
#define GL_BLEND                                     0x0BE2
#define GL_BLEND_SRC                                 0x0BE1
#define GL_BLEND_DST                                 0x0BE0
#define GL_BLEND_EQUATION                            0x8009
#define GL_BLEND_COLOR                               0x8005
#define GL_DEPTH_TEST                                0x0B71
#define GL_TEXTURE_2D                                0x0DE1
#define GL_TEXTURE_WRAP_S                            0x2802
#define GL_TEXTURE_WRAP_T                            0x2803
//#define GL_TEXTURE_WRAP_R                            0x8072
#define GL_TEXTURE_MAG_FILTER                        0x2800
#define GL_TEXTURE_MIN_FILTER                        0x2801
#define GL_TEXTURE_BASE_LEVEL                        0x813C
#define GL_TEXTURE_MAX_LEVEL                         0x813D
#define GL_TEXTURE_SWIZZLE_RGBA                      0x8E46
#define GL_TEXTURE                                   0x1702
#define GL_TEXTURE_WIDTH                             0x1000
#define GL_TEXTURE_HEIGHT                            0x1001
#define GL_TEXTURE_INTERNAL_FORMAT                   0x1003
#define GL_MAX_COLOR_ATTACHMENTS                     0x8CDF
#define GL_COLOR_ATTACHMENT0                         0x8CE0
#define GL_COLOR_ATTACHMENT1                         0x8CE1
#define GL_COLOR_ATTACHMENT2                         0x8CE2
#define GL_COLOR_ATTACHMENT3                         0x8CE3
#define GL_COLOR_ATTACHMENT4                         0x8CE4
#define GL_COLOR_ATTACHMENT5                         0x8CE5
#define GL_COLOR_ATTACHMENT6                         0x8CE6
#define GL_COLOR_ATTACHMENT7                         0x8CE7
#define GL_COLOR_ATTACHMENT8                         0x8CE8
#define GL_COLOR_ATTACHMENT9                         0x8CE9
#define GL_COLOR_ATTACHMENT10                        0x8CEA
#define GL_COLOR_ATTACHMENT11                        0x8CEB
#define GL_COLOR_ATTACHMENT12                        0x8CEC
#define GL_COLOR_ATTACHMENT13                        0x8CED
#define GL_COLOR_ATTACHMENT14                        0x8CEE
#define GL_COLOR_ATTACHMENT15                        0x8CEF
#define GL_COLOR_ATTACHMENT16                        0x8CF0
#define GL_COLOR_ATTACHMENT17                        0x8CF1
#define GL_COLOR_ATTACHMENT18                        0x8CF2
#define GL_COLOR_ATTACHMENT19                        0x8CF3
#define GL_COLOR_ATTACHMENT20                        0x8CF4
#define GL_COLOR_ATTACHMENT21                        0x8CF5
#define GL_COLOR_ATTACHMENT22                        0x8CF6
#define GL_COLOR_ATTACHMENT23                        0x8CF7
#define GL_COLOR_ATTACHMENT24                        0x8CF8
#define GL_COLOR_ATTACHMENT25                        0x8CF9
#define GL_COLOR_ATTACHMENT26                        0x8CFA
#define GL_COLOR_ATTACHMENT27                        0x8CFB
#define GL_COLOR_ATTACHMENT28                        0x8CFC
#define GL_COLOR_ATTACHMENT29                        0x8CFD
#define GL_COLOR_ATTACHMENT30                        0x8CFE
#define GL_COLOR_ATTACHMENT31                        0x8CFF
#define GL_RED                                       0x1903
#define GL_ALPHA                                     0x1906
#define GL_LUMINANCE                                 0x1909
#define GL_LUMINANCE_ALPHA                           0x190A
#define GL_ALPHA4                                    0x803B
#define GL_ALPHA8                                    0x803C
#define GL_ALPHA12                                   0x803D
#define GL_ALPHA16                                   0x803E
#define GL_LUMINANCE4                                0x803F
#define GL_LUMINANCE8                                0x8040
#define GL_LUMINANCE12                               0x8041
#define GL_LUMINANCE16                               0x8042
#define GL_LUMINANCE4_ALPHA4                         0x8043
#define GL_LUMINANCE6_ALPHA2                         0x8044
#define GL_LUMINANCE8_ALPHA8                         0x8045
#define GL_LUMINANCE12_ALPHA4                        0x8046
#define GL_LUMINANCE12_ALPHA12                       0x8047
#define GL_LUMINANCE16_ALPHA16                       0x8048
#define GL_INTENSITY                                 0x8049
#define GL_INTENSITY4                                0x804A
#define GL_INTENSITY8                                0x804B
#define GL_INTENSITY12                               0x804C
#define GL_INTENSITY16                               0x804D
#define GL_R3_G3_B2                                  0x2A10
#define GL_RGB4                                      0x804F
#define GL_RGB5                                      0x8050
#define GL_RGB8                                      0x8051
#define GL_RGB10                                     0x8052
#define GL_RGB12                                     0x8053
#define GL_RGB16                                     0x8054
#define GL_RGBA2                                     0x8055
#define GL_RGBA4                                     0x8056
#define GL_RGB5_A1                                   0x8057
#define GL_RGB10_A2                                  0x8059
#define GL_RGBA12                                    0x805A
#define GL_RGBA16                                    0x805B
#define GL_SRGB                                      0x8C40
#define GL_SRGB8                                     0x8C41
#define GL_SRGB_ALPHA                                0x8C42
#define GL_SRGB8_ALPHA8                              0x8C43
#define GL_R8                                        0x8229
#define GL_RGBA8                                     0x8058
#define GL_RGB16F                                    0x881B
#define GL_RGB32F                                    0x8815
#define GL_RGBA32F                                   0x8814
#define GL_RGBA16F                                   0x881A
#define GL_RGB                                       0x1907
#define GL_RGBA                                      0x1908
#define GL_HALF_FLOAT                                0x140B
#define GL_FRAGMENT_SHADER                           0x8B30
#define GL_VERTEX_SHADER                             0x8B31
#define GL_GEOMETRY_SHADER                           0x8DD9
#define GL_ELEMENT_ARRAY_BUFFER                      0x8893
#define GL_COMPILE_STATUS                            0x8B81
#define GL_LINK_STATUS                               0x8B82
#define GL_VALIDATE_STATUS                           0x8B83
#define GL_INFO_LOG_LENGTH                           0x8B84
#define GL_STATIC_DRAW                               0x88E4
#define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE        0x8CD0
#define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME        0x8CD1
#define GL_DEPTH_ATTACHMENT                          0x8D00
#define GL_STENCIL_INDEX                             0x1901
#define GL_DEPTH_COMPONENT                           0x1902
#define GL_DEPTH_COMPONENT16                         0x81A5
#define GL_DEPTH_COMPONENT24                         0x81A6
#define GL_DEPTH_COMPONENT32                         0x81A7
#define GL_DEPTH_STENCIL                             0x84F9
#define GL_DEPTH24_STENCIL8                          0x88F0
#define GL_COMPRESSED_SRGB                           0x8C48
#define GL_COMPRESSED_SRGB_ALPHA                     0x8C49
#define GL_COMPRESSED_SLUMINANCE                     0x8C4A
#define GL_COMPRESSED_SLUMINANCE_ALPHA               0x8C4B
#define GL_STENCIL_ATTACHMENT                        0x8D20
#define GL_RENDERBUFFER_WIDTH                        0x8D42
#define GL_RENDERBUFFER_HEIGHT                       0x8D43
#define GL_FRAMEBUFFER_SRGB                          0x8DB9
#define GL_DONT_CARE                                 0x1100
#define GL_FRONT                                     0x0404
#define GL_BACK                                      0x0405
#define GL_FRONT_AND_BACK                            0x0408
#define GL_RENDERBUFFER_INTERNAL_FORMAT              0x8D44
#define GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING     0x8210
#define GL_DEBUG_SEVERITY_HIGH                       0x9146
#define GL_DEBUG_SEVERITY_MEDIUM                     0x9147
#define GL_DEBUG_SEVERITY_LOW                        0x9148
#define GL_DEBUG_OUTPUT_SYNCHRONOUS                  0x8242
//#define GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH          0x8243
//#define GL_DEBUG_CALLBACK_FUNCTION                   0x8244
//#define GL_DEBUG_CALLBACK_USER_PARAM                 0x8245
#define GL_DEBUG_SOURCE_API                          0x8246
#define GL_DEBUG_SOURCE_WINDOW_SYSTEM                0x8247
#define GL_DEBUG_SOURCE_SHADER_COMPILER              0x8248
#define GL_DEBUG_SOURCE_THIRD_PARTY                  0x8249
#define GL_DEBUG_SOURCE_APPLICATION                  0x824A
#define GL_DEBUG_SOURCE_OTHER                        0x824B
#define GL_DEBUG_TYPE_ERROR                          0x824C
#define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR            0x824D
#define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR             0x824E
#define GL_DEBUG_TYPE_PORTABILITY                    0x824F
#define GL_DEBUG_TYPE_PERFORMANCE                    0x8250
#define GL_DEBUG_TYPE_OTHER                          0x8251
//#define GL_DEBUG_LOGGED_MESSAGES                     0x9145
//#define GL_DEBUG_SEVERITY_HIGH                       0x9146
//#define GL_DEBUG_SEVERITY_MEDIUM                     0x9147
//#define GL_DEBUG_SEVERITY_LOW                        0x9148
//#define GL_DEBUG_TYPE_MARKER                         0x8268
//#define GL_DEBUG_TYPE_PUSH_GROUP                     0x8269
//#define GL_DEBUG_TYPE_POP_GROUP                      0x826A
//#define GL_DEBUG_SEVERITY_NOTIFICATION               0x826B
//#define GL_DEBUG_GROUP_STACK_DEPTH                   0x826D
//#define GL_DEBUG_OUTPUT                              0x92E0
//#define GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB              0x8242
//#define GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_ARB      0x8243
//#define GL_DEBUG_CALLBACK_FUNCTION_ARB               0x8244
//#define GL_DEBUG_CALLBACK_USER_PARAM_ARB             0x8245
//#define GL_DEBUG_SOURCE_API_ARB                      0x8246
//#define GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB            0x8247
//#define GL_DEBUG_SOURCE_SHADER_COMPILER_ARB          0x8248
//#define GL_DEBUG_SOURCE_THIRD_PARTY_ARB              0x8249
//#define GL_DEBUG_SOURCE_APPLICATION_ARB              0x824A
//#define GL_DEBUG_SOURCE_OTHER_ARB                    0x824B
//#define GL_DEBUG_TYPE_ERROR_ARB                      0x824C
//#define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB        0x824D
//#define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB         0x824E
//#define GL_DEBUG_TYPE_PORTABILITY_ARB                0x824F
//#define GL_DEBUG_TYPE_PERFORMANCE_ARB                0x8250
//#define GL_DEBUG_TYPE_OTHER_ARB                      0x8251
//#define GL_DEBUG_LOGGED_MESSAGES_ARB                 0x9145
//#define GL_DEBUG_SEVERITY_HIGH_ARB                   0x9146
//#define GL_DEBUG_SEVERITY_MEDIUM_ARB                 0x9147
//#define GL_DEBUG_SEVERITY_LOW_ARB                    0x9148
//#define GL_DEBUG_LOGGED_MESSAGES_AMD                 0x9145
//#define GL_DEBUG_SEVERITY_HIGH_AMD                   0x9146
//#define GL_DEBUG_SEVERITY_MEDIUM_AMD                 0x9147
//#define GL_DEBUG_SEVERITY_LOW_AMD                    0x9148
#define GL_DEBUG_CATEGORY_API_ERROR_AMD              0x9149
#define GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD          0x914A
//#define GL_DEBUG_CATEGORY_DEPRECATION_AMD            0x914B
//#define GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD     0x914C
//#define GL_DEBUG_CATEGORY_PERFORMANCE_AMD            0x914D
#define GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD        0x914E
#define GL_DEBUG_CATEGORY_APPLICATION_AMD            0x914F
#define GL_DEBUG_CATEGORY_OTHER_AMD                  0x9150
//#define GL_                                          0x
//#define GL_FRAMEBUFFER_ATTACHMENT_CCOMPONENT_TYPE    0x8211
//#define GL_RENDERBUFFER_INTERNAL_FORMAT              0x8D44
//#define GL_FRAMEBUFFER_COMPLETE                      0x8CD5
#define GL_NO_ERROR                                  0
#define GL_INVALID_ENUM                              0x0500
#define GL_INVALID_VALUE                             0x0501
#define GL_INVALID_OPERATION                         0x0502
#define GL_STACK_OVERFLOW                            0x0503
#define GL_STACK_UNDERFLOW                           0x0504
#define GL_OUT_OF_MEMORY                             0x0505


#define GLF_CLEAR( name ) void name( GLbitfield mask )
typedef GLF_CLEAR( glClear_t );
FP_DEF
glClear_t * glClear;

#define GLF_CLEARCOLOR( name ) void name( GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha )
typedef GLF_CLEARCOLOR( glClearColor_t );
FP_DEF
glClearColor_t * glClearColor;

#define GLF_CLEARDEPTH( name ) void name( GLclampd depth )
typedef GLF_CLEARDEPTH( glClearDepth_t );
FP_DEF
glClearDepth_t * glClearDepth;

#define GLF_CLEARDEPTHF( name ) void name( GLfloat depth )
typedef GLF_CLEARDEPTHF( glClearDepthf_t );
FP_DEF
glClearDepthf_t * glClearDepthf;

#define GLF_VIEWPORT( name ) void name( GLint x, GLint y, GLsizei width, GLsizei height )
typedef GLF_VIEWPORT( glViewport_t );
FP_DEF
glViewport_t * glViewport;

#define GLF_DRAWARRAYS( name ) void name( GLenum mode, GLint first, GLsizei count )
typedef GLF_DRAWARRAYS( glDrawArrays_t );
FP_DEF
glDrawArrays_t * glDrawArrays;

#define GLF_DRAWELEMENTS( name ) void name( GLenum mode, GLsizei count, GLenum type, const GLvoid *indices )
typedef GLF_DRAWELEMENTS( glDrawElements_t );
FP_DEF
glDrawElements_t * glDrawElements;

#define GLF_DRAWPOLYGONMODE( name ) void name( GLenum face, GLenum mode )
typedef GLF_DRAWPOLYGONMODE( glPolygonMode_t );
FP_DEF
glPolygonMode_t * glPolygonMode;

#define GLF_CULLFACE( name ) void name( GLenum mode )
typedef GLF_CULLFACE( glCullFace_t );
FP_DEF
glCullFace_t * glCullFace;

#define GLF_USEPROGRAM( name ) void name( GLuint program )
typedef GLF_USEPROGRAM( glUseProgram_t );
FP_DEF
glUseProgram_t * glUseProgram;

#define GLF_ENABLEVERTEXATTRIBARRAY( name ) void name( GLuint index )
typedef GLF_ENABLEVERTEXATTRIBARRAY( glEnableVertexAttribArray_t );
FP_DEF
glEnableVertexAttribArray_t * glEnableVertexAttribArray;

#define GLF_DISABLEVERTEXATTRIBARRAY( name ) void name( GLuint index )
typedef GLF_DISABLEVERTEXATTRIBARRAY( glDisableVertexAttribArray_t );
FP_DEF
glDisableVertexAttribArray_t * glDisableVertexAttribArray;

#define GLF_VERTEXATTRIBPOINTER( name ) void name( GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer )
typedef GLF_VERTEXATTRIBPOINTER( glVertexAttribPointer_t );
FP_DEF
glVertexAttribPointer_t * glVertexAttribPointer;

#define GLF_BINDBUFFER( name ) void name( GLenum target, GLuint buffer )
typedef GLF_BINDBUFFER( glBindBuffer_t );
FP_DEF
glBindBuffer_t * glBindBuffer;

#define GLF_GLBUFFERDATA( name ) void name( GLenum target, GLsizeiptr size, const void *data, GLenum usage )
typedef GLF_GLBUFFERDATA( glBufferData_t );
FP_DEF
glBufferData_t * glBufferData;

#define GLF_UNIFORMMATRIX3FV( name ) void name( GLint location, GLsizei count, GLboolean transpose, const GLfloat *value )
typedef GLF_UNIFORMMATRIX3FV( glUniformMatrix3fv_t );
FP_DEF
glUniformMatrix3fv_t * glUniformMatrix3fv;

#define GLF_UNIFORMMATRIX4FV( name ) void name( GLint location, GLsizei count, GLboolean transpose, const GLfloat *value )
typedef GLF_UNIFORMMATRIX4FV( glUniformMatrix4fv_t );
FP_DEF
glUniformMatrix4fv_t * glUniformMatrix4fv;

#define GLF_UNIFORM4FV( name ) void name( GLint location, GLsizei count, const GLfloat *value )
typedef GLF_UNIFORM4FV( glUniform4fv_t );
FP_DEF
glUniform4fv_t * glUniform4fv;

#define GLF_UNIFORM4F( name ) void name( GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3 )
typedef GLF_UNIFORM4F( glUniform4f_t );
FP_DEF
glUniform4f_t * glUniform4f;

#define GLF_CHECKFRAMEBUFFERSTATUS( name ) GLenum name( GLenum target )
typedef GLF_CHECKFRAMEBUFFERSTATUS( glCheckFramebufferStatus_t );
FP_DEF
glCheckFramebufferStatus_t * glCheckFramebufferStatus;

#define GLF_BINDFRAMEBUFFER( name ) void name( GLenum target, GLuint framebuffer )
typedef GLF_BINDFRAMEBUFFER( glBindFramebuffer_t );
FP_DEF
glBindFramebuffer_t * glBindFramebuffer;

#define GLF_DEPTHMASK( name ) void name( GLboolean flag )
typedef GLF_DEPTHMASK( glDepthMask_t );
FP_DEF
glDepthMask_t * glDepthMask;

#define GLF_ENABLE( name ) void name( GLenum cap )
typedef GLF_ENABLE( glEnable_t );
FP_DEF
glEnable_t * glEnable;

#define GLF_DISABLE( name ) void name( GLenum cap )
typedef GLF_DISABLE( glDisable_t );
FP_DEF
glDisable_t * glDisable;

#define GLF_ACTIVETEXTURE( name ) void name( GLenum texture )
typedef GLF_ACTIVETEXTURE( glActiveTexture_t );
FP_DEF
glActiveTexture_t * glActiveTexture;

#define GLF_BINDTEXTURE( name ) void name( GLenum target, GLuint texture )
typedef GLF_BINDTEXTURE( glBindTexture_t );
FP_DEF
glBindTexture_t * glBindTexture;

#define GLF_UNIFORM1I( name ) void name( GLint location, GLint v0 )
typedef GLF_UNIFORM1I( glUniform1i_t );
FP_DEF
glUniform1i_t * glUniform1i;

#define GLF_VERTEXATTRIBDIVISOR( name ) void name( GLuint index, GLuint divisor )
typedef GLF_VERTEXATTRIBDIVISOR( glVertexAttribDivisor_t );
FP_DEF
glVertexAttribDivisor_t * glVertexAttribDivisor;

#define GLF_BUFFERSUBDATA( name ) void name( GLenum target, GLintptr offset, GLsizeiptr size, const void *data )
typedef GLF_BUFFERSUBDATA( glBufferSubData_t );
FP_DEF
glBufferSubData_t * glBufferSubData;

#define GLF_DEPTHFUNC( name ) void name( GLenum func )
typedef GLF_DEPTHFUNC( glDepthFunc_t );
FP_DEF
glDepthFunc_t * glDepthFunc;

#define GLF_BLENDFUNC( name ) void name( GLenum sfactor, GLenum dfactor )
typedef GLF_BLENDFUNC( glBlendFunc_t );
FP_DEF
glBlendFunc_t * glBlendFunc;

#define GLF_BLENDEQUATION( name ) void name( GLenum mode )
typedef GLF_BLENDEQUATION( glBlendEquation_t );
FP_DEF
glBlendEquation_t * glBlendEquation;

#define GLF_BLENDFUNCSEPARATE( name ) void name( GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha )
typedef GLF_BLENDFUNCSEPARATE( glBlendFuncSeparate_t );
FP_DEF
glBlendFuncSeparate_t * glBlendFuncSeparate;

#define GLF_VERTEXATTRIBIPOINTER( name ) void name( GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer )
typedef GLF_VERTEXATTRIBIPOINTER( glVertexAttribIPointer_t );
FP_DEF
glVertexAttribIPointer_t * glVertexAttribIPointer;

#define GLF_ISRENDERBUFFER( name ) GLboolean name( GLuint renderbuffer )
typedef GLF_ISRENDERBUFFER( glIsRenderbuffer_t );
FP_DEF
glIsRenderbuffer_t * glIsRenderbuffer;

#define GLF_ISFRAMEBUFFER( name ) GLboolean name( GLuint framebuffer )
typedef GLF_ISFRAMEBUFFER( glIsFramebuffer_t );
FP_DEF
glIsFramebuffer_t * glIsFramebuffer;

#define GLF_GENFRAMEBUFFERS( name ) void name( GLsizei n, GLuint *framebuffers )
typedef GLF_GENFRAMEBUFFERS( glGenFramebuffers_t );
FP_DEF
glGenFramebuffers_t * glGenFramebuffers;

#define GLF_GENRENDERBUFFERS( name ) void name( GLsizei n, GLuint *renderbuffers )
typedef GLF_GENRENDERBUFFERS( glGenRenderbuffers_t );
FP_DEF
glGenRenderbuffers_t * glGenRenderbuffers;

#define GLF_GENTEXTURES( name ) void name( GLsizei n, GLuint *textures )
typedef GLF_GENTEXTURES( glGenTextures_t );
FP_DEF
glGenTextures_t * glGenTextures;

#define GLF_TEXPARAMETERI( name ) void name( GLenum target, GLenum pname, GLint param )
typedef GLF_TEXPARAMETERI( glTexParameteri_t );
FP_DEF
glTexParameteri_t * glTexParameteri;

#define GLF_TEXPARAMETERIV( name ) void name( GLenum target, GLenum pname, const GLint *params )
typedef GLF_TEXPARAMETERIV( glTexParameteriv_t );
FP_DEF
glTexParameteriv_t * glTexParameteriv;

#define GLF_GETTEXLEVELPARAMETERIV( name ) void name( GLenum target, GLint level, GLenum pname, GLint *params )
typedef GLF_GETTEXLEVELPARAMETERIV( glGetTexLevelParameteriv_t );
FP_DEF
glGetTexLevelParameteriv_t * glGetTexLevelParameteriv;

#define GLF_GETRENDERBUFFERRAMETERIV( name ) void name( GLenum target, GLenum pname, GLint *params )
typedef GLF_GETRENDERBUFFERRAMETERIV( glGetRenderbufferParameteriv_t );
FP_DEF
glGetRenderbufferParameteriv_t * glGetRenderbufferParameteriv;

#define GLF_TEXIMAGE2D( name ) void name( GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid * pixels )
typedef GLF_TEXIMAGE2D( glTexImage2D_t );
FP_DEF
glTexImage2D_t * glTexImage2D;

#define GLF_GETERROR( name ) GLenum name(  )
typedef GLF_GETERROR( glGetError_t );
FP_DEF
glGetError_t * glGetError;

#define GLF_CREATESHADER( name ) GLuint name( GLenum type )
typedef GLF_CREATESHADER( glCreateShader_t );
FP_DEF
glCreateShader_t * glCreateShader;

#define GLF_ISPROGRAM( name ) GLboolean name( GLuint program )
typedef GLF_ISPROGRAM( glIsProgram_t );
FP_DEF
glIsProgram_t * glIsProgram;

#define GLF_CREATEPROGRAM( name ) GLuint name(  )
typedef GLF_CREATEPROGRAM( glCreateProgram_t );
FP_DEF
glCreateProgram_t * glCreateProgram;

#define GLF_SHADERSOURCE( name ) void name( GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length )
typedef GLF_SHADERSOURCE( glShaderSource_t );
FP_DEF
glShaderSource_t * glShaderSource;

#define GLF_COMPILESHADER( name ) void name( GLuint shader )
typedef GLF_COMPILESHADER( glCompileShader_t );
FP_DEF
glCompileShader_t * glCompileShader;

#define GLF_GETPROGRAMIV( name ) void name( GLuint program, GLenum pname, GLint *params )
typedef GLF_GETPROGRAMIV( glGetProgramiv_t );
FP_DEF
glGetProgramiv_t * glGetProgramiv;

#define GLF_GETSHADERIV( name ) void name( GLuint shader, GLenum pname, GLint *params )
typedef GLF_GETSHADERIV( glGetShaderiv_t );
FP_DEF
glGetShaderiv_t * glGetShaderiv;

#define GLF_GETPROGRAMINFOLOG( name ) void name( GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog )
typedef GLF_GETPROGRAMINFOLOG( glGetProgramInfoLog_t );
FP_DEF
glGetProgramInfoLog_t * glGetProgramInfoLog;

#define GLF_GETSHADERINFOLOG( name ) void name( GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog )
typedef GLF_GETSHADERINFOLOG( glGetShaderInfoLog_t );
FP_DEF
glGetShaderInfoLog_t * glGetShaderInfoLog;

#define GLF_DELETEPROGRAM( name ) void name( GLuint program )
typedef GLF_DELETEPROGRAM( glDeleteProgram_t );
FP_DEF
glDeleteProgram_t * glDeleteProgram;

#define GLF_DELETESHADER( name ) void name( GLuint shader )
typedef GLF_DELETESHADER( glDeleteShader_t );
FP_DEF
glDeleteShader_t * glDeleteShader;

#define GLF_ATTACHSHADER( name ) void name( GLuint program, GLuint shader )
typedef GLF_ATTACHSHADER( glAttachShader_t );
FP_DEF
glAttachShader_t * glAttachShader;

#define GLF_DETACHSHADER( name ) void name( GLuint program, GLuint shader )
typedef GLF_DETACHSHADER( glDetachShader_t );
FP_DEF
glDetachShader_t * glDetachShader;

#define GLF_ISTEXTURE( name ) GLboolean name( GLuint texture )
typedef GLF_ISTEXTURE( glIsTexture_t );
FP_DEF
glIsTexture_t * glIsTexture;

#define GLF_VALIDATEPROGRAM( name ) void name( GLuint program )
typedef GLF_VALIDATEPROGRAM( glValidateProgram_t );
FP_DEF
glValidateProgram_t * glValidateProgram;

#define GLF_GENBUFFERS( name ) void name( GLsizei n, GLuint *buffers )
typedef GLF_GENBUFFERS( glGenBuffers_t );
FP_DEF
glGenBuffers_t * glGenBuffers;

#define GLF_GETUNIFORMLOCATION( name ) GLint name( GLuint program, const GLchar *name )
typedef GLF_GETUNIFORMLOCATION( glGetUniformLocation_t );
FP_DEF
glGetUniformLocation_t * glGetUniformLocation;

#define GLF_GENVERTEXARRAYS( name ) void name( GLsizei n, GLuint *arrays )
typedef GLF_GENVERTEXARRAYS( glGenVertexArrays_t );
FP_DEF
glGenVertexArrays_t * glGenVertexArrays;

#define GLF_BINDVERTEXARRAY( name ) void name( GLuint array )
typedef GLF_BINDVERTEXARRAY( glBindVertexArray_t );
FP_DEF
glBindVertexArray_t * glBindVertexArray;

#define GLF_GETATTRIBLOCATION( name ) GLint name( GLuint program, const GLchar *name )
typedef GLF_GETATTRIBLOCATION( glGetAttribLocation_t );
FP_DEF
glGetAttribLocation_t * glGetAttribLocation;

#define GLF_GETINTEGERV( name ) void name( GLenum pname, GLint *params )
typedef GLF_GETINTEGERV( glGetIntegerv_t );
FP_DEF
glGetIntegerv_t * glGetIntegerv;

#define GLF_GETFRAMEBUFFERATTACHMENTPARAMETERIV( name ) void name( GLenum target, GLenum attachment, GLenum pname, GLint *params )
typedef GLF_GETFRAMEBUFFERATTACHMENTPARAMETERIV( glGetFramebufferAttachmentParameteriv_t );
FP_DEF
glGetFramebufferAttachmentParameteriv_t * glGetFramebufferAttachmentParameteriv;

#define GLF_DEBUGMESSAGECONTROL( name ) void name( GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled )
typedef GLF_DEBUGMESSAGECONTROL( glDebugMessageControl_t );
FP_DEF
glDebugMessageControl_t * glDebugMessageControl;

#define GLDEBUGPROC( name ) void name( GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message, const void *userParam );
typedef GLDEBUGPROC( gl_debug_proc_t );

#define GLF_DEBUGMESSAGECALLBACK( name ) void name( gl_debug_proc_t callback, const void *userParam )
typedef GLF_DEBUGMESSAGECALLBACK( glDebugMessageCallback_t );
typedef GLF_DEBUGMESSAGECALLBACK( glDebugMessageCallbackARB_t );
typedef GLF_DEBUGMESSAGECALLBACK( glDebugMessageCallbackAMD_t );
FP_DEF
glDebugMessageCallback_t * glDebugMessageCallback; OPTIONAL
FP_DEF
glDebugMessageCallbackARB_t * glDebugMessageCallbackARB; OPTIONAL
FP_DEF
glDebugMessageCallbackAMD_t * glDebugMessageCallbackAMD; OPTIONAL

#define GLF_LINKPROGRAM( name ) void name( GLuint program )
typedef GLF_LINKPROGRAM( glLinkProgram_t );
FP_DEF
glLinkProgram_t * glLinkProgram;

#define GLF_FRAMEBUFFERRENDERBUFFER( name ) void name( GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer )
typedef GLF_FRAMEBUFFERRENDERBUFFER( glFramebufferRenderbuffer_t );
FP_DEF
glFramebufferRenderbuffer_t * glFramebufferRenderbuffer;

#define GLF_FRAMEBUFFERTEXTURE2D( name ) void name( GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level )
typedef GLF_FRAMEBUFFERTEXTURE2D( glFramebufferTexture2D_t );
FP_DEF
glFramebufferTexture2D_t * glFramebufferTexture2D;

#define GLF_RENDERBUFFERSTORAGE( name ) void name( GLenum target, GLenum internalformat, GLsizei width, GLsizei height )
typedef GLF_RENDERBUFFERSTORAGE( glRenderbufferStorage_t );
FP_DEF
glRenderbufferStorage_t * glRenderbufferStorage;

#define GLF_DELETEFRAMEBUFFERS( name ) void name( GLsizei n, const GLuint *framebuffers )
typedef GLF_DELETEFRAMEBUFFERS( glDeleteFramebuffers_t );
FP_DEF
glDeleteFramebuffers_t * glDeleteFramebuffers;

#define GLF_DELETERENDERBUFFERS( name ) void name( GLsizei n, const GLuint *renderbuffers )
typedef GLF_DELETERENDERBUFFERS( glDeleteRenderbuffers_t );
FP_DEF
glDeleteRenderbuffers_t * glDeleteRenderbuffers;

#define GLF_BINDRENDERBUFFER( name ) void name( GLenum target, GLuint renderbuffer )
typedef GLF_BINDRENDERBUFFER( glBindRenderbuffer_t );
FP_DEF
glBindRenderbuffer_t * glBindRenderbuffer;

#define GLF_DRAWARRAYSINSTANCEDBASEINSTANCE( name ) void name( GLenum mode, GLint first, GLsizei count, GLsizei instancecount, GLuint baseinstance )
typedef GLF_DRAWARRAYSINSTANCEDBASEINSTANCE( glDrawArraysInstancedBaseInstance_t );
FP_DEF
glDrawArraysInstancedBaseInstance_t * glDrawArraysInstancedBaseInstance;

#define GLF_DRAWARRAYSINSTANCED( name ) void name( GLenum mode, GLint first, GLsizei count, GLsizei instancecount )
typedef GLF_DRAWARRAYSINSTANCED( glDrawArraysInstanced_t );
FP_DEF
glDrawArraysInstanced_t * glDrawArraysInstanced;

#define GLF_POINTSIZE( name ) void name( GLfloat size )
typedef GLF_POINTSIZE( glPointSize_t );
FP_DEF
glPointSize_t * glPointSize;



#if LOAD_THEM

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#define GLF_( name ) name(  )
typedef (  );

#endif

#undef FP_DEF
#endif
