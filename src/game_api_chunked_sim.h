#ifndef GAME_API_CHUNKED_SIM_H
#define GAME_API_CHUNKED_SIM_H 1

#define TG_STATIC_ASSERT(cond) typedef char static_assert_##__LINE__[(cond)?1:-1];

#include "basic_types.h"
#include "tgmath.h"
#include "game_api_lepre.h"

/*
   NOTE(theGiallo): for now we do not check for duplicates when we add an entity
   to a chunk, so it should be up to the simulation to avoid duplicates.

   The entity can be in multiple chunks, but it's simulated only by the owner.
 */

struct
Entity
{
	u64 identifier;
	u32 type;
	void * body;
	s32 owner_chunk_x,
	    owner_chunk_y;
};

struct
Entity_List_Node
{
	Entity * entity;
	Entity_List_Node * next;
};
typedef Entity_List_Node* Entity_List;

#define MH_TYPE Entity_List_Node
#include "mem_hotel.inc.h"

bool
add_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh );
bool
remove_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh );

struct
Entity_Hash_Table
{
	Entity_List table[1024];
	Entity_List_Node_MH list_mh;
};
bool
remove_entity( Entity * entity, Entity_Hash_Table * ht );
bool
add_entity( Entity * entity, Entity_Hash_Table * ht );

typedef Entity_Hash_Table Entity_Index;

inline
bool
add_entity_to_index( Entity * entity, Entity_Index * index )
{
	bool ret = add_entity( entity, index );

	return ret;
}

enum
class
Neighbors_ID : u8
{
	NORTH_WEST,
	NORTH,
	NORTH_EAST,
	WEST,
	EAST,
	SOUTH_WEST,
	SOUTH,
	SOUTH_EAST,
	// ---
	COUNT
};
struct
Sim_Chunk
{
	Entity_Index entities;
	s32 idx_x,idx_y;
	Sim_Chunk * neighbors[(u8)Neighbors_ID::COUNT];
};

void
simulate_chunk_fixed_timestep( Sim_Chunk * sim_chunk );

void
simulate_chunk( Sim_Chunk * sim_chunk );

#define ENTITY_FLOORS_COUNT 4096

#define MH_TYPE Entity
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

#define MH_TYPE Entity_List_Node
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

struct
Entities_By_Id
{
	Entity_List table[1024*1024];
	Entity_List_Node_MH_4096 list_mh;
};

struct
Entity_Storage
{
	Entity_MH_4096 mh;
	u64 seeds[2];

	Entities_By_Id entities_by_id;
};

// NOTE(theGiallo): we should not store entities by their pointer because
// doing that we cannot serialize the pointer.
// 1 - We could serialize the pointer relative to the game_mem.
// 2 - We also could store the pointer to the header and the identifier. Then
// at load we could query for the header pointer using the identifier. After
// that could the pointer change? It could change only after a deallocation and
// a deallocation should happen only if the entity is destroyed or if it's
// serialized. In the first case it should never reappear. In the second case
// it would be reloaded at de-serialization.

Entity *
recreate_entity_header( Entity_Storage * storage, u32 type, u64 identifier );

Entity *
create_entity_header( Entity_Storage * storage );

void
destroy_entity_header( Entity_Storage * storage, Entity * entity );

Entity *
find_entity_by_identifier( Entity_Storage * storage, u64 identifier );

////////////////////////////////////////////////////////////////////////////////

// TODO(theGiallo, 2016-10-16): make the type out of a hash of something so that
// one can create a new type of entity without worrying. Make also a table to
// get the string name out of the type value.
#define ENTITY_TYPE_EXAMPLE 0x1

struct
Entity_Example
{
	float v;
};

#define MH_TYPE Entity_Example
#include "mem_hotel.inc.h"

struct
Entity_Example_Storage
{
	Entity_Example_MH mh;
};

Entity *
create_entity_example( Entity_Storage * entity_storage, Entity_Example_Storage * storage );

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): collision detection

union
Sphere
{
	#pragma pack(push,0)
	struct
	{
		V3 center;
		f32 radius;
	};
	#pragma pack(pop)
	struct
	{
		f32 x,y,z;
		f32 _radius;
	};
};
TG_STATIC_ASSERT(sizeof(Sphere) == 16);
struct
AABB
{
	V3 center_pos;
	V3 half_dimensions;
};

// NOTE(theGiallo): the idea here is to have a Collisions storage and Entities
// to point to a Collisions in there.
// NOTE(theGiallo): simulation idea
// Entities move ( dynamic physics simulation step )
// collisions are checked
// Entities that are colliding have to move back if are relatively solid, and
//    resolve the collision. This could mean simulate an impact, fire sounds,
//    fire particles, or any other kind of effect. For this we need the Entity
//    pointer.
//    I want to try not to simulate again here, but to modify physics attributes
//    and demand the simulation to the next time-step.
enum class
Collidable_Type : u8
{
	SPHERE,
	AABB,
	OOBB,
	CONVEX_POLYGON,
};
struct
Collidable
{
	Collidable_Type type;
	Entity * owner;
};
struct
Collidable_Sphere
{
	Collidable collidable;
	Sphere sphere;
};
struct
Collidable_AABB
{
	Collidable collidable;
	AABB aabb;
};

struct
Contact
{
	V3 world_pos;
};
struct
Collision
{
	Collidable * collidable;
	// TODO(theGiallo, 2016-11-03): contacts should be stored in an external
	// storage or should we store here a max amount?
	Contact * contacts;
	u32 contacts_count;
};
struct
Collision_List_Node
{
	Collision collision;
	Collision_List_Node * next;
};

struct
Collisions
{
	Collision_List_Node * collisions_list;
};

struct
Bounding_Sphere
{
	Sphere sphere;
	Collidable * real_collidable;
	Collisions * collisions;
};
struct
Bounding_Sphere_List_Node
{
	Bounding_Sphere * bounding_sphere;
	Bounding_Sphere_List_Node * next;
};

struct
Bounding_Sphere_Multigrid_Layer
{
	// NOTE(theGiallo): please make this pointer cache-aligned
	Bounding_Sphere_List_Node ** grid;
	u32 grid_side;
};

struct
Bounding_Sphere_Multigrid
{
	Bounding_Sphere_Multigrid_Layer * layers;
	u32 layers_count;
	f32 side_meters;
};

inline
Bounding_Sphere_List_Node **
grid_element( Bounding_Sphere_Multigrid_Layer * bsml, u32 x, u32 y, u32 z )
{
	Bounding_Sphere_List_Node ** ret;

	u32 grid_side = bsml->grid_side;
	if ( grid_side > 32 )
	{
		// NOTE(theGiallo): grid is organized in 2x2x2 chunks,
		// being pointers 8bytes a chunk is 64bytes,
		// usually the size of a cache line
		// TODO(theGiallo, 2016-11-02): cache the cachable values at creation time
		const u32 chunk_side = 2;
		const u32 chunk_volume = cube( chunk_side );
		u32 chunk_x = x / chunk_side;
		u32 chunk_y = y / chunk_side;
		u32 chunk_z = z / chunk_side;
		u32 grid_chunk_side = grid_side / chunk_side;
		lpr_assert( grid_side % chunk_side == 0 );
		ret =
		bsml->grid + ( chunk_x * chunk_volume +
		               chunk_y * grid_chunk_side * chunk_volume +
		               chunk_z * squareu( grid_chunk_side ) * chunk_volume +
		               x % chunk_side +
		               y % chunk_side +
		               z % chunk_side );
	} else
	{
		ret = bsml->grid + ( x + y * grid_side + z * squareu( grid_side ) );
	}

	return ret;
}

void
insert( Bounding_Sphere_Multigrid * bsm, Bounding_Sphere * bounding_sphere );

// NOTE(theGiallo): bs is the bounding sphere previously inserted, with the old
// values. The sphere is removed from the multigrid, its values are replaced
// with the new from new_bs_value, and it is inserted again.
void
move( Bounding_Sphere_Multigrid * bsm,
      Bounding_Sphere * bs, Bounding_Sphere * new_bs_value );

void
collision_check_everything( Bounding_Sphere_Multigrid * bsm );

#endif
