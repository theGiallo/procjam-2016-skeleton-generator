#include "game_api_opengl_tools.h"

// NOTE(theGiallo): h in [0,1)
Lpr_Rgba_f32
rgb_from_hsv( Lpr_Rgba_f32 hsv )
{
	Lpr_Rgba_f32 ret;
	ret.a = hsv.a;
	f32 c = hsv.g * hsv.b;
	f32 hh = hsv.r * 6.0;

	f32 x = c * ( 1.0 - abs( hh - 2.0f * floorf( hh / 2.0f ) - 1.0 ) );

	if ( hh < 1.0 || hh >= 6.0 )
	{
		ret.r = c;
		ret.g = x;
		ret.b = 0;
	} else
	if ( hh < 2.0 )
	{
		ret.r = x;
		ret.g = c;
		ret.b = 0;
	} else
	if ( hh < 3.0 )
	{
		ret.r = 0;
		ret.g = c;
		ret.b = x;
	} else
	if ( hh < 4.0 )
	{
		ret.r = 0;
		ret.g = x;
		ret.b = c;
	} else
	if ( hh < 5.0 )
	{
		ret.r = x;
		ret.g = 0;
		ret.b = c;
	} else
	if ( hh < 6.0 )
	{
		ret.r = c;
		ret.g = 0;
		ret.b = x;
	}

	f32 m = hsv.b - c;
	ret.r += m;
	ret.g += m;
	ret.b += m;

	return ret;
}

V3
rgb_from_v2( V2 v )
{
	f32 hue = angle_rad( v ) / TAU_F;
	Lpr_Rgba_f32 col = rgb_from_hsv( Lpr_Rgba_f32{hue, 0.7f, 1.0f, 1.0f} );
	V3 ret = { col.r, col.g, col.b };
	return ret;
}

inline
void
initialize_camera( Camera3D * camera, f32 inverse_aspect_ratio )
{
	camera->pos  = V3{};
	camera->look = V3{1.0f,0.0f,0.0f};
	camera->up   = V3{0.0f,0.0f,1.0f};
	update_view( camera );
	set_projection( camera, 0.1f, V2{0.5f,0.5f}, 90,
	                inverse_aspect_ratio );
}

inline
void
resize_camera( Camera3D * camera, u32 width, u32 height )
{
	f32 inverse_aspect_ratio =
	   (f32)height / (f32)width;
	set_projection( camera, camera->near, camera->vanishing_point, camera->fov,
	                inverse_aspect_ratio );
}

inline
void
update_view( Camera3D * camera )
{
	camera->V = view_mx4( camera->look, camera->up, camera->pos );
}
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, V2 size )
{
	camera->P = perspective_infinite_mx4( near, vanishing_point, size );
	camera->fov = fov_from_near_plane_size( near, size.w );
	camera->size = size;
	camera->vanishing_point = vanishing_point;
	camera->near = near;
}
inline
void
set_projection( Camera3D * camera, f32 near, V2 vanishing_point, f32 fov, f32 inverse_aspect_ratio )
{
#if 1
	camera->P =
	   perspective_infinite_mx4( near, vanishing_point, fov,
	                             inverse_aspect_ratio, &camera->size );
#else
	f32 far = 10000;
	camera->P =
	   perspective_mx4( near, far, vanishing_point, fov,
	                    inverse_aspect_ratio, &camera->size );
#endif
	camera->near = near;
	camera->vanishing_point = vanishing_point;
	camera->fov = fov;
	camera->inverse_aspect_ratio = inverse_aspect_ratio;
}

inline
V2u32
compute_window_center( Game_Memory * game_mem )
{
	V2u32 ret =
	   make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
	ret /= 2;
	return ret;
}

inline
void
warp_mouse_to_center( System_API * sys_api, Game_Memory * game_mem )
{
	V2u32 origin =
	   make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
	origin /= 2;
	sys_api->warp_mouse_in_window(
	   game_mem->main_window.window_id, origin.x,origin.y );
}

// NOTE(theGiallo):
// dir should be positive if the key is ahead or right
// dir should be negative if the key is back or left
inline
void
manage_motion_key( Game_Memory * game_mem, Phys_Key key, s32 dir, f32 * var )
{
	//s32 is_pressed = (s32)game_mem->inputs.keyboard_state[key];
	s32 pre_rel =
	(s32)game_mem->inputs.keyboard_pressions[key] -
	(s32)game_mem->inputs.keyboard_releases [key];
	if ( pre_rel > 0 )
	{
		*var += 1.0f * sign( dir );
	} else
	if ( pre_rel < 0 )
	{
		*var -= 1.0f * sign( dir );
	}
}

// NOTE(theGiallo): ahead, right and up are supposed to be normalized
// motion values are supposed to be +1, 0 or-1
inline
void
apply_controlled_motion( Controlled_Motion motion, V3 ahead, V3 right, V3 up,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time, V3 * pos )
{
	*pos +=
	(
	   ahead *
	      ( motion.straight *
	         ( motion.straight > 0.0f ?
	            distributed_speed->positive.straight :
	            distributed_speed->negative.straight ) ) +
	   right *
	      ( motion.side     *
	         ( motion.side > 0.0f ?
	            distributed_speed->positive.side :
	            distributed_speed->negative.side )     ) +
	   up    *
	      ( motion.vertical *
	         ( motion.vertical > 0.0f ?
	            distributed_speed->positive.vertical :
	            distributed_speed->negative.vertical ) )
	) * ( delta_time * speed_mod );
}

inline
void
apply_controlled_motion( Controlled_Motion motion, Camera3D * camera,
                         Distributed_Speed * distributed_speed, f32 speed_mod,
                         Time delta_time )
{
	apply_controlled_motion( motion,
	                         normalize( camera->look ),
	                         normalize( camera->look ^ camera->up ),
	                         camera->up,
	                         distributed_speed, speed_mod,
	                         delta_time, &camera->pos );
}

inline
void
per_frame_camera_controls_and_update(
   Game_Memory * game_mem, Camera3D * camera, Controlled_Motion * camera_motion,
   Distributed_Speed * distributed_camera_speed, f32 shift_speed_mod = 10.0f )
{
	manage_motion_key( game_mem, PK_W,      1, &camera_motion->straight );
	manage_motion_key( game_mem, PK_S,     -1, &camera_motion->straight );
	manage_motion_key( game_mem, PK_D,      1, &camera_motion->side     );
	manage_motion_key( game_mem, PK_A,     -1, &camera_motion->side     );
	manage_motion_key( game_mem, PK_SPACE,  1, &camera_motion->vertical );
	manage_motion_key( game_mem, PK_C,     -1, &camera_motion->vertical );

	f32 speed_mod = 1.0f;
	if ( game_mem->inputs.keyboard_state[PK_LSHIFT] )
	{
		speed_mod = shift_speed_mod;
	}

	apply_controlled_motion( *camera_motion, camera,
	                         distributed_camera_speed,
	                         speed_mod,
	                         game_mem->delta_time );

	f32 a = 5.0f * game_mem->delta_time * speed_mod;
	if (  game_mem->inputs.keyboard_state[PK_Q] &&
	     !game_mem->inputs.keyboard_state[PK_E] )
	{
		Mx4 rot_up =
		   rotation_mx4( camera->up, a );
		camera->look = normalize( rot_up * camera->look );
	}
	if ( !game_mem->inputs.keyboard_state[PK_Q] &&
	      game_mem->inputs.keyboard_state[PK_E] )
	{
		Mx4 rot_up =
		   rotation_mx4( camera->up, -a );
		camera->look = normalize( rot_up * camera->look );
	}

	update_view( camera );
}

inline
bool
fp_camera_mouse_motion( System_API * sys_api, Game_Memory * game_mem,
                        s32 pos_x, s32 pos_y,
                        s32 motion_x, s32 motion_y, Camera3D * camera )
{
	bool ret = false;
	V2u32 window_center = compute_window_center( game_mem );
	if ( pos_x != (s32)window_center.x ||
	     pos_y != (s32)window_center.y )
	{
		ret = true;
		V2 win_size =
		   (V2)make_V2u32( game_mem->main_window.lpr_draw_data.window_size );
		f32 angle_per_px = camera->fov / win_size.w;
		f32 u_a = -angle_per_px * motion_x;
		f32 r_a = -angle_per_px * motion_y;
		V3 right = normalize( camera->look ^ camera->up );
		Mx4 rot_right =
		   rotation_mx4( right, r_a );
		Mx4 rot_up =
		   rotation_mx4( camera->up, u_a );
		Mx4 rot = rot_up * rot_right;
		camera->look = normalize( rot * camera->look );
		update_view( camera );
		sys_api->warp_mouse_in_window(
		   game_mem->main_window.window_id,
		   window_center.x,window_center.y );
	}

	return ret;
}

inline
void
set_ogl_camera( Shader * shader, Camera3D * camera )
{
	glUseProgram( shader->program );
	if ( shader->VP_loc != -1 )
	{
		glUniformMatrix4fv( shader->VP_loc, 1, GL_FALSE,
		                    camera->VP.arr );
	}
	if ( shader->V_loc != -1 )
	{
		glUniformMatrix4fv( shader->V_loc, 1, GL_FALSE,
		                    camera->V.arr );
	}
	if ( shader->P_loc != -1 )
	{
		glUniformMatrix4fv( shader->P_loc, 1, GL_FALSE,
		                    camera->P.arr );
	}
}

void
render_colored_vertices( Shader * shader,
                         Colored_Vertex * vertices, u32 vertices_in_buffer,
                         u32 vertices_count,
                         Lpr_Rgba_f32 * default_color,
                         GLuint vbo, GLenum draw_mode, GLenum buffer_usage )
{
	glUseProgram( shader->program );
	if ( shader->color_loc != -1 && default_color )
	{
		glUniform4f( shader->color_loc, 0.0f, 0.05f, 0.4f, 0.5f );
	}
	u32 buffer_byte_size = vertices_in_buffer * sizeof( Colored_Vertex );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              NULL,  buffer_usage );
	glBufferData( GL_ARRAY_BUFFER, buffer_byte_size,
	              vertices,  buffer_usage );

	u32 stride = sizeof( Colored_Vertex );
	glEnableVertexAttribArray( shader->vertex_loc );
	glVertexAttribPointer( shader->vertex_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)0  // array buffer offset
	                     );

	glEnableVertexAttribArray( shader->vertex_color_loc );
	glVertexAttribPointer( shader->vertex_color_loc, // location
	                       3,        // size
	                       GL_FLOAT, // type
	                       GL_FALSE, // normalized
	                       stride,   // stride
	                       (void*)sizeof( V3 ) // array buffer offset
	                     );
	glDrawArrays( draw_mode, 0, vertices_count );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glUseProgram( 0 );
}

bool
create_framebuffer_with_depth( V2u32 px_size,
                               Frame_Buffer_With_Depth * fb )
{
	if ( ! glIsFramebuffer( fb->linear_fbo ) )
	{
		glGenFramebuffers( 1, &fb->linear_fbo );
	}
	glBindFramebuffer( GL_FRAMEBUFFER, fb->linear_fbo );
	if ( ! glIsRenderbuffer( fb->linear_color_rt ) )
	{
		glGenRenderbuffers( 1, &fb->linear_color_rt );
	}
	glBindRenderbuffer(    GL_RENDERBUFFER, fb->linear_color_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA16F, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                           GL_RENDERBUFFER, fb->linear_color_rt );

	glGenRenderbuffers( 1, &fb->depth_rt );
	glBindRenderbuffer(    GL_RENDERBUFFER, fb->depth_rt );
	glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, px_size.w, px_size.h );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
	                           GL_RENDERBUFFER, fb->depth_rt );

	// NOTE(theGiallo): Generate and bind the OGL texture for diffuse
	if ( ! glIsTexture( fb->linear_fb_texture ) )
	{
		glGenTextures( 1, &fb->linear_fb_texture );
	}
	glBindTexture( GL_TEXTURE_2D, fb->linear_fb_texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, px_size.w, px_size.h, 0,
	              GL_RGBA, GL_HALF_FLOAT, NULL );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	// NOTE(theGiallo): Attach the texture to the FBO
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                        GL_TEXTURE_2D, fb->linear_fb_texture, 0 );

	// NOTE(theGiallo): Check if all worked fine and unbind the FBO
	if( !lpr_check_and_log_framebuffer_status() )
	{
		lpr_log_err( "Something failed during FBO initialization. FBO is not complete." );
		// TODO(theGiallo, 2015/09/28): proper clean
		glDeleteFramebuffers( 1, &fb->linear_fbo );
		glDeleteRenderbuffers( 1, &fb->linear_color_rt );
		glDeleteRenderbuffers( 1, &fb->depth_rt );
		LPR_ILLEGAL_PATH();
		return false;
	}
	lpr_log_framebuffer_info();

	// NOTE(theGiallo): OpenGL cleanup
	glBindTexture( GL_TEXTURE_2D, 0 );
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	fb->size = px_size;

	return true;
}

bool
create_program_from_text( GLuint *     gl_program,
                          const char * geometry_shader_text,
                          GLint        geometry_shader_text_length,
                          const char * vertex_shader_text,
                          GLint        vertex_shader_text_length,
                          const char * fragment_shader_text,
                          GLint        fragment_shader_text_length )
{

	*gl_program = glCreateProgram( );
	GLuint gl_p = *gl_program;

	GLuint gl_shader_geometry,
	       gl_shader_vertex,
	       gl_shader_fragment;

	if ( geometry_shader_text )
	{
		gl_shader_geometry = glCreateShader( GL_GEOMETRY_SHADER   );
	}
	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );

	GLint compiled;
	int info_log_length;

	if ( ( geometry_shader_text && !gl_shader_geometry ) ||
	     !gl_shader_vertex || !gl_shader_fragment )
	{
		lpr_log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	if ( geometry_shader_text )
	{
		// NOTE(theGiallo): compile and attach geometry shader
		glShaderSource( gl_shader_geometry,                    // shader
		                1,                                     // count
		                (const GLchar**)&geometry_shader_text, // string
		                &geometry_shader_text_length );        // length
		glCompileShader( gl_shader_geometry );

		// NOTE(theGiallo): check geometry shader compilation
		compiled = GL_FALSE;
		info_log_length = 0;
		glGetShaderiv( gl_shader_geometry, GL_COMPILE_STATUS,  &compiled );
		glGetShaderiv( gl_shader_geometry, GL_INFO_LOG_LENGTH, &info_log_length );
		if ( info_log_length > 0 )
		{
			char shader_info_log[info_log_length+1];
			glGetShaderInfoLog( gl_shader_geometry, info_log_length, NULL,
			                    shader_info_log );
			lpr_log_info( "\ngeometry shader compilation infos (%dB):\n" \
			              "----------------------------------------\n" \
			              "%s\n" \
			              "----------------------------------------\n",
			              info_log_length,
			              shader_info_log );
		}
		if ( compiled == GL_FALSE )
		{
			lpr_log_err( "Error compiling geometry shader." );

			glDeleteProgram( gl_p );

			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
		// NOTE(theGiallo): compilation successfull => attach geometry shader
		glAttachShader( gl_p, gl_shader_geometry );
	}

	gl_shader_vertex   = glCreateShader( GL_VERTEX_SHADER   );
	gl_shader_fragment = glCreateShader( GL_FRAGMENT_SHADER );
	if ( !gl_shader_vertex || !gl_shader_fragment )
	{
		lpr_log_err( "Error creating shader objects." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}

	// NOTE(theGiallo): compile and attach vertex shader
	glShaderSource( gl_shader_vertex,                    // shader
	                1,                                   // count
	                (const GLchar**)&vertex_shader_text, // string
	                &vertex_shader_text_length );        // length
	glCompileShader( gl_shader_vertex );

	// NOTE(theGiallo): check vertex shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_vertex, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_vertex, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_vertex, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nVertex shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );

		return lpr_false;
	}
	// NOTE(theGiallo): compilation successfull => attach vertex shader
	glAttachShader( gl_p, gl_shader_vertex );


	// NOTE(theGiallo): compile and attach fragment shader
	glShaderSource( gl_shader_fragment,                    // shader
	                1,                                     // count
	                (const GLchar**)&fragment_shader_text, // string
	                &fragment_shader_text_length );        // length
	glCompileShader( gl_shader_fragment );

	// NOTE(theGiallo): check fragment shader compilation
	compiled = GL_FALSE;
	info_log_length = 0;
	glGetShaderiv( gl_shader_fragment, GL_COMPILE_STATUS,  &compiled );
	glGetShaderiv( gl_shader_fragment, GL_INFO_LOG_LENGTH, &info_log_length );
	if ( info_log_length > 0 )
	{
		char shader_info_log[info_log_length+1];
		glGetShaderInfoLog( gl_shader_fragment, info_log_length, NULL,
		                    shader_info_log );
		lpr_log_info( "\nFragment shader compilation infos (%dB):\n" \
		              "----------------------------------------\n" \
		              "%s\n" \
		              "----------------------------------------\n",
		              info_log_length,
		              shader_info_log );
	}
	if ( compiled == GL_FALSE )
	{
		lpr_log_err( "Error compiling vertex shader." );

		glDeleteProgram( gl_p );

		if ( geometry_shader_text )
		{
			glDetachShader( gl_p, gl_shader_geometry );
			glDeleteShader( gl_shader_geometry );
		}

		glDetachShader( gl_p, gl_shader_vertex );
		glDeleteShader( gl_shader_vertex );

		glDetachShader( gl_p, gl_shader_fragment );
		glDeleteShader( gl_shader_fragment );
		return false;
	}
	// NOTE(theGiallo): compilation successfull => attach fragment shader
	glAttachShader( gl_p, gl_shader_fragment );


	// NOTE(theGiallo): link the program
	glLinkProgram( gl_p );

	// NOTE(theGiallo): check the program linking
	{
		GLint link_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram linking infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_LINK_STATUS, &link_status );
		if ( link_status == GL_FALSE )
		{
			lpr_log_err( "Error linking the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	glValidateProgram( gl_p );

	// NOTE(theGiallo): check the program validation
	{
		GLint validation_status = GL_FALSE;
		int info_log_length;
		glGetProgramiv(gl_p, GL_INFO_LOG_LENGTH, &info_log_length);
		if ( info_log_length > 0 )
		{
			char program_info_log[info_log_length+1];
			glGetProgramInfoLog( gl_p, info_log_length, NULL,
			                     program_info_log );
			lpr_log_info( "\nProgram validation infos:\n\n%s\n", program_info_log );
		}
		glGetProgramiv( gl_p, GL_VALIDATE_STATUS, &validation_status );
		if ( validation_status == GL_FALSE )
		{
			lpr_log_err( "Error validating the program." );

			glDeleteProgram( gl_p );

			if ( geometry_shader_text )
			{
				glDetachShader( gl_p, gl_shader_geometry );
				glDeleteShader( gl_shader_geometry );
			}

			glDetachShader( gl_p, gl_shader_vertex );
			glDeleteShader( gl_shader_vertex );

			glDetachShader( gl_p, gl_shader_fragment );
			glDeleteShader( gl_shader_fragment );
			return false;
		}
	}

	// NOTE(theGiallo): we will never modify or reuse the shader objects, so we can detach
	// and delete them.

	if ( geometry_shader_text )
	{
		glDetachShader( gl_p, gl_shader_geometry );
		glDeleteShader( gl_shader_geometry );
	}

	glDetachShader( gl_p, gl_shader_vertex );
	glDeleteShader( gl_shader_vertex );

	glDetachShader( gl_p, gl_shader_fragment );
	glDeleteShader( gl_shader_fragment );

	return true;
}

bool
create_program( System_API * sys_api, Game_Memory * game_mem,
                const char * geom, const char * vert, const char * frag,
                GLuint * out_program )
{
	bool ret = false;

	s64 size_geom = {};
	u8 * mem_geom = {};
	if ( geom )
	{

		size_geom = sys_api->get_file_size( geom );
		if ( size_geom < 0 )
		{
			return ret;
		}

		u64 old_top = game_mem->mem_stack_tmp.first_free;
		mem_geom = game_mem->mem_stack_tmp.push( size_geom );
		if ( !mem_geom )
		{
			return ret;
		}

		s64 res = sys_api->read_entire_file( geom, mem_geom, size_geom );
		if ( SYS_API_RET_IS_ERROR( res ) )
		{
			lpr_log_err( "failed to read geometry shader file '%s', error:\n%s\n%s",
			             geom, sys_api->get_error_name( res ),
			             sys_api->get_error_description( res ) );
			game_mem->mem_stack_tmp.first_free = old_top;
			return ret;
		}
	}

	s64 size_vert = sys_api->get_file_size( vert );
	if ( size_vert < 0 )
	{
		return ret;
	}

	u64 old_top = game_mem->mem_stack_tmp.first_free;
	u8 * mem_vert = game_mem->mem_stack_tmp.push( size_vert );
	if ( !mem_vert )
	{
		return ret;
	}

	s64 res = sys_api->read_entire_file( vert, mem_vert, size_vert );
	if ( SYS_API_RET_IS_ERROR( res ) )
	{
		lpr_log_err( "failed to read vertex shader file '%s', error:\n%s\n%s",
		             vert, sys_api->get_error_name( res ),
		             sys_api->get_error_description( res ) );
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	s64 size_frag = sys_api->get_file_size( frag );
	if ( size_frag < 0 )
	{
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	u8 * mem_frag = game_mem->mem_stack_tmp.push( size_frag );
	if ( !mem_frag )
	{
		return ret;
	}
	res = sys_api->read_entire_file( frag, mem_frag, size_frag );
	if ( SYS_API_RET_IS_ERROR( res ) )
	{
		lpr_log_err( "failed to read fragment shader file '%s', error:\n%s\n%s",
		             vert, sys_api->get_error_name( res ),
		             sys_api->get_error_description( res ) );
		game_mem->mem_stack_tmp.first_free = old_top;
		return ret;
	}

	GLuint program = {};
	if ( create_program_from_text( &program,
	                               (const char*)mem_geom, size_geom,
	                               (const char*)mem_vert, size_vert,
	                               (const char*)mem_frag, size_frag ) )
	{
		ret = true;
		if ( glIsProgram( *out_program ) )
		{
			glDeleteProgram( *out_program );
		}
		*out_program = program;
	}

	game_mem->mem_stack_tmp.first_free = old_top;
	return ret;
}

