#ifndef SDL_PLATFORM_LEPRE_H
#define SDL_PLATFORM_LEPRE_H 1

#if defined(DEBUG)
	#define LPR_DEBUG 1
#endif

#ifndef LPR_APP_NAME
	#define LPR_APP_NAME "sdl platform"
#endif

#include "lepre.h"

#endif /* ifndef SDL_PLATFORM_LEPRE_H */
