#include "game_api_lepre.h"
#include "game_api_procjam2016.h"



// TODO(theGiallo, 2016-11-11): remove
#include "global_game_mem.h"



#define MH_EMIT_BODY 1
#define MH_BODY_ONLY 1
#define MH_FLOORS_COUNT 1024
#define MH_TYPE Bone
#include "mem_hotel.inc.h"
#define MH_FLOORS_COUNT 1024
#define MH_TYPE Articulation
#include "mem_hotel.inc.h"
#define MH_FLOORS_COUNT 1024
#define MH_TYPE Articulation_List_Node
#include "mem_hotel.inc.h"
#undef MH_BODY_ONLY
#undef MH_EMIT_BODY

#define MIN_BONE_LENGTH 0.01f;

Bone *
make_new_bone( Bone_MH_1024 * bones_mh )
{
	Bone * ret = allocate_clean( bones_mh );

	if ( ret )
	{
		lpr_assert( ret->articulations_list == NULL );

		ret->length = MIN_BONE_LENGTH;
	}

	return ret;
}

#define BIT_VALUE ((f64)pow(2.0,-61.0))

inline
internal
f32
decode_f32( Instruction * instruction )
{
	f32 ret;

	ret = (f32)( ( (f64)instruction->_s64 ) * BIT_VALUE );

	return ret;
}

inline
internal
f32
decode_f32_01_pos( Instruction * instruction )
{
	f32 ret;

	ret = (f32)( ( (f64)instruction->_u64 ) / (f64)( U64_MAX ) );
	lpr_assert( isfinite( ret ) );
	lpr_assert( !isnan( ret ) );

	return ret;
}

inline
internal
f32
decode_f32_01_signed( Instruction * instruction )
{
	f32 ret;

	ret = (f32)( ( (f64)instruction->_s64 ) / (f64)( 0x1L<<63L ) );

	return ret;
}

internal
f32
decode_length( Instruction * instruction )
{
	f32 ret = decode_f32( instruction );
	ret = lpr_abs( ret );
	return ret;
}

internal
f32
decode_move( Instruction * instruction )
{
	f32 ret = decode_f32_01_pos( instruction );
	return ret;
}

internal
V3
decode_rotation( Instruction * instructions )
{
	V3 ret;
	ret.x =
	   decode_f32_01_pos( instructions + 0 );
	ret.y =
	   decode_f32_01_pos( instructions + 1 );
	ret.z =
	   decode_f32_01_pos( instructions + 2 );
	ret *= 360.0f;
	lpr_assert( isfinite( ret.x ) );
	lpr_assert( isfinite( ret.y ) );
	lpr_assert( isfinite( ret.z ) );

	return ret;
}

internal
void
decode_add_bone( DNA * dna, Instruction * instructions, u32 * instructions_count, u32 * jump )
{
	f32 ic_f  = decode_f32_01_pos( instructions + 0 );
	f32 jmp_f = decode_f32_01_pos( instructions + 1 );

	u32 dna_instructions_count = dna->instructions_count;
	u32 dna_groups_count = dna_instructions_count/INSTRUCTION_GROUP_COUNT;
	u32 jmp_g = dna_groups_count * jmp_f;
	*jump = jmp_g * INSTRUCTION_GROUP_COUNT;
	*instructions_count = roundf( ic_f * ( dna_groups_count - jmp_g ) ) * INSTRUCTION_GROUP_COUNT;
}

internal
u64
decode_reg_value( Instruction * instruction )
{
	u64 ret = instruction->_u64;
	return ret;
}

internal
inline
u64
add_to_reg( u64 reg, u64 val )
{
	u64 ret = reg;
	if ( U64_MAX - reg < val )
	{
		ret = U64_MAX;
	} else
	{
		ret += val;
	}
	return ret;
}

internal
inline
u64
sub_to_reg( u64 reg, u64 val )
{
	u64 ret = reg;
	if ( reg < val )
	{
		ret = 0;
	} else
	{
		ret -= val;
	}
	return ret;
}

internal
inline
u64
mul_reg( u64 reg, u64 val )
{
	u64 ret = reg;
	if ( val && reg && (
	        reg > U64_MAX / val ||
	        val > U64_MAX / reg ) )
	{
		ret = U64_MAX;
	} else
	{
		u64 old = reg;
		ret *= val;
		lpr_assert( old <= ret ||
		            val == 0 );
	}
	return ret;
}

internal
inline
u64
div_reg( u64 reg, u64 val )
{
	u64 ret = reg;
	if ( val == 0 )
	{
		ret = U64_MAX;
	} else
	{
		ret /= val;
	}
	return ret;
}

void
generate_skeleton( Bone_MH_1024 * bones_mh, Articulation_MH_1024 * articulations_mh,
                   Articulation_List_Node_MH_1024 * articulation_ln_mh,
                   Bone ** bone_stack, Articulation ** articulation_stack,
                   Execution_State * execution_stack,
                   u32 stack_capacity,
                   Skeleton * skeleton )
{
	DNA * dna = &skeleton->dna;
	Instruction * one_past_last_instruction = dna->instructions + dna->instructions_count;

	s32 curr_stack_id = 0;
	memset( bone_stack,         0x0, sizeof(Bone*        )   * stack_capacity );
	memset( articulation_stack, 0x0, sizeof(Articulation*)   * stack_capacity );
	memset( execution_stack,    0x0, sizeof(Execution_State) * stack_capacity );
	articulation_stack[curr_stack_id] =
	skeleton->root_articulation = allocate_clean( articulations_mh );
	lpr_assert( articulation_stack[curr_stack_id] );
	skeleton->root_articulation->bone =
	bone_stack[curr_stack_id] = make_new_bone( bones_mh );
	lpr_assert( bone_stack[curr_stack_id] );

	execution_stack[curr_stack_id].dna = *dna;

	bool gate_before = false;
	bool bad_dna = false;
	bool gate_after = false;
	while ( curr_stack_id >= 0 && !bad_dna )
	{
		Execution_State * curr_state = execution_stack + curr_stack_id;
		Instruction instruction;
		instruction._enum = Instruction_enum::STOP;
		if ( curr_state->next_instruction_location < curr_state->dna.instructions_count )
		{
			if ( curr_state->dna.instructions + curr_state->next_instruction_location >=
			     one_past_last_instruction ||
			     curr_state->dna.instructions + curr_state->next_instruction_location <
			     dna->instructions )
			{
				if ( curr_state->dna.instructions + curr_state->next_instruction_location !=
				     one_past_last_instruction ||
				     curr_state->next_instruction_location > curr_state->dna.instructions_count ||
				     curr_stack_id != 0 )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
				}
			} else
			{
				instruction = curr_state->dna.instructions[curr_state->next_instruction_location];
			}
		}
		switch ( instruction._enum )
		{
			case Instruction_enum::ADD_LENGTH:
			//case Instruction_enum::SUB_LENGTH:
			{
				if ( curr_state->dna.instructions +
				        ( curr_state->next_instruction_location + 1 )
				     >= one_past_last_instruction )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				f32 v = decode_length(
				   curr_state->dna.instructions +
				      ( curr_state->next_instruction_location + 1 ) );
				switch( instruction._enum )
				{
					case Instruction_enum::ADD_LENGTH:
						bone_stack[curr_stack_id]->length += v;
						break;
				#if 0
					case Instruction_enum::SUB_LENGTH:
						bone_stack[curr_stack_id]->length -= v;
						break;
				#endif
					default:
						break;
				}
			}
				break;
			case Instruction_enum::ADD_ROTATION:
			case Instruction_enum::SUB_ROTATION:
			{
				if ( curr_state->dna.instructions +
				        ( curr_state->next_instruction_location + 2 )
				     >= one_past_last_instruction )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				lpr_assert( articulation_stack[curr_stack_id] );
				V3 v = decode_rotation(
				   curr_state->dna.instructions +
				      ( curr_state->next_instruction_location + 1 ) );
				switch ( instruction._enum )
				{
					case Instruction_enum::ADD_ROTATION:
						articulation_stack[curr_stack_id]->rotation += v;
						break;
					case Instruction_enum::SUB_ROTATION:
						articulation_stack[curr_stack_id]->rotation -= v;
						break;
					default:
						break;
				}
			}
				break;
			case Instruction_enum::ADD_BONE:
			{
				if ( curr_state->dna.instructions +
				        ( curr_state->next_instruction_location + 2 )
				     >= one_past_last_instruction )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				if ( curr_stack_id >= MAX_BONES_PER_STACK - 1 )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				++curr_stack_id;
				Articulation * art =
				articulation_stack[curr_stack_id] = allocate_clean( articulations_mh );
				if( !articulation_stack[curr_stack_id] )
				{
					lpr_log_dbg( "articulations full" );
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				lpr_assert( articulation_stack[curr_stack_id]->bone == NULL );
				bone_stack[curr_stack_id] =
				art->bone = make_new_bone( bones_mh );
				if ( !art->bone )
				{
					lpr_log_dbg( "bones full" );
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}

				Articulation_List_Node * aln = allocate_clean( articulation_ln_mh );
				if( !aln )
				{
					lpr_log_dbg( "articulations_ln full" );
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				lpr_assert( aln->articulation == NULL );
				lpr_assert( aln->next == NULL );
				aln->next =
				bone_stack[curr_stack_id-1]->articulations_list;
				bone_stack[curr_stack_id-1]->articulations_list = aln;
				aln->articulation = art;

				u32 ic, jmp;
				decode_add_bone(
				   &curr_state->dna,
				   curr_state->dna.instructions + ( curr_state->next_instruction_location + 1 ),
				   &ic, &jmp );
				execution_stack[curr_stack_id].next_instruction_location = 0;
				execution_stack[curr_stack_id].dna.instructions_count = ic;
				execution_stack[curr_stack_id].dna.instructions = execution_stack[curr_stack_id-1].dna.instructions + jmp;
				memcpy( execution_stack[curr_stack_id  ].registers,
				        execution_stack[curr_stack_id-1].registers,
				        sizeof ( execution_stack[curr_stack_id].registers[0] ) *
				        NUM_REGISTERS );
				// NOTE(theGiallo): segfaults here should be made impossible
			}
				break;
			case Instruction_enum::MOVE_UP:
			//case Instruction_enum::MOVE_DOWN:
			{
				if ( curr_state->dna.instructions +
				        ( curr_state->next_instruction_location + 1 )
				     >= one_past_last_instruction ||
				     !articulation_stack[curr_stack_id] )
				{
					bad_dna = true;
					lpr_log_dbg( "bad dna" );
					break;
				}
				f32 v = decode_move(
				   curr_state->dna.instructions +
				      ( curr_state->next_instruction_location + 1 ) );
				switch( instruction._enum )
				{
					case Instruction_enum::MOVE_UP:
						articulation_stack[curr_stack_id]->length_percentage_attachment +=
						( 1.0f - articulation_stack[curr_stack_id]->length_percentage_attachment ) * v;
						break;
				#if 0
					case Instruction_enum::MOVE_DOWN:
					{
						f32 lpa = articulation_stack[curr_stack_id]->length_percentage_attachment;
						articulation_stack[curr_stack_id]->length_percentage_attachment += ( 1.0f - lpa ) *  v;
					}
				#endif
						break;
					default:
						break;
				}
			}
				break;
			case Instruction_enum::SET_REG_0:
			case Instruction_enum::SET_REG_1:
			case Instruction_enum::SET_REG_2:
			case Instruction_enum::ADD_TO_REG_0:
			case Instruction_enum::ADD_TO_REG_1:
			case Instruction_enum::ADD_TO_REG_2:
			case Instruction_enum::SUB_TO_REG_0:
			case Instruction_enum::SUB_TO_REG_1:
			case Instruction_enum::SUB_TO_REG_2:
			case Instruction_enum::MUL_REG_0:
			case Instruction_enum::MUL_REG_1:
			case Instruction_enum::MUL_REG_2:
			case Instruction_enum::DIV_REG_0:
			case Instruction_enum::DIV_REG_1:
			case Instruction_enum::DIV_REG_2:
			{
				u64 v = decode_reg_value(
				   curr_state->dna.instructions +
				      ( curr_state->next_instruction_location + 1 ) );
				switch ( instruction._enum )
				{
					case Instruction_enum::SET_REG_0:
					case Instruction_enum::SET_REG_1:
					case Instruction_enum::SET_REG_2:
					{
						u32 r_id =
						   (u64)instruction._enum - (u64)Instruction_enum::SET_REG_0;
						execution_stack[curr_stack_id].registers[r_id]._u64 = v;
					}
						break;
					case Instruction_enum::ADD_TO_REG_0:
					case Instruction_enum::ADD_TO_REG_1:
					case Instruction_enum::ADD_TO_REG_2:
					{
						u32 r_id =
						   (u64)instruction._enum - (u64)Instruction_enum::ADD_TO_REG_0;
						execution_stack[curr_stack_id].registers[r_id]._u64 =
						   add_to_reg( execution_stack[curr_stack_id].registers[r_id]._u64, v );
					}
						break;
					case Instruction_enum::SUB_TO_REG_0:
					case Instruction_enum::SUB_TO_REG_1:
					case Instruction_enum::SUB_TO_REG_2:
					{
						u32 r_id =
						   (u64)instruction._enum - (u64)Instruction_enum::SUB_TO_REG_0;
						execution_stack[curr_stack_id].registers[r_id]._u64 =
						   sub_to_reg( execution_stack[curr_stack_id].registers[r_id]._u64, v );
					}
						break;
					case Instruction_enum::MUL_REG_0:
					case Instruction_enum::MUL_REG_1:
					case Instruction_enum::MUL_REG_2:
					{
						u32 r_id =
						   (u64)instruction._enum - (u64)Instruction_enum::MUL_REG_0;
						execution_stack[curr_stack_id].registers[r_id]._u64 =
						   mul_reg( execution_stack[curr_stack_id].registers[r_id]._u64, v );
					}
						break;
					case Instruction_enum::DIV_REG_0:
					case Instruction_enum::DIV_REG_1:
					case Instruction_enum::DIV_REG_2:
					{
						u32 r_id =
						   (u64)instruction._enum - (u64)Instruction_enum::DIV_REG_0;
						execution_stack[curr_stack_id].registers[r_id]._u64 =
						   div_reg( execution_stack[curr_stack_id].registers[r_id]._u64, v );
					}
						break;
					default:
						LPR_ILLEGAL_PATH();
						break;
				}
			}
				break;
			case Instruction_enum::ADD_REG_0_TO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				add_to_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::ADD_REG_0_TO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 =
				add_to_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::ADD_REG_1_TO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 =
				add_to_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::ADD_REG_1_TO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 +=
				add_to_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::ADD_REG_2_TO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 +=
				add_to_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::ADD_REG_2_TO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				add_to_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::SUB_REG_0_TO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				sub_to_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::SUB_REG_0_TO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 =
				sub_to_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::SUB_REG_1_TO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 =
				sub_to_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::SUB_REG_1_TO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 +=
				sub_to_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::SUB_REG_2_TO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 +=
				sub_to_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::SUB_REG_2_TO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				sub_to_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::MUL_REG_0_INTO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				mul_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::MUL_REG_0_INTO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 =
				mul_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::MUL_REG_1_INTO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 =
				mul_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::MUL_REG_1_INTO_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 +=
				mul_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::MUL_REG_2_INTO_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 +=
				mul_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::MUL_REG_2_INTO_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				mul_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_0_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				div_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_0_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 =
				div_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[0]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_1_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 =
				div_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_1_REG_2:
				execution_stack[curr_stack_id].registers[2]._u64 +=
				div_reg( execution_stack[curr_stack_id].registers[2]._u64,
				            execution_stack[curr_stack_id].registers[1]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_2_REG_0:
				execution_stack[curr_stack_id].registers[0]._u64 +=
				div_reg( execution_stack[curr_stack_id].registers[0]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::DIV_BY_REG_2_REG_1:
				execution_stack[curr_stack_id].registers[1]._u64 =
				div_reg( execution_stack[curr_stack_id].registers[1]._u64,
				            execution_stack[curr_stack_id].registers[2]._u64 );
				break;
			case Instruction_enum::IF_ZERO_REG_0:
			case Instruction_enum::IF_ZERO_REG_1:
			case Instruction_enum::IF_ZERO_REG_2:
			{
				u32 r_id =
				   (u64)instruction._enum - (u64)Instruction_enum::IF_ZERO_REG_0;
				if ( execution_stack[curr_stack_id].registers[r_id]._u64 != 0 )
				{
					curr_state->next_instruction_location += INSTRUCTION_GROUP_COUNT;
				}
			}
				break;
			case Instruction_enum::IF_NOT_ZERO_REG_0:
			case Instruction_enum::IF_NOT_ZERO_REG_1:
			case Instruction_enum::IF_NOT_ZERO_REG_2:
			{
				u32 r_id =
				   (u64)instruction._enum - (u64)Instruction_enum::IF_ZERO_REG_0;
				if ( execution_stack[curr_stack_id].registers[r_id]._u64 == 0 )
				{
					curr_state->next_instruction_location += INSTRUCTION_GROUP_COUNT;
				}
			}
				break;
				break;
			case Instruction_enum::STOP:
				--curr_stack_id;
				break;
			default:
				bad_dna = true;
				lpr_log_dbg( "bad dna" );
				break;
		}
		curr_state->next_instruction_location += INSTRUCTION_GROUP_COUNT;
	}

	lpr_assert( !gate_before );
	lpr_assert( !gate_after  );
	if ( bad_dna )
	{
		dna->bad_dna = true;
	}
}

void
deallocate_skeleton( Skeleton * skeleton,
                     Bone_MH_1024 * bones_mh, Articulation_MH_1024 * articulations_mh,
                     Articulation_List_Node_MH_1024 * articulation_ln_mh,
                     Bone ** bone_stack, Articulation_List_Node ** articulation_ln_stack,
                     u32 stack_capacity )
{
	if ( !skeleton->root_articulation )
	{
		return;
	}
	memset( bone_stack,            0x0, sizeof(Bone*                  ) * stack_capacity );
	memset( articulation_ln_stack, 0x0, sizeof(Articulation_List_Node*) * stack_capacity );
	s32 stack_top = 0;
	bone_stack[stack_top] = skeleton->root_articulation->bone;

	bool backtracked = false;
	while ( stack_top >= 0 )
	{
		lpr_assert( (u32)stack_top < stack_capacity );
		if ( !articulation_ln_stack[stack_top] )
		{
			if ( !backtracked )
			{
				// NOTE(theGiallo): add bone to draw buffer
				if ( bone_stack[stack_top] && bone_stack[stack_top]->articulations_list )
				{
					articulation_ln_stack[stack_top] = bone_stack[stack_top]->articulations_list;
				} else
				{
					if ( bone_stack[stack_top] )
					{
						deallocate( bones_mh, bone_stack[stack_top] );
						DEBUG_ONLY( bone_stack[stack_top]->articulations_list = NULL; )
					}
					backtracked = true;
					--stack_top;
				}
			} else
			{
				if ( bone_stack[stack_top] )
				{
					deallocate( bones_mh, bone_stack[stack_top] );
					DEBUG_ONLY( bone_stack[stack_top]->articulations_list = NULL; )
				}
				backtracked = true;
				--stack_top;
			}
		} else
		{
			lpr_assert( stack_top + 1 < (s32)stack_capacity );
			bone_stack[stack_top+1] = articulation_ln_stack[stack_top]->articulation->bone;
			lpr_assert( !articulation_ln_stack[stack_top+1] );
			Articulation_List_Node * to_del_ln = articulation_ln_stack[stack_top];
			Articulation * to_del_art = to_del_ln->articulation;
			articulation_ln_stack[stack_top] = articulation_ln_stack[stack_top]->next;
			deallocate( articulation_ln_mh, to_del_ln );
			DEBUG_ONLY( to_del_ln->articulation = NULL; )
			DEBUG_ONLY( to_del_ln->next = NULL; )
			if ( to_del_art )
			{
				deallocate( articulations_mh, to_del_art );
				DEBUG_ONLY( to_del_art->bone = NULL; )
			}
			backtracked = false;
			++stack_top;
		}
	}

	deallocate( articulations_mh, skeleton->root_articulation );
}

void
generate_dna_and_skeleton( Skeleton * skeleton, u64 seeds[2], Procjam * pj )
{
	skeleton->dna.bad_dna = false;
	Instruction * instructions = skeleton->dna.instructions;
	u32 instructions_count = skeleton->dna.instructions_count;
	s32 number_of_tries = 0;
	do
	{
		deallocate_skeleton( skeleton,
		                     &pj->bones_mh, &pj->articulations_mh,
		                     &pj->articulation_ln_mh,
		                     pj->bone_stack,
		                     pj->articulation_ln_stack,
		                     ARRAY_COUNT(pj->bone_stack) );
		lpr_assert( pj->bones_mh          .total_occupancy == 0 );
		lpr_assert( pj->articulations_mh  .total_occupancy == 0 );
		lpr_assert( pj->articulation_ln_mh.total_occupancy == 0 );

		for ( u32 i = 0; i!=instructions_count; )
		{
			Instruction inst;
			// TODO(theGiallo, 2016-11-10): 1 means no STOP instruction is generated
			inst._s64 = (s64)randu_between( seeds, 1, (u32)Instruction_enum::_COUNT - 1 );
			bool inserted = false;
			switch ( inst._enum )
			{
				case Instruction_enum::MOVE_UP:
				//case Instruction_enum::MOVE_DOWN:
				case Instruction_enum::ADD_LENGTH:
				//case Instruction_enum::SUB_LENGTH:
					if ( i < 1024 - 1 )
					{
						instructions[i+0] = (Instruction)inst;
						instructions[i+1]._u64 = lpr_xorshift128plus( seeds );
						inserted = true;
					}
					break;
				case Instruction_enum::ADD_BONE:
					if ( i < 1024 - 2 )
					{
						instructions[i+0] = (Instruction)inst;

						instructions[i+1]._u64 = lpr_xorshift128plus( seeds );
						instructions[i+2]._u64 = lpr_xorshift128plus( seeds );

						inserted = true;
					}
					break;
				case Instruction_enum::ADD_ROTATION:
				case Instruction_enum::SUB_ROTATION:
					if ( i < 1024 - 3 )
					{
						instructions[i+0] = (Instruction)inst;
						instructions[i+1]._u64 = lpr_xorshift128plus( seeds );
						instructions[i+2]._u64 = lpr_xorshift128plus( seeds );
						instructions[i+3]._u64 = lpr_xorshift128plus( seeds );
						inserted = true;
					}
					break;
				case Instruction_enum::STOP:
					instructions[i+0] = (Instruction)inst;
					inserted = true;
					break;
				case Instruction_enum::SET_REG_0:
				case Instruction_enum::SET_REG_1:
				case Instruction_enum::SET_REG_2:
				case Instruction_enum::ADD_TO_REG_0:
				case Instruction_enum::ADD_TO_REG_1:
				case Instruction_enum::ADD_TO_REG_2:
				case Instruction_enum::SUB_TO_REG_0:
				case Instruction_enum::SUB_TO_REG_1:
				case Instruction_enum::SUB_TO_REG_2:
				case Instruction_enum::MUL_REG_0:
				case Instruction_enum::MUL_REG_1:
				case Instruction_enum::MUL_REG_2:
				case Instruction_enum::DIV_REG_0:
				case Instruction_enum::DIV_REG_1:
				case Instruction_enum::DIV_REG_2:
					instructions[i+0] = (Instruction)inst;
					instructions[i+1]._u64 = lpr_xorshift128plus( seeds );
					inserted = true;
						break;
				case Instruction_enum::ADD_REG_0_TO_REG_1:
				case Instruction_enum::ADD_REG_0_TO_REG_2:
				case Instruction_enum::ADD_REG_1_TO_REG_0:
				case Instruction_enum::ADD_REG_1_TO_REG_2:
				case Instruction_enum::ADD_REG_2_TO_REG_0:
				case Instruction_enum::ADD_REG_2_TO_REG_1:
				case Instruction_enum::SUB_REG_0_TO_REG_1:
				case Instruction_enum::SUB_REG_0_TO_REG_2:
				case Instruction_enum::SUB_REG_1_TO_REG_0:
				case Instruction_enum::SUB_REG_1_TO_REG_2:
				case Instruction_enum::SUB_REG_2_TO_REG_0:
				case Instruction_enum::SUB_REG_2_TO_REG_1:
				case Instruction_enum::MUL_REG_0_INTO_REG_1:
				case Instruction_enum::MUL_REG_0_INTO_REG_2:
				case Instruction_enum::MUL_REG_1_INTO_REG_0:
				case Instruction_enum::MUL_REG_1_INTO_REG_2:
				case Instruction_enum::MUL_REG_2_INTO_REG_0:
				case Instruction_enum::MUL_REG_2_INTO_REG_1:
				case Instruction_enum::DIV_BY_REG_0_REG_1:
				case Instruction_enum::DIV_BY_REG_0_REG_2:
				case Instruction_enum::DIV_BY_REG_1_REG_0:
				case Instruction_enum::DIV_BY_REG_1_REG_2:
				case Instruction_enum::DIV_BY_REG_2_REG_0:
				case Instruction_enum::DIV_BY_REG_2_REG_1:
				case Instruction_enum::IF_ZERO_REG_0:
				case Instruction_enum::IF_ZERO_REG_1:
				case Instruction_enum::IF_ZERO_REG_2:
				case Instruction_enum::IF_NOT_ZERO_REG_0:
				case Instruction_enum::IF_NOT_ZERO_REG_1:
				case Instruction_enum::IF_NOT_ZERO_REG_2:
					instructions[i+0] = (Instruction)inst;
					inserted = true;
						break;
				default:
					LPR_ILLEGAL_PATH();
			}
			if ( inserted )
			{
				i += INSTRUCTION_GROUP_COUNT;
			}
		}
		generate_skeleton( &pj->bones_mh, &pj->articulations_mh,
		                   &pj->articulation_ln_mh,
		                   pj->bone_stack,
		                   pj->articulation_stack,
		                   pj->execution_stack,
		                   ARRAY_COUNT(pj->bone_stack ),
		                   skeleton );
	} while ( number_of_tries++ < 1000 && skeleton->dna.bad_dna );
}

////////////////////////////////////////////////////////////////////////////////

internal
void
dynamically_update_shader( System_API * sys_api, Game_Memory * game_mem, Shader * shader )
{
	Time mod_time_geom, mod_time_vert, mod_time_frag;
	mod_time_geom = mod_time_vert = mod_time_frag = 0.0;
	sys_api->get_file_modified_time( shader->geom.file_path,
	                                      &mod_time_geom );
	if ( sys_api->get_file_modified_time( shader->vert.file_path,
	                                      &mod_time_vert ) &&
	     sys_api->get_file_modified_time( shader->frag.file_path,
	                                      &mod_time_frag ) )
	{
		if ( shader->geom.modified_time < mod_time_geom ||
		     shader->vert.modified_time < mod_time_vert ||
		     shader->frag.modified_time < mod_time_frag )
		{
			shader->geom.modified_time = mod_time_geom;
			shader->vert.modified_time = mod_time_vert;
			shader->frag.modified_time = mod_time_frag;

			lpr_log_dbg( "shader files modified, going to make a new program (%s)", shader->frag.file_path );

			if ( !create_program( sys_api, game_mem,
			                      shader->geom.file_path,
			                      shader->vert.file_path,
			                      shader->frag.file_path,
			                      &shader->program ) )
			{
				lpr_log_err( "failed to load or compile the new shader" );
			} else
			{
				s32 i_ret;
				i_ret =
				   glGetAttribLocation( shader->program, "vertex_color" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->vertex_color_loc = i_ret;
				i_ret =
				   glGetAttribLocation( shader->program, "vertex" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->vertex_loc = i_ret;
				i_ret =
				   glGetUniformLocation( shader->program, "VP" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->VP_loc = i_ret;
				i_ret =
				   glGetUniformLocation( shader->program, "V" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->V_loc = i_ret;
				i_ret =
				   glGetUniformLocation( shader->program, "P" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->P_loc = i_ret;
				i_ret =
				   glGetUniformLocation( shader->program, "color" );
				if( i_ret == -1 ) lpr_log_dbg( "gl error = %u, i_ret = %d", glGetError(), i_ret );
				shader->color_loc = i_ret;
			}
		}
	}
}

void
procjam_init( System_API * sys_api, Game_Memory * game_mem )
{
	Procjam * pj = &game_mem->procjam;
	procjam_init_OGL( sys_api, game_mem, &pj->ogl );

	pj->dna_pool.init( pj->instructions_memory, sizeof(pj->instructions_memory) );

	Skeleton * s = pj->skeletons;
	++pj->skeletons_count;

	u32 instructions_count = 4096 * 4;
	s->dna.instructions =
	(Instruction*)pj->dna_pool.allocate_clean( instructions_count * sizeof( Instruction ) );
	s->dna.instructions_count = instructions_count;
	pj->seeds[0] = 78216487912639487L;
	pj->seeds[1] = (u64)(1e9 * game_mem->game_time);

	generate_dna_and_skeleton( s, pj->seeds, pj );

	pj->ogl.distributed_camera_speed = {{1.0f,1.0f,1.0f},{1.0f,1.0f,1.0f}};

}

GAME_API_INPUT( procjam_input )
{
	switch ( e->type )
	{
		case EVENT_WINDOW:
			switch ( e->window.type )
			{
				case EVENT_WINDOW_RESIZED:
					if ( e->window.window_id == game_mem->main_window.window_id )
					{
						ProcjamOGL * ogl = &game_mem->procjam.ogl;
						lpr_log_dbg( "main window resized, recreating proc fb" );
						if ( !create_framebuffer_with_depth(
						         V2u32{e->window.w, e->window.h},
						         &ogl->fb_with_depth ) )
						{
							lpr_log_err( "failed to create a new framebuffer after window resizing" );
						}

						resize_camera( &ogl->camera, e->window.w, e->window.h );
					}
				default:
					break;
			}
			break;

		case EVENT_MOUSE:
			switch ( e->mouse.type )
			{
				case EVENT_MOUSE_MOTION:
					if ( game_mem->procjam.ogl.fp_mouse )
					{
						//bool res =
						fp_camera_mouse_motion(
						   sys_api, game_mem,
						   e->mouse.motion.pos_x, e->mouse.motion.pos_y,
						   e->mouse.motion.motion_x, e->mouse.motion.motion_y,
						   &game_mem->procjam.ogl.camera );
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}

void
procjam_update( System_API * sys_api, Game_Memory * game_mem )
{
	Procjam * pj = &game_mem->procjam;
	Shader * shader = &pj->ogl.line_shader;
	dynamically_update_shader( sys_api, game_mem, shader );

	if ( game_mem->inputs.keyboard_releases[PK_R] )
	{
		generate_dna_and_skeleton( pj->skeletons, pj->seeds, pj );
	}

	if ( game_mem->inputs.keyboard_releases[PK_RETURN] )
	{
		pj->ogl.fp_mouse = !pj->ogl.fp_mouse;
		if ( pj->ogl.fp_mouse )
		{
			warp_mouse_to_center( sys_api, game_mem );
		}
	}

	per_frame_camera_controls_and_update(
	   game_mem, &pj->ogl.camera, &pj->ogl.camera_motion,
	   &pj->ogl.distributed_camera_speed, 10.0f );
}

void
procjam_render( Game_Memory * game_mem )
{
	Procjam * pj = &game_mem->procjam;
	ProcjamOGL * ogl = &pj->ogl;

	ogl->camera.VP = ogl->camera.P * ogl->camera.V;

	ogl->vertices_count = 0;

	// NOTE(theGiallo): XYZ axis
	f32 l = 10.0f;
	ogl->vertices[ogl->vertices_count++] = {{0.0f,0.0f,0.0f},{1.0f,0.0f,0.0f}};
	ogl->vertices[ogl->vertices_count++] = {{   l,0.0f,0.0f},{1.0f,0.0f,0.0f}};
	ogl->vertices[ogl->vertices_count++] = {{0.0f,0.0f,0.0f},{0.0f,1.0f,0.0f}};
	ogl->vertices[ogl->vertices_count++] = {{0.0f,   l,0.0f},{0.0f,1.0f,0.0f}};
	ogl->vertices[ogl->vertices_count++] = {{0.0f,0.0f,0.0f},{0.0f,0.0f,1.0f}};
	ogl->vertices[ogl->vertices_count++] = {{0.0f,0.0f,   l},{0.0f,0.0f,1.0f}};

	V3 pos = {1.0f,1.0f,0.0f};
	V3 rot = {};
	for ( u32 i = 0; i != pj->skeletons_count; ++i )
	{
		add_skeleton_to_render( pj->skeletons + i, ogl, pos, rot,
		                        pj->bone_stack,
		                        pj->articulation_ln_stack,
		                        pj->mx4_stack,
		                        ARRAY_COUNT(pj->bone_stack) );
		pos.x += 10.0f;
	}

	glBindFramebuffer( GL_FRAMEBUFFER, ogl->fb_with_depth.linear_fbo );

	glDepthMask( GL_TRUE );
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LESS );
	glClearDepthf( 1.0f );
	glClearColor( 0.3f, 0.3f, 0.3f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	if ( ogl->vertices_count )
	{
		set_ogl_camera( &ogl->line_shader, &ogl->camera );

		Lpr_Rgba_f32 default_color = {1.0f,0.0f,0.0f,1.0f};
		render_colored_vertices(
		   &ogl->line_shader, ogl->vertices, ARRAY_COUNT( ogl->vertices ),
		   ogl->vertices_count, &default_color, ogl->vertex_buff, GL_LINES, GL_STREAM_DRAW );
	}

	glBindFramebuffer( GL_FRAMEBUFFER, 0 );


	Lpr_Texture texture = {};
	texture.gl_texture = ogl->fb_with_depth.linear_fb_texture;
	texture.size = ogl->fb_with_depth.size;
	texture.default_overflow = {LPR_CLAMP_TO_EDGE, LPR_CLAMP_TO_EDGE};
	lpr_add_rect_full_window( &game_mem->main_window.lpr_draw_data, 0, &texture, lpr_C_colors_u8.white, 0.0f, {} );

	lpr_s32 glyph_id;
	Lpr_AA_Rect aa_bb;
	Lpr_V2 ending_pt;
	V2u32 wc = compute_window_center( game_mem );
	Lpr_AA_Rect rect;
	rect.pos.x = wc.x;
	rect.pos.y = wc.y - 120;
	Lpr_V2u32 ws = game_mem->main_window.lpr_draw_data.window_size;
	rect.size.x = ws.x;
	rect.size.y = ws.y;
	rect.size.w -= 20;
	rect.size.h -= 20;

	char str[1024*1024] = {};
	u32 first_free = 0;

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "vertices count: %d\n", ogl->vertices_count );

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "skeletons count: %d\n", pj->skeletons_count );

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "bones_mh: %u/%u\n", pj->bones_mh.total_occupancy, pj->bones_mh.capacity );

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "articulations_mh: %u/%u\n", pj->articulations_mh.total_occupancy, pj->articulations_mh.capacity );

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "articulation_ln_mh: %u/%u\n", pj->articulation_ln_mh.total_occupancy, pj->articulation_ln_mh.capacity );

	first_free +=
	snprint_dna( str + first_free, sizeof(str)-first_free, &pj->skeletons[0].dna );

#if 0
	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "camera V:\n" );
	for ( int i=0; i!=16; ++i )
	{
		first_free +=
		snprintf( str + first_free, sizeof(str)-first_free, "%7.3f%s",
		          ogl->camera.V.arr[i], i%4==3?"\n":" " );
	}


	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "camera P:\n" );
	for ( int i=0; i!=16; ++i )
	{
		first_free +=
		snprintf( str + first_free, sizeof(str)-first_free, "%7.3f%s",
		          ogl->camera.P.arr[i], i%4==3?"\n":" " );
	}

	first_free +=
	snprintf( str + first_free, sizeof(str)-first_free, "camera look: %f, %f, %f\n", ogl->camera.look.x, ogl->camera.look.y, ogl->camera.look.z );

#endif

	lpr_add_multiline_text_in_rect(
	   &game_mem->main_window.lpr_draw_data, 1,
	   &game_mem->fonts.multi_font, 0, 1,
	   rect, str, first_free, lpr_C_colors_u8.white, false, 0, 1.0f, false, false, &glyph_id, &aa_bb, &ending_pt );
}

void
procjam_init_OGL( System_API * sys_api, Game_Memory * game_mem, ProcjamOGL * ogl )
{
	ogl->line_shader.geom.file_path = NULL;
	ogl->line_shader.vert.file_path = "resources/shaders/3d_lines.vert";
	ogl->line_shader.frag.file_path = "resources/shaders/3d_lines.frag";

	create_program( sys_api, game_mem, ogl->line_shader.geom.file_path,
	                ogl->line_shader.vert.file_path, ogl->line_shader.frag.file_path,
	                &ogl->line_shader.program );

	create_framebuffer_with_depth(
	   make_V2u32( game_mem->main_window.lpr_draw_data.window_size ), &ogl->fb_with_depth );

	create_and_fill_ogl_buffer( &ogl->vertex_buff, GL_ARRAY_BUFFER, GL_STREAM_DRAW, ogl->vertices, sizeof(ogl->vertices) );

	Lpr_V2u32 ws = game_mem->main_window.lpr_draw_data.window_size;
	f32 inverse_aspect_ratio = (f32)ws.h/(f32)ws.w;
	initialize_camera( &ogl->camera, inverse_aspect_ratio );
	ogl->camera.look = {-0.863f, -0.4f, -0.3f };
}

void
add_skeleton_to_render( Skeleton * skeleton, ProcjamOGL * ogl, V3 pos, V3 rot,
                        Bone ** bone_stack,
                        Articulation_List_Node ** articulations_stack,
                        Mx4 * model_stack,
                        u32 stack_capacity )
{
	V3 scale = {1.0f,1.0f,1.0f};
	memset( bone_stack,          0x0, sizeof( bone_stack[0]          ) * stack_capacity );
	memset( articulations_stack, 0x0, sizeof( articulations_stack[0] ) * stack_capacity );
	memset( model_stack,         0x0, sizeof( model_stack[0]         ) * stack_capacity );
	s32 stack_top = 0;
	bone_stack[stack_top] = skeleton->root_articulation->bone;
	model_stack[stack_top] = transform_mx4( pos, rot, scale ) * rotation_mx4( skeleton->root_articulation->rotation );
	V3 color = {0.8f,0.8f,0.8f};

	bool added[1024*255] = {};
	bool backtracked = false;
	while ( stack_top >= 0 )
	{
		if ( !articulations_stack[stack_top] )
		{
			if ( !backtracked )
			{
				// NOTE(theGiallo): add bone to draw buffer
				u64 rel_p = (u64)bone_stack[stack_top] - (u64)global_game_mem->procjam.bones_mh.mem;
				u64 id = rel_p / sizeof(Bone);
				//lpr_log_dbg( "bone %p [%5lu]", bone_stack[stack_top], id );
				lpr_assert( id < ARRAY_COUNT(added) );
				lpr_assert( !added[id] );
				added[id] = true;
				//lpr_log_dbg( "adding vertex %u", ogl->vertices_count );
				V3 start = {}, end = V3{0.0f, 0.0f, 1.0f} * bone_stack[stack_top]->length;
				//lpr_log_dbg( "              %u", ogl->vertices_count );
				ogl->vertices[ogl->vertices_count].vertex = model_stack[stack_top] * start;
				//lpr_log_dbg( "              %u", ogl->vertices_count );
				ogl->vertices[ogl->vertices_count].color = hadamard( color, {1.5f, 0.3f, 0.3f } );
				++ogl->vertices_count;
				//lpr_log_dbg( "   adding vertex %u", ogl->vertices_count );
				ogl->vertices[ogl->vertices_count].vertex = model_stack[stack_top] * end;
				ogl->vertices[ogl->vertices_count].color = color;
				++ogl->vertices_count;

				if ( bone_stack[stack_top]->articulations_list )
				{
					articulations_stack[stack_top] = bone_stack[stack_top]->articulations_list;
				} else
				{
					backtracked = true;
					--stack_top;
				}
			} else
			{
				backtracked = true;
				--stack_top;
			}
		} else
		{
			bone_stack[stack_top+1] = articulations_stack[stack_top]->articulation->bone;
			V3 p = {};
			p.z = bone_stack[stack_top]->length * articulations_stack[stack_top]->articulation->length_percentage_attachment;
			V3 r = articulations_stack[stack_top]->articulation->rotation;
			Mx4 m = transform_mx4( p, r, V3{1.0f,1.0f,1.0f} );
			model_stack[stack_top+1] = model_stack[stack_top] * m;
			lpr_assert( !articulations_stack[stack_top+1] );
			articulations_stack[stack_top] = articulations_stack[stack_top]->next;

			backtracked = false;
			++stack_top;
		}
	}
}

u32
snprint_dna( char * string, u32 byte_size, DNA * dna )
{
	u32 first_free = 0;
	first_free +=
	snprintf( string + first_free, byte_size - first_free, "%s\n", dna->bad_dna?"bad":"good" );

	Instruction * instructions = dna->instructions;
	u32 instructions_count = dna->instructions_count;
	for ( u32 i = 0; i!=instructions_count; )
	{
		Instruction_enum inst = instructions[i]._enum;
		switch ( inst )
		{
			case Instruction_enum::MOVE_UP:
			//case Instruction_enum::MOVE_DOWN:
			case Instruction_enum::ADD_LENGTH:
			//case Instruction_enum::SUB_LENGTH:
				if ( i < dna->instructions_count - 1 )
				{
					const char * name;
					f32 val;
					switch ( inst )
					{
						case Instruction_enum::MOVE_UP:
							name = "MOVE_UP";
							val = decode_move( instructions + ( i + 1 ) );
							break;
					#if 0
						case Instruction_enum::MOVE_DOWN:
							name = "MOVE_DOWN";
							val = -decode_move( instructions + ( i + 1 ) );
							break;
					#endif
						case Instruction_enum::ADD_LENGTH:
							name = "ADD_LENGTH";
							val = decode_length( instructions + ( i + 1 ) );
							break;
					#if 0
						case Instruction_enum::SUB_LENGTH:
							name = "SUB_LENGTH";
							val = -decode_length( instructions + ( i + 1 ) );
							break;
					#endif
						default: break;
					}
					first_free +=
					snprintf( string + first_free, byte_size - first_free,
					          "%s %f\n", name, val );
				}
				break;
			case Instruction_enum::ADD_BONE:
				if ( i < dna->instructions_count - 2 )
				{
					f32 ic  = decode_f32_01_pos( instructions + ( i + 1 ) );
					f32 jmp = decode_f32_01_pos( instructions + ( i + 2 ) );
					first_free +=
					snprintf( string + first_free, byte_size - first_free,
					          "%s %f %f\n", "ADD_BONE", ic, jmp );
				}
				break;
			case Instruction_enum::ADD_ROTATION:
			case Instruction_enum::SUB_ROTATION:
				if ( i < dna->instructions_count - 3 )
				{
					V3 val = decode_rotation( instructions + ( i + 1 ) );
					const char * name = "ADD_ROTATION";
					if ( inst == Instruction_enum::SUB_ROTATION )
					{
						name = "SUB_ROTATION";
					}
					first_free +=
					snprintf( string + first_free, byte_size - first_free,
					          "%s %f, %f, %f\n", name, val.x, val.y, val.z );
				}
				break;
			case Instruction_enum::SET_REG_0:
			case Instruction_enum::SET_REG_1:
			case Instruction_enum::SET_REG_2:
			case Instruction_enum::ADD_TO_REG_0:
			case Instruction_enum::ADD_TO_REG_1:
			case Instruction_enum::ADD_TO_REG_2:
			case Instruction_enum::SUB_TO_REG_0:
			case Instruction_enum::SUB_TO_REG_1:
			case Instruction_enum::SUB_TO_REG_2:
			case Instruction_enum::MUL_REG_0:
			case Instruction_enum::MUL_REG_1:
			case Instruction_enum::MUL_REG_2:
			case Instruction_enum::DIV_REG_0:
			case Instruction_enum::DIV_REG_1:
			case Instruction_enum::DIV_REG_2:
			{
				const char * name = NULL;
				switch ( inst )
				{
					case Instruction_enum::SET_REG_0:
						name = "SET_REG_0";
						break;
					case Instruction_enum::SET_REG_1:
						name = "SET_REG_1";
						break;
					case Instruction_enum::SET_REG_2:
						name ="SET_REG_2";
						break;
					case Instruction_enum::ADD_TO_REG_0:
						name ="ADD_TO_REG_0";
						break;
					case Instruction_enum::ADD_TO_REG_1:
						name ="ADD_TO_REG_1";
						break;
					case Instruction_enum::ADD_TO_REG_2:
						name ="ADD_TO_REG_2";
						break;
					case Instruction_enum::SUB_TO_REG_0:
						name ="SUB_TO_REG_0";
						break;
					case Instruction_enum::SUB_TO_REG_1:
						name ="SUB_TO_REG_1";
						break;
					case Instruction_enum::SUB_TO_REG_2:
						name ="SUB_TO_REG_2";
						break;
					case Instruction_enum::MUL_REG_0:
						name ="MUL_REG_0";
						break;
					case Instruction_enum::MUL_REG_1:
						name ="MUL_REG_1";
						break;
					case Instruction_enum::MUL_REG_2:
						name ="MUL_REG_2";
						break;
					case Instruction_enum::DIV_REG_0:
						name ="DIV_REG_0";
						break;
					case Instruction_enum::DIV_REG_1:
						name ="DIV_REG_1";
						break;
					case Instruction_enum::DIV_REG_2:
						name ="DIV_REG_2";
						break;
					default:
						break;
				}
				u64 v = decode_reg_value( instructions + ( i + 1 ) );
				first_free +=
				snprintf( string + first_free, byte_size - first_free,
				          "%s %lu\n", name, v );
			}
				break;
			case Instruction_enum::ADD_REG_0_TO_REG_1:
			case Instruction_enum::ADD_REG_0_TO_REG_2:
			case Instruction_enum::ADD_REG_1_TO_REG_0:
			case Instruction_enum::ADD_REG_1_TO_REG_2:
			case Instruction_enum::ADD_REG_2_TO_REG_0:
			case Instruction_enum::ADD_REG_2_TO_REG_1:
			case Instruction_enum::SUB_REG_0_TO_REG_1:
			case Instruction_enum::SUB_REG_0_TO_REG_2:
			case Instruction_enum::SUB_REG_1_TO_REG_0:
			case Instruction_enum::SUB_REG_1_TO_REG_2:
			case Instruction_enum::SUB_REG_2_TO_REG_0:
			case Instruction_enum::SUB_REG_2_TO_REG_1:
			case Instruction_enum::MUL_REG_0_INTO_REG_1:
			case Instruction_enum::MUL_REG_0_INTO_REG_2:
			case Instruction_enum::MUL_REG_1_INTO_REG_0:
			case Instruction_enum::MUL_REG_1_INTO_REG_2:
			case Instruction_enum::MUL_REG_2_INTO_REG_0:
			case Instruction_enum::MUL_REG_2_INTO_REG_1:
			case Instruction_enum::DIV_BY_REG_0_REG_1:
			case Instruction_enum::DIV_BY_REG_0_REG_2:
			case Instruction_enum::DIV_BY_REG_1_REG_0:
			case Instruction_enum::DIV_BY_REG_1_REG_2:
			case Instruction_enum::DIV_BY_REG_2_REG_0:
			case Instruction_enum::DIV_BY_REG_2_REG_1:
			case Instruction_enum::IF_ZERO_REG_0:
			case Instruction_enum::IF_ZERO_REG_1:
			case Instruction_enum::IF_ZERO_REG_2:
			case Instruction_enum::IF_NOT_ZERO_REG_0:
			case Instruction_enum::IF_NOT_ZERO_REG_1:
			case Instruction_enum::IF_NOT_ZERO_REG_2:
			{
				const char * name = NULL;
				switch ( inst )
				{
					case Instruction_enum::ADD_REG_0_TO_REG_1:
						name ="ADD_REG_0_TO_REG_1";
						break;
					case Instruction_enum::ADD_REG_0_TO_REG_2:
						name ="ADD_REG_0_TO_REG_2";
						break;
					case Instruction_enum::ADD_REG_1_TO_REG_0:
						name ="ADD_REG_1_TO_REG_0";
						break;
					case Instruction_enum::ADD_REG_1_TO_REG_2:
						name ="ADD_REG_1_TO_REG_2";
						break;
					case Instruction_enum::ADD_REG_2_TO_REG_0:
						name ="ADD_REG_2_TO_REG_0";
						break;
					case Instruction_enum::ADD_REG_2_TO_REG_1:
						name ="ADD_REG_2_TO_REG_1";
						break;
					case Instruction_enum::SUB_REG_0_TO_REG_1:
						name ="SUB_REG_0_TO_REG_1";
						break;
					case Instruction_enum::SUB_REG_0_TO_REG_2:
						name ="SUB_REG_0_TO_REG_2";
						break;
					case Instruction_enum::SUB_REG_1_TO_REG_0:
						name ="SUB_REG_1_TO_REG_0";
						break;
					case Instruction_enum::SUB_REG_1_TO_REG_2:
						name ="SUB_REG_1_TO_REG_2";
						break;
					case Instruction_enum::SUB_REG_2_TO_REG_0:
						name ="SUB_REG_2_TO_REG_0";
						break;
					case Instruction_enum::SUB_REG_2_TO_REG_1:
						name ="SUB_REG_2_TO_REG_1";
						break;
					case Instruction_enum::MUL_REG_0_INTO_REG_1:
						name ="MUL_REG_0_INTO_REG_1";
						break;
					case Instruction_enum::MUL_REG_0_INTO_REG_2:
						name ="MUL_REG_0_INTO_REG_2";
						break;
					case Instruction_enum::MUL_REG_1_INTO_REG_0:
						name ="MUL_REG_1_INTO_REG_0";
						break;
					case Instruction_enum::MUL_REG_1_INTO_REG_2:
						name ="MUL_REG_1_INTO_REG_2";
						break;
					case Instruction_enum::MUL_REG_2_INTO_REG_0:
						name ="MUL_REG_2_INTO_REG_0";
						break;
					case Instruction_enum::MUL_REG_2_INTO_REG_1:
						name ="MUL_REG_2_INTO_REG_1";
						break;
					case Instruction_enum::DIV_BY_REG_0_REG_1:
						name ="DIV_BY_REG_0_REG_1";
						break;
					case Instruction_enum::DIV_BY_REG_0_REG_2:
						name ="DIV_BY_REG_0_REG_2";
						break;
					case Instruction_enum::DIV_BY_REG_1_REG_0:
						name ="DIV_BY_REG_1_REG_0";
						break;
					case Instruction_enum::DIV_BY_REG_1_REG_2:
						name ="DIV_BY_REG_1_REG_2";
						break;
					case Instruction_enum::DIV_BY_REG_2_REG_0:
						name ="DIV_BY_REG_2_REG_0";
						break;
					case Instruction_enum::DIV_BY_REG_2_REG_1:
						name ="DIV_BY_REG_2_REG_1";
						break;
					case Instruction_enum::IF_ZERO_REG_0:
						name ="IF_ZERO_REG_0";
						break;
					case Instruction_enum::IF_ZERO_REG_1:
						name ="IF_ZERO_REG_1";
						break;
					case Instruction_enum::IF_ZERO_REG_2:
						name ="IF_ZERO_REG_2";
						break;
					case Instruction_enum::IF_NOT_ZERO_REG_0:
						name ="IF_NOT_ZERO_REG_0";
						break;
					case Instruction_enum::IF_NOT_ZERO_REG_1:
						name ="IF_NOT_ZERO_REG_1";
						break;
					case Instruction_enum::IF_NOT_ZERO_REG_2:
						name ="IF_NOT_ZERO_REG_2";
						break;
					default:
						break;
				}
				first_free +=
				snprintf( string + first_free, byte_size - first_free,
				          "%s\n", name );
			}
					break;

			case Instruction_enum::STOP:
				first_free +=
				snprintf( string + first_free, byte_size - first_free,
				          "%s\n", "STOP" );
				break;
			default:
				first_free +=
				snprintf( string + first_free, byte_size - first_free,
				          "%s = [ %lu | %ld ] [ %lu | %ld ] [ %lu | %ld ]\n",
				          "BAD",
				          instructions[i+1]._u64, instructions[i+1]._s64,
				          instructions[i+2]._u64, instructions[i+2]._s64,
				          instructions[i+3]._u64, instructions[i+3]._s64 );
				break;
		}
		i+=INSTRUCTION_GROUP_COUNT;
	}

	return first_free;
}
