#include "game_api_chunked_sim.h"
#include "game_api_utility.h"
#include "game_api_lepre.h"

bool
add_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh )
{
	bool ret = false;
	Entity_List_Node * new_node = allocate( list_mh );
	if ( new_node )
	{
		new_node->entity = entity;
		new_node->next = *list;
		*list = new_node;
		ret = true;
	}
	return ret;
}

bool
remove_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh )
{
	bool ret = false;

	u64 identifier = entity->identifier;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		if ( (*curr_list)->entity->identifier == identifier )
		{
			deallocate( list_mh, *curr_list );
			*curr_list = (*curr_list)->next;
			ret = true;
			break;
		}
	}

	return ret;
}

inline
internal
u32
get_hash_entity_id( Entity * entity, Entity_Hash_Table * ht )
{
	u64 identifier = entity->identifier;
#if 0
	u32 ret = ( identifier & 0xFFFFFFFF ) ^ ( ( identifier >> 32 ) & 0xFFFFFFFF );
#else
	u32 ret = identifier;
#endif
	ret = ret % ARRAY_COUNT( ht->table );

	return ret;
}

bool
remove_entity( Entity * entity, Entity_Hash_Table * ht )
{
	bool ret;

	u32 id = get_hash_entity_id( entity, ht );

	ret =
	remove_entity( entity, ht->table + id, &ht->list_mh );

	return ret;
}

bool
add_entity( Entity * entity, Entity_Hash_Table * ht )
{
	bool ret;

	u32 id = get_hash_entity_id( entity, ht );

	ret =
	add_entity( entity, ht->table + id, &ht->list_mh );

	return ret;
}

void
simulate_entity( Sim_Chunk * sim_chunk, Entity * entity )
{
	if ( entity->owner_chunk_x != sim_chunk->idx_x ||
	     entity->owner_chunk_y != sim_chunk->idx_y )
	{
		return;
	}

	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep_hypothetically( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep_check_hypothesis( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_entity_fixed_timestep( Sim_Chunk * sim_chunk, Entity * entity )
{
	switch ( entity->type )
	{
		// NOTE(theGiallo): here we have the code for the compile time known
		// entities. In the default the dl ones.
		case ENTITY_TYPE_EXAMPLE:
			break;
		default:
			// TODO(theGiallo, 2016-10-17): here find the proper function to
			// call. The entity could be loaded from a dl, the type name and
			// function inserted into hash-tables, accessed here.
			break;
	}
}

void
simulate_chunk( Sim_Chunk * sim_chunk )
{
	Entity_List * table = sim_chunk->entities.table;
	for ( int i = 0; i!= ARRAY_COUNT( sim_chunk->entities.table ); ++i )
	{
		for ( Entity_List list = table[i];
		      list;
		      list = list->next )
		{
			simulate_entity( sim_chunk, list->entity );
		}
	}
}

void
simulate_chunk_fixed_timestep( Sim_Chunk * sim_chunk )
{
	Entity_List * table = sim_chunk->entities.table;
	for ( int i = 0; i!= ARRAY_COUNT( sim_chunk->entities.table ); ++i )
	{
		for ( Entity_List list = table[i];
		      list;
		      list = list->next )
		{
			simulate_entity_fixed_timestep( sim_chunk, list->entity );
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline
internal
u32
get_hash_entity_id( u64 identifier, Entities_By_Id * ht )
{
#if 0
	u32 ret = ( identifier & 0xFFFFFFFF ) ^ ( ( identifier >> 32 ) & 0xFFFFFFFF );
#else
	u32 ret = identifier;
#endif
	ret = ret % ARRAY_COUNT( ht->table );

	return ret;
}

internal
bool
add( Entities_By_Id * entities_by_id, Entity * entity )
{
	bool ret = false;

	u32 id = get_hash_entity_id( entity->identifier, entities_by_id );
	Entity_List * list = entities_by_id->table + id;
	Entity_List_Node * new_node = allocate( &entities_by_id->list_mh );
	if ( new_node )
	{
		new_node->entity = entity;
		new_node->next   = *list;
		*list = new_node;
		ret = true;
	}

	return ret;
}

internal
bool
remove( Entities_By_Id * entities_by_id, u64 identifier )
{
	bool ret = false;

	u32 id = get_hash_entity_id( identifier, entities_by_id );
	Entity_List * = entities_by_id->table + id;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		if ( (*curr_list)->entity->identifier == identifier )
		{
			deallocate( &entities_by_id->list_mh, *curr_list );
			*curr_list = (*curr_list)->next;
			ret = true;
			break;
		}
	}

	return ret;
}

internal
inline
void
index_entity_by_identifier( Entity_Storage * storage, Entity * entity )
{
	add( &storage->entities_by_id, entity );
}

internal
inline
void
unindex_entity_by_identifier( Entity_Storage * storage, Entity * entity )
{
	remove( &storage->entities_by_id, entity->identifier );
}

Entity *
recreate_entity_header( Entity_Storage * storage, u32 type, u64 identifier )
{
	Entity * ret = allocate_clean( &storage->mh );

	if ( ret )
	{
		ret->type       = type;
		ret->identifier = identifier;
		index_entity_by_identifier( storage, ret );
	}

	return ret;

}

Entity *
create_entity_header( Entity_Storage * storage )
{
	Entity * ret = allocate_clean( &storage->mh );

	if ( ret )
	{
		ret->identifier = lpr_xorshift128plus( storage->seeds );
		index_entity_by_identifier( storage, ret );
	}

	return ret;
}

void
destroy_entity_header( Entity_Storage * storage, Entity * entity )
{
	unindex_entity_by_identifier( storage, entity );
	deallocate( &storage->mh, entity );
}

Entity *
find_entity_by_identifier( Entity_Storage * storage, u64 identifier )
{
	Entity * ret = NULL;

	Entities_By_Id * entities_by_id = &storage->entities_by_id;
	u32 id = get_hash_entity_id( identifier, entities_by_id );
	Entity_List * list = entities_by_id->table + id;

	for ( Entity_List * curr_list = list;
	      *curr_list;
	      curr_list = &(*curr_list)->next )
	{
		Entity * entity = (*curr_list)->entity;
		if ( entity->identifier == identifier )
		{
			ret = entity;
			break;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

Entity *
create_entity_example( Entity_Storage * entity_storage, Entity_Example_Storage * storage )
{
	Entity * ret = create_entity_header( entity_storage );
	Entity_Example * entity_example = allocate_clean( &storage->mh );
	ret->body = entity_example;
	ret->type = ENTITY_TYPE_EXAMPLE;

	return ret;
}
