#ifndef PROCJAM_2016_H
#define PROCJAM_2016_H 1

#include "basic_types.h"
#include "tgmath.h"
#include "our_gl.h"
#include "game_api_opengl_tools.h"

struct Articulation_List_Node;
struct
Bone
{
	f32 length;
	Articulation_List_Node * articulations_list;
};

#define MH_FLOORS_COUNT 1024
#define MH_TYPE Bone
#include "mem_hotel.inc.h"

struct
Articulation
{
	Bone * bone;
	V3 rotation;
	f32 length_percentage_attachment; // NOTE(theGiallo): [0-1]
};

#define MH_FLOORS_COUNT 1024
#define MH_TYPE Articulation
#include "mem_hotel.inc.h"

struct
Articulation_List_Node
{
	Articulation * articulation;
	Articulation_List_Node * next;
};

#define MH_FLOORS_COUNT 1024
#define MH_TYPE Articulation_List_Node
#include "mem_hotel.inc.h"

#define INSTRUCTION_GROUP_COUNT 4
enum class
Instruction_enum : s64
{
	STOP = 0x0,
	ADD_BONE,
	ADD_LENGTH,
	//SUB_LENGTH,
	ADD_ROTATION,
	SUB_ROTATION,
	MOVE_UP,
	//MOVE_DOWN,
#if 1
	// --- REGISTERS
	SET_REG_0,
	SET_REG_1,
	SET_REG_2,
	ADD_TO_REG_0,
	ADD_TO_REG_1,
	ADD_TO_REG_2,
	SUB_TO_REG_0,
	SUB_TO_REG_1,
	SUB_TO_REG_2,
	MUL_REG_0,
	MUL_REG_1,
	MUL_REG_2,
	DIV_REG_0,
	DIV_REG_1,
	DIV_REG_2,
	ADD_REG_0_TO_REG_1,
	ADD_REG_0_TO_REG_2,
	ADD_REG_1_TO_REG_0,
	ADD_REG_1_TO_REG_2,
	ADD_REG_2_TO_REG_0,
	ADD_REG_2_TO_REG_1,
	SUB_REG_0_TO_REG_1,
	SUB_REG_0_TO_REG_2,
	SUB_REG_1_TO_REG_0,
	SUB_REG_1_TO_REG_2,
	SUB_REG_2_TO_REG_0,
	SUB_REG_2_TO_REG_1,
	MUL_REG_0_INTO_REG_1,
	MUL_REG_0_INTO_REG_2,
	MUL_REG_1_INTO_REG_0,
	MUL_REG_1_INTO_REG_2,
	MUL_REG_2_INTO_REG_0,
	MUL_REG_2_INTO_REG_1,
	DIV_BY_REG_0_REG_1,
	DIV_BY_REG_0_REG_2,
	DIV_BY_REG_1_REG_0,
	DIV_BY_REG_1_REG_2,
	DIV_BY_REG_2_REG_0,
	DIV_BY_REG_2_REG_1,
#if 0
	ADD_BONE_REG_0_1,
	ADD_BONE_REG_1_0,
	ADD_BONE_REG_0_2,
	ADD_BONE_REG_2_0,
	ADD_BONE_REG_1_2,
	ADD_BONE_REG_2_1,
	ADD_LENGTH_REG_0,
	ADD_LENGTH_REG_1,
	ADD_LENGTH_REG_2,
	//SUB_LENGTH_REG_0,
	//SUB_LENGTH_REG_1,
	//SUB_LENGTH_REG_2,
	ADD_ROTATION_REG_0_1_2,
	ADD_ROTATION_REG_0_2_1,
	ADD_ROTATION_REG_1_0_2,
	ADD_ROTATION_REG_1_2_0,
	ADD_ROTATION_REG_2_1_0,
	ADD_ROTATION_REG_2_0_1,
	SUB_ROTATION_REG_0_1_2,
	SUB_ROTATION_REG_0_2_1,
	SUB_ROTATION_REG_1_0_2,
	SUB_ROTATION_REG_1_2_0,
	SUB_ROTATION_REG_2_1_0,
	SUB_ROTATION_REG_2_0_1,
	//MOVE_UP_REG_0,
	//MOVE_UP_REG_1,
	//MOVE_UP_REG_2,
	MOVE_DOWN_REG_0,
	MOVE_DOWN_REG_1,
	MOVE_DOWN_REG_2,
#endif
	// --- CONDITIONALS
	IF_ZERO_REG_0,
	IF_ZERO_REG_1,
	IF_ZERO_REG_2,
	IF_NOT_ZERO_REG_0,
	IF_NOT_ZERO_REG_1,
	IF_NOT_ZERO_REG_2,
#endif
	// ---
	_COUNT
};

union
Instruction
{
	s64 _s64;
	u64 _u64;
	Instruction_enum _enum;
};

struct
DNA
{
	Instruction * instructions;
	u32 instructions_count;
	bool bad_dna;
};


struct
Skeleton
{
	DNA dna;
	Articulation * root_articulation;
};

struct
Execution_State
{
	DNA dna;
#define NUM_REGISTERS 3
	Instruction registers[NUM_REGISTERS];
	u32 next_instruction_location;
};

// NOTE(theGiallo): bones structure
// variation in parameters with a fixed structure
// variation of the structure
//
// structure means the graph structure of the skeleton
// parameters are attachment position (in %), relative rotation
// and bone deformation
//
// How to get bones like the skull?
//
// we could simulate bone growth... but it could be long and difficult
//
// for now straight bones, maybe we could use splines later


// operations -> expressions
// generate expressions
// operators, operands <- where to read memory (relative or absolute)
//
// operations
// +
// -
// *
// /
// sqrt
// exponential
// power
//
// where to read
// anything has to be of the same length and aligned the same way and use the
// same encoding
//
// to grow length it can add to a value
// to spawn a child? baybe writing on a value that triggers the spawning of a
// bone. It should have parameters, so it should work something like a function
// calling.
//
// dna could be like a program, a series of instructions
//
// at first we have a single bone
// then we could add to its length or add an articulation with a new bone
// we have to have a way to navigate the skeleton or to have a "current" bone
// we could opt for having a stack/queue of bones to process
//
// add bone at percent $p
//
// 64 bit values for everything

#define PROCJAM_MAX_VERTICES 65536
struct
ProcjamOGL
{
	//GLuint vao;
	Shader line_shader;
	Frame_Buffer_With_Depth fb_with_depth;
	GLuint vertex_buff;
	Colored_Vertex vertices[2048*255*2];
	u32 vertices_count;
	Camera3D camera;
	Controlled_Motion camera_motion;
	Distributed_Speed distributed_camera_speed;
	bool fp_mouse;
};

#define PROCJAM_SKELETON_MAX_POPULATION_COUNT 1024
struct
Procjam
{
	ProcjamOGL ogl;
	Bone_MH_1024                   bones_mh;
	Articulation_MH_1024           articulations_mh;
	Articulation_List_Node_MH_1024 articulation_ln_mh;
	Skeleton skeletons[PROCJAM_SKELETON_MAX_POPULATION_COUNT];
	Mem_Pool dna_pool;
	Instruction instructions_memory[1024*1024*128];

	#define MAX_BONES_PER_STACK (1024)
	Bone * bone_stack[MAX_BONES_PER_STACK];
	Articulation * articulation_stack[MAX_BONES_PER_STACK];
	Execution_State execution_stack[MAX_BONES_PER_STACK];
	Articulation_List_Node * articulation_ln_stack[MAX_BONES_PER_STACK];
	Mx4 mx4_stack[MAX_BONES_PER_STACK];
	u64 seeds[2];
	u32 skeletons_count;
};

void
procjam_init( System_API * sys_api, Game_Memory * game_mem );

GAME_API_INPUT( procjam_input );

void
procjam_update( System_API * sys_api, Game_Memory * game_mem );

void
procjam_render( Game_Memory * game_mem );

void
procjam_init_OGL( System_API * sys_api, Game_Memory * game_mem, ProcjamOGL * ogl );

void
add_skeleton_to_render( Skeleton * skeleton, ProcjamOGL * ogl, V3 pos, V3 rot,
                        Bone ** bone_stack,
                        Articulation_List_Node ** articulations_stack,
                        Mx4 * model_stack,
                        u32 stack_capacity );

u32
snprint_dna( char * string, u32 byte_size, DNA * dna );

void
generate_skeleton( Bone_MH_1024 * bones_mh, Articulation_MH_1024 * articulations_mh,
                   Articulation_List_Node_MH_1024 * articulation_ln_mh,
                   Skeleton * skeleton );

#endif
