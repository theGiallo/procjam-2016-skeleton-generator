#include "game_api_lepre.h"
#include "game_api_utility.h"
#include "tgprof.h"

////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): the first time pass *internal_bit = 0
internal
u8
rand_bit( u64 seeds[2], u64 * internal_data,
          u8 * internal_bit_id )
{
	if ( !*internal_bit_id  || *internal_bit_id > 64 )
	{
		*internal_bit_id = 1;
		*internal_data = lpr_xorshift128plus( seeds );
	}

	u8 ret = ( *internal_data >> ( *internal_bit_id - 1 ) ) & 0x1;

	++*internal_bit_id;

	return ret;
}

internal
f32
randf( u64 seeds[2] )
{
	u64 rnd = lpr_xorshift128plus( seeds );
	f64 d = ( rnd / (double)U64_MAX );
	f32 ret = (f64)FLT_MIN + d * ( (f64)FLT_MAX - (f64)FLT_MIN );
	return ret;
}

internal
float
randd_01( uint64_t seeds[2] )
{
	uint64_t rnd = lpr_xorshift128plus( seeds );
	float ret = ( rnd / (float)UINT64_MAX );
	return ret;
}

internal
float
randd_between( uint64_t seeds[2], float left, float right )
{
	uint64_t rnd = lpr_xorshift128plus( seeds );
	float ret = ( rnd / (float)UINT64_MAX ) * ( right - left ) + left;
	return ret;
}

internal
u32
randu_between( u64 seeds[2], u32 min, u32 max )
{
	u64 rnd = lpr_xorshift128plus( seeds );
	u32 * rnd2 = (u32*)&rnd;
	u32 ret = min + ( ( rnd2[0] ^ rnd2[1] ) % ( max + 1 - min ) );
	return ret;
}

internal
f64
randd_gauss_01( Marsaglia_Gauss_Data * g_data )
{
	f64 ret;

	if ( g_data->phase == 0 )
	{
		do
		{
			g_data->u1 = randd_01( g_data->seeds );
			g_data->u2 = randd_01( g_data->seeds );

			g_data->v1 = 2 * g_data->u1 - 1.0;
			g_data->v2 = 2 * g_data->u2 - 1.0;
			g_data->s = square( g_data->v1 ) + square( g_data->v2 );
		} while ( g_data->s >= 1 || g_data->s == 0 );

		ret = g_data->v1 * sqrt( -2.0 * log( g_data->s ) / g_data->s );
	} else
	{
		ret = g_data->v2 * sqrt( -2.0 * log( g_data->s ) / g_data->s );
	}

	g_data->phase = 1 - g_data->phase;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

// http://www.isthe.com/chongo/tech/comp/fnv/index.html

internal
u32
u32_FNV1a( const char * str )
{
	u32 ret;

	u32 FNV_prime    = 16777619,
	    offset_basis = 2166136261;

	ret = offset_basis;
	for ( u32 i = 0; str[i]; ++i )
	{
		ret = ret ^ (u32)str[i];
		ret = ret * FNV_prime;
	}

	return ret;
}

internal
u32
u32_FNV1a( const void * str, u32 bytes_size )
{
	u32 ret;

	u32 FNV_prime    = 16777619,
	    offset_basis = 2166136261;

	ret = offset_basis;
	for ( u32 i = 0; i < bytes_size; ++i )
	{
		ret = ret ^ (u32)((u8*)str)[i];
		ret = ret * FNV_prime;
	}

	return ret;
}


////////////////////////////////////////////////////////////////////////////////

internal
inline
u32
u32_from_f32_for_radixsort( f32 f )
{
	u32 ret;
	u32 uf = *(u32*)&f;
	u32 mask = -s32( uf >> 31 ) | 0x80000000;
	ret = uf ^ mask;

	return ret;
}

internal
void
radix_sort_f32( f32 * keys_arr, u32 keys_arr_size, u16 * ids,
                Sort_Order sort_order,
                bool restore_keys_values )
{
#if DEBUG
	{
		f32 test_float = 3.0f;
		lpr_assert(
		   f32_from_u32_after_radixsort(
		      u32_from_f32_for_radixsort( test_float ) )
		   == test_float );
	}
#endif

	for ( u32 i = 0; i < keys_arr_size; ++i )
	{
		u32 uf;
		if ( sort_order == SORT_ASCENDANTLY )
		{
			uf = u32_from_f32_for_radixsort( keys_arr[i] );
		} else
		{
			uf = u32_from_f32_for_radixsort( -keys_arr[i] );
		}
		keys_arr[i] = *(f32*)&uf;
	}

	u16 tmp[keys_arr_size];
	lpr_radix_sort_mc_32_16( (u32*)keys_arr, ids, tmp, keys_arr_size );

	if ( restore_keys_values )
	{
		for ( u32 i = 0; i < keys_arr_size; ++i )
		{
			u32 uf = f32_from_u32_after_radixsort( keys_arr[i] );
			keys_arr[i] = *(f32*)&uf;
		}
	}
}

internal
void
sort_lpr_batch_f32( Lpr_Draw_Data * draw_data, u32 batch_id,
                    f32 * keys_arr, bool restore_keys_values )
{
	PROFILE_SECTION();

	lpr_assert( batch_id < draw_data->batches_count );

	Lpr_Batch_Data * batch = draw_data->batches + batch_id;

	u32 keys_arr_size = batch->quads_count;
	u16 ids[keys_arr_size];
	for ( u32 i = 0; i < keys_arr_size; ++i )
	{
		ids[i] = i;
	}

	radix_sort_f32( keys_arr, keys_arr_size, ids, SORT_ASCENDANTLY, restore_keys_values );

	lpr_reorder_16_s( ids, batch->instances_data, keys_arr_size,
	                  sizeof( batch->instances_data[0] ) );
}

internal
void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count, Sort_Order sort_order )
{
	u32 firsts[4][256] = {};

	// NOTE(theGiallo): count buckets occurrences
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i] >> 0  ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[4] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=4; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[i] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[i] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = keys[i];
		}
		u32 * swp = tmp;
		      tmp = keys;
		      keys = swp;
	}
}

internal
void
radix_sort_mc_32_16( const u32 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order )
{
#define BYTES_COUNT 4
	u32 firsts[BYTES_COUNT][256] = {};

	// NOTE(theGiallo): count buckets occurrences
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[BYTES_COUNT] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=BYTES_COUNT; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		      tmp = ids;
		      ids = swp;
	}
#undef BYTES_COUNT
}

internal
void
radix_sort_mc_64_16( const u64 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order )
{
#define BYTES_COUNT 8
	u32 firsts[BYTES_COUNT][256] = LPR_ZERO_STRUCT;

	// NOTE(theGiallo): count buckets occurrencies
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
			++firsts[4][( keys[i] >> 32 ) & 0xFF];
			++firsts[5][( keys[i] >> 40 ) & 0xFF];
			++firsts[6][( keys[i] >> 48 ) & 0xFF];
			++firsts[7][( keys[i] >> 56 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
			++firsts[4][0xFF - ( ( keys[i] >> 32 ) & 0xFF )];
			++firsts[5][0xFF - ( ( keys[i] >> 40 ) & 0xFF )];
			++firsts[6][0xFF - ( ( keys[i] >> 48 ) & 0xFF )];
			++firsts[7][0xFF - ( ( keys[i] >> 56 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[BYTES_COUNT] = LPR_ZERO_STRUCT;
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		v[4] = firsts[4][i];
		v[5] = firsts[5][i];
		v[6] = firsts[6][i];
		v[7] = firsts[7][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		firsts[4][i] = counts[4];
		firsts[5][i] = counts[5];
		firsts[6][i] = counts[6];
		firsts[7][i] = counts[7];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
		counts[4] += v[4];
		counts[5] += v[5];
		counts[6] += v[6];
		counts[7] += v[7];
	}

	for ( u8 b=0; b!=BYTES_COUNT; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		      tmp = ids;
		      ids = swp;
	}
#undef BYTES_COUNT
}

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
internal
s32
string_min( const char * string0, const char * string1 )
{
	s32 ret = 0;
	const u8 * us0 = (const u8 *) string0;
	const u8 * us1 = (const u8 *) string1;
	if ( !us0 && us1 )
	{
		ret = -1;
		return ret;
	}
	if ( !us0 && !us1 )
	{
		ret = 0;
		return ret;
	}
	if ( us0 && !us1 )
	{
		ret = 1;
		return ret;
	}

	for ( u32 i = 0; i < U32_MAX; ++i )
	{
		if ( us0[i] && us1[i] )
		{
			if ( us0[i] == us1[i] )
			{
				continue;
			}// else
			if ( us0[i] < us1[i] )
			{
				ret = -1;
			} else
			//if ( us0[i] > us1[i] )
			{
				ret = 1;
			}
		} else
		if ( us0[i] && !us1[i] )
		{
			ret = 1;
		} else
		if ( !us0[i] && us1[i] )
		{
			ret = -1;
		} else
		{
			ret = 0;
		}
		break;
	}

	return ret;
}

internal
void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[i];
		u32 d;
		if ( data )
		{
			d = data[i];
		}
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[j] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				strings[j+1] = strings[j];
				if ( data )
				{
					data[j+1] = data[j];
				}
			} else
			{
				break;
			}
		}
		strings[j+1] = s;
		if ( data )
		{
			data[j+1] = d;
		}
	}
}

internal
void
sort_lexicographically_insertion_ids_only( const char *const* strings, u32 count,
                                           u32 * ids, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[ids[i]];
		u32 id;
		id = ids[i];
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[ids[j]] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				ids[j+1] = ids[j];
			} else
			{
				break;
			}
		}
		ids[j+1] = id;
	}
}

internal
void
sort_lexicographically_insertion_ids_only_16( const char *const* strings, u32 count,
                                              u16 * ids, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[ids[i]];
		u16 id;
		id = ids[i];
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[ids[j]] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				ids[j+1] = ids[j];
			} else
			{
				break;
			}
		}
		ids[j+1] = id;
	}
}

#define COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX 150
internal
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order,
                                        u32 * data,
                                        u32 * data_tmp,
                                        u32 * lengths_strings,
                                        u32 * lengths_tmp,
                                        u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		u32 lengths_t[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion( strings, count, tmp,
		                                               sort_order,
		                                               data, data_tmp,
		                                               lengths_s, lengths_t,
		                                               char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		tmp[dest] = strings[s_idx];
		lengths_tmp[dest] = lengths_strings[s_idx];
		if ( data )
		{
			data_tmp[dest] = data[s_idx];
		}
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_strings;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_strings =
			sort_lexicographically_radix_insertion( tmp + first, bin_count, strings + first,
			                                        sort_order,
			                                        data_tmp + first, data + first,
			                                        lengths_strings + first, lengths_tmp + first,
			                                        char_idx+1 );
		} else
		{
			res_is_in_strings = false;
			sort_lexicographically_insertion( tmp + first, bin_count, data_tmp + first, sort_order );
		}
		if ( res_is_in_strings )
		{
			memcpy( tmp + first, strings + first, bin_count * sizeof( tmp[0] ) );
			memcpy( lengths_tmp + first, lengths_strings + first,
			        bin_count * sizeof( lengths_tmp[0] ) );
			if ( data )
			{
				memcpy( data_tmp + first, data + first,
				        bin_count * sizeof( data[0] ) );
			}
		}
	}

	solution_is_in_tmp = false;
	memcpy( strings, tmp, count * sizeof( tmp[0] ) );
	if ( data )
	{
		memcpy( data, data_tmp, count * sizeof( data[0] ) );
	}
	return solution_is_in_tmp;
}

internal
bool
sort_lexicographically_radix_insertion_ids_only( char const*const* strings, u32 count,
                                                 Sort_Order sort_order,
                                                 u32 * ids,
                                                 u32 * ids_tmp,
                                                 u32 const * lengths_strings,
                                                 u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only( strings, count, ids, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion_ids_only( strings, count,
		                                                        sort_order,
		                                                        ids, ids_tmp,
		                                                        lengths_s,
		                                                        char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		ids_tmp[dest] = s_idx;
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_tmp;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_tmp =
			!sort_lexicographically_radix_insertion_ids_only(
			   strings + first, bin_count,
			   sort_order,
			   ids_tmp + first, ids + first,
			   lengths_strings + first,
			   char_idx+1 );
		} else
		{
			res_is_in_tmp = true;
			sort_lexicographically_insertion_ids_only( strings + first, bin_count, ids_tmp + first, sort_order );
		}
		if ( !res_is_in_tmp )
		{
			memcpy( ids_tmp + first, ids + first,
			        bin_count * sizeof( ids[0] ) );
		}
	}

	solution_is_in_tmp = false;
	memcpy( ids, ids_tmp, count * sizeof( ids[0] ) );
	return solution_is_in_tmp;
}


internal
bool
sort_lexicographically_radix_insertion_ids_only_16( char const*const* strings, u32 count,
                                                    Sort_Order sort_order,
                                                    u16 * ids,
                                                    u16 * ids_tmp,
                                                    u32 const * lengths_strings,
                                                    u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only_16( strings, count, ids, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion_ids_only_16( strings, count,
		                                                           sort_order,
		                                                           ids, ids_tmp,
		                                                           lengths_s,
		                                                           char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		ids_tmp[dest] = s_idx;
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_tmp;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_tmp =
			!sort_lexicographically_radix_insertion_ids_only_16(
			   strings + first, bin_count,
			   sort_order,
			   ids_tmp + first, ids + first,
			   lengths_strings + first,
			   char_idx+1 );
		} else
		{
			res_is_in_tmp = true;
			sort_lexicographically_insertion_ids_only_16( strings + first, bin_count, ids_tmp + first, sort_order );
		}
		if ( !res_is_in_tmp )
		{
			memcpy( ids_tmp + first, ids + first,
			        bin_count * sizeof( ids[0] ) );
		}
	}

	solution_is_in_tmp = false;
	memcpy( ids, ids_tmp, count * sizeof( ids[0] ) );
	return solution_is_in_tmp;
}

internal
void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp,
                        Sort_Order sort_order,
                        u32 * data,
                        u32 * data_tmp,
                        u32 * lengths_strings,
                        u32 * lengths_tmp )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
	}
	char const * tmp_local[tmp?0:count];
	if ( tmp == NULL)
	{
		tmp = tmp_local;
	}
	sort_lexicographically_radix_insertion( strings, count, tmp, sort_order,
	                                        data, data_tmp,
	                                        lengths_strings, lengths_tmp );
}

internal
void
sort_lexicographically_ids_only( char const*const* strings, u32 count,
                                 Sort_Order sort_order,
                                 u32 * ids,
                                 u32 * ids_tmp,
                                 u32 * lengths_strings )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only( strings, count, ids, sort_order );
	}
	u32 ids_tmp_local[ids_tmp?0:count];
	if ( ids_tmp == NULL)
	{
		ids_tmp = ids_tmp_local;
	}
	sort_lexicographically_radix_insertion_ids_only( strings, count, sort_order,
	                                                 ids, ids_tmp,
	                                                 lengths_strings );
}

internal
void
sort_lexicographically_ids_only_16( char const*const* strings, u32 count,
                                    Sort_Order sort_order,
                                    u16 * ids,
                                    u16 * ids_tmp,
                                    u32 * lengths_strings )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only_16( strings, count, ids, sort_order );
	}
	u16 ids_tmp_local[ids_tmp?0:count];
	if ( ids_tmp == NULL)
	{
		ids_tmp = ids_tmp_local;
	}
	sort_lexicographically_radix_insertion_ids_only_16( strings, count, sort_order,
	                                                    ids, ids_tmp,
	                                                    lengths_strings );
}
