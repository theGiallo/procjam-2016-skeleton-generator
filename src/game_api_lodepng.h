#ifndef GAME_API_LODEPNG_H
#define GAME_API_LODEPNG_H 1

//#define LODEPNG_NO_COMPILE_DISK
//#define LODEPNG_NO_COMPILE_ALLOCATORS

#include <lodepng.h>

#if !WE_LOAD_OUR_GL
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>
#else
#define OUR_GL_EXPORT_POINTERS 1
#include "our_gl.h"
#endif

#include "game_api_lepre.h"
#include "game_api.h"

extern
bool
load_png( System_API * sys_api, const char * file_path,
          Mem_Stack * tmp_mem, Mem_Stack * mem,
          u8 ** out_blob, u32 * width, u32 * height );

extern
void
flip_RGBA8_image_vertically_and_premultiply_alpha( u8 * image,
                                                   s32 width, s32 height,
                                                   bool sRGB );

#endif /* ifndef GAME_API_LODEPNG_H */
