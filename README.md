# ProcJam 2016 - Skeleton Generator
## The idea
Some skeletons are presented to the user, they are all of the same species. She can discard some and then make them mate, generating new skeletons. A genetic algorithm is applied to the DNA of those skeletons to crossover and mutate the genes. The DNA is not a classic representation of the skeleton, but it is a series of instructions, like a byte-code.

## Implementing the idea
### The Skeleton
The skeleton is represented as a tree of bones. The branching is an articulation and is expressed as a length percentage of the attachment point, a rotation (Euler representation) and a bone. Yes, an articulation can be in the middle of a bone, but the "child" has there one of its extremes.
### The DNA
The DNA is practically a byte-code of a virtual machine.
The bytecode is divided in blocks. Instructions are represented by an instruction code followed by 3 other blocks, used as arguments where needed. This 4 blocks are called "instruction group". This makes the byte-code instructions of fixed length. This is to make cuts easy and to reduce illegal cases. The blocks are 64bit long, thus an instruction group is 4 blocks, or 256bits. Blocks are encoded as signed or unsigned integers. In some cases they are decoded as 32bit floating point values between 0 and 1. This is because genetic algorithm work better with fixed point representations and because I wanted all possible values to make sense and be valid.

#### The core instructions
These are the core instructions, keys to the building of the skeleton:

- `ADD_BONE`
- `ADD_LENGTH`
- `MOVE_UP	`
- `ADD_ROTATION`
- `SUB_ROTATION`

The skeleton is generated starting from a single articulation with a bone of length 0.1 in it, aligned with the Z axis. A stack of bones, articulations and execution state is kept during the DNA execution. At each `ADD_BONE` instruction the stack is incremented. Each execution state consist of a DNA, an execution point and 3 registers. The `ADD_BONE` instruction has 2 arguments, indicating a starting and an ending point in the current DNA, that will be used as the DNA of the next stack layer. The registers are copied to the next layer. DNA is never modified, only registers are modified. When a DNA execution gets to its end the stack is popped. When the stack is empty the skeleton is complete.

#### The advanced instructions
After experimenting with the core instructions I added 3 registers to the "VM" and conditionals based on them. This was aimed to make iterations possible. Before this a lot of cases where infinite loops, but I wanted controlled loops, so I needed values to modify and to check. I implemented conditionals not as jumps but as ifs regulating the next instruction execution,.to minimize illegal cases after cuts and DNA modifications.
There are instructions to add/subtract values from the registers, to multiply/divide them by values, to assign registers to other registers. I have not yet implemented instructions to use value from registers to do core operations because I want to have and idea of the possibilities of this instructions set.

### Current status
Now a random DNA is generated at start-up and at the release of `R`. It is simulated every frame and bones are rendered as segments.
It can happen that the generated DNA is bad, that means it generated too many bones and articulations.

To navigate the space there is an FPV-WASD camera, which mouse-look is activated by `RETURN`. `Q` and `E` rotate the camera and `SPACE` and `C` move it up and down. 

# Hot code reloading test
## What?
An implementation of hot code reloading like the [Handmade Hero's one](https://hero.handmadedev.org/videos/win32-platform/day021.html).

## How?
The key part is the separation between platform and game layers.
The platform layer has the main, and constitutes the executable.
It loads the game functions from a dynamic library at start-up and whenever it detects the library has been modified.

The whole game memory resides in the platform layer and is passed by reference to the game functions. Thus data structure changes won't work. The system layer never writes or reads from the game memory. It only bulk copies and restores it for debug purposes. The system layer calls the game functions, passing all the needed data. This is the only relationship it has with the game layer.

The system layer passes input events to the game layer, and calls update and render functions. It is up to the game layer to implement the game loop iteration.

A system API is passed to the game functions in the form of a struct with function pointers, declared in the generic system header. In this way the game layer can be system agnostic.

## Debug Functionalities Controls
Controls are listed into [CONTROLS.md](./CONTROLS.md).
