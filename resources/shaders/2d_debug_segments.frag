#version 330 core
layout(row_major) uniform;

in  vec4 _color;

out vec4 out_color;

void main( void )
{
	out_color = _color;
}