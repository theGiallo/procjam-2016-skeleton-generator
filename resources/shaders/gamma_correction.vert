#version 330 core
layout(row_major) uniform;

in  vec2 vertex;
in  vec2 uv;

out vec2 _uv;

void main( void )
{

	gl_Position = vec4( vertex.xy, 0, 1 );

	_uv = uv;
}