#version 330 core
layout(row_major) uniform;

in  vec2 vertex;
in  vec2 uv;
struct Quad_Data
{
	vec2 pos;
	vec2 size;
	float rot;
	uint color;
	uint texture_id;
	// NOTE(theGiallo): these 2 are to set the quad UV coordinates from the [0,1]
	// NOTE(theGiallo): the resulting UV coordinates are in the limit space, not
	// in the entire texture space
	vec2 uv_offset;
	vec2 uv_scale;
	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	vec2 uv_origin;
	vec2 uv_limits;
	uint texture_overflow_x;
	uint texture_overflow_y;
	uint texture_min_filter;
	uint texture_mag_filter;
};

in  Quad_Data quad_data;

struct
Fragment_Tex_Data
{
	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	vec2 uv_origin;
	vec2 uv_limits;
	ivec2 overflow_policies;
	uint min_filter;
	uint mag_filter;
};

flat out vec4 color;
flat out uint texture_id;
flat out Fragment_Tex_Data tex_data;

out vec2 _uv;

uniform mat3 VP;

vec2
rotate( vec2 v, float rad )
{
	mat2 rotation = mat2(
	     vec2( cos( rad ),  -sin( rad ) ),
	     vec2( sin( rad ),   cos( rad ) )
	 );

	return v * rotation;
}

void main( void )
{
	// vec3 p3 = vec3( vertex * quad_data.size + quad_data.pos, 1.0) * VP;
	vec3 p3 = vec3( rotate( vertex * quad_data.size, radians( quad_data.rot ) ) + quad_data.pos, 1.0) * VP;
	// _pos = p3.xy / p3.z;
	// _pos = p3.xy;

	gl_Position = vec4( p3.xy, 0.0, 1.0 );

	_uv = quad_data.uv_offset + uv * quad_data.uv_scale;
	// _uv = uv;

	texture_id = quad_data.texture_id;

	color.r = float( ( quad_data.color >> 0  ) & uint(0xff) ) / 255.0f;
	color.g = float( ( quad_data.color >> 8  ) & uint(0xff) ) / 255.0f;
	color.b = float( ( quad_data.color >> 16 ) & uint(0xff) ) / 255.0f;
	color.a = float( ( quad_data.color >> 24 ) & uint(0xff) ) / 255.0f;

	color.rgb = pow( color.rgb, vec3(2.2,2.2,2.2) ) * color.a; // NOTE: premultiply alpha

	tex_data.uv_origin = quad_data.uv_origin;
	tex_data.uv_limits = quad_data.uv_limits;
	tex_data.overflow_policies =
	ivec2( quad_data.texture_overflow_x, quad_data.texture_overflow_y );
	tex_data.min_filter = quad_data.texture_min_filter;
	tex_data.mag_filter = quad_data.texture_mag_filter;
}
