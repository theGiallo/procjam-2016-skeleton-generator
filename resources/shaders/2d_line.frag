#version 330 core
layout(row_major) uniform;

in  vec4 _color;

out vec4 out_color;

void main( void )
{
	out_color = _color;
	// out_color = mix( _color, vec4(0.0,1.0,0.0,1.0), 0.9999999 );
}