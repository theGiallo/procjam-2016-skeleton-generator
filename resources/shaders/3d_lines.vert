#version 330 core
layout(row_major) uniform;

uniform mat4 VP;
uniform mat4 V;
uniform mat4 P;

in  vec3 vertex;
in  vec3 vertex_color;

out vec3 v_color;
out float z;
out float depth;
out float meters_depth;
out float vid;
flat out vec3 sun_pos_v;
flat out vec3 sun_dir_v;
out vec3 view_pos;
out vec3 world_pos;

void
main()
{
	//gl_Position = vec4(vertex, 1.0) * VP;
	world_pos = vertex;
	vec3 camera_pos = -vec3( V[0][3], V[1][3], V[2][3] );
	vec4 view_pos4 = vec4(vertex, 1.0) * V;
	view_pos = ( view_pos4 / view_pos4.w ).xyz;
	gl_Position = vec4( vertex, 1.0 ) * VP;

	//gl_Position /= gl_Position.w;
	depth = gl_Position.z / gl_Position.w;
	meters_depth = -view_pos.z;
	z = vertex.z;
	vid = gl_VertexID;

	vec3 sun_pos_w = vec3( 0.0, 1.0, 0.5 );
	vec4 spv = vec4( sun_pos_w, 0.0 ) * V;
	sun_pos_v = spv.xyz;
	sun_dir_v = normalize( -sun_pos_v );

	v_color = vertex_color;
}
