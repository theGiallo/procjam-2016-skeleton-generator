#version 330 core
layout(row_major) uniform;

struct
Fragment_Tex_Data
{
	// NOTE(theGiallo): these 2 sets the rectangle in which to limit the sampling
	// and the overflow behavior. This defines the uv space: this rectangle is
	// considered [0,1]
	vec2 uv_origin;
	vec2 uv_limits;
	ivec2 overflow_policies;
	uint min_filter;
	uint mag_filter;
};

flat in vec4 color;
flat in uint texture_id;
flat in Fragment_Tex_Data tex_data;

in vec2 _uv;

#define NUM_TEXTURES 64
uniform mat4 VP;
uniform sampler2D textures[NUM_TEXTURES];

out vec4 out_color;

// NOTE(theGiallo): texture overflow
#define LEPRE_CLAMP_TO_EDGE   0x0u
#define LEPRE_REPEAT          0x1u
#define LEPRE_MIRRORED_REPEAT 0x2u

// NOTE(theGiallo): texture filtering
#define LEPRE_NEAREST             0x0u
#define LEPRE_BILINEAR            0x1u
#define LEPRE_MIPMAP_NEAREST      0x2u
#define LEPRE_MIPMAP_BILINEAR     0x3u
#define LEPRE_MIPMAP_LINEAR       0x4u
#define LEPRE_MIPMAP_TRILINEAR    0x5u

// NOTE(theGiallo): retrieved empirically
#define MIPMAP_EPSYLON 0.005
float
mip_map_level( sampler2D sampler, vec2 full_uv )
{
	vec2 texture_coordinate = full_uv * textureSize( sampler, 0 );
	vec2 dx_vtc = dFdx( texture_coordinate );
	vec2 dy_vtc = dFdy( texture_coordinate );
	float delta_max_sqr = max( dot( dx_vtc, dx_vtc ), dot( dy_vtc, dy_vtc ) );
	float mml = 0.5 * log2( delta_max_sqr );
	if ( abs( mml ) < MIPMAP_EPSYLON )
	{
		mml = 0;
	}
	return mml;
}

// NOTE(theGiallo): start and end are inclusive
int
overflow_repeat( int v, int start, int end )
{
	return start + int( mod( v - start, end - start + 1 ) );
}

// NOTE(theGiallo): start and end are inclusive
int
overflow_repeat_mirrored( int v, int start, int end )
{
	int span = end - start + 1;
	int val = v - start;
	int f = int( floor( float( val ) / float( span ) ) );

	// NOTE(theGiallo): it's mod( val, span )
	int m = val - span * f;

	if ( f % 2 == 0 )
	{
		return start + m;
	} else
	{
		return start + span - m - 1;
	}
}

// NOTE(theGiallo): min_v and max_v are inclusive
int
manage_overflow( int v, uint policy, int min_v, int max_v )
{
	if ( policy == LEPRE_CLAMP_TO_EDGE )
	{
		v = clamp( v, min_v, max_v );
	} else
	if ( policy == LEPRE_REPEAT )
	{
		v = overflow_repeat( v, min_v, max_v );
	} else
	if ( policy == LEPRE_MIRRORED_REPEAT )
	{
		v = overflow_repeat_mirrored( v, min_v, max_v );
	}
	#if 1
	else
	{
		v = 0;
	}
	#endif

	return v;
}

ivec2
manage_overflow( ivec2 v, ivec2 overflow_policies, ivec2 min_v, ivec2 max_v )
{
	v.x = manage_overflow( v.x, uint( overflow_policies.x ), min_v.x, max_v.x );
	v.y = manage_overflow( v.y, uint( overflow_policies.y ), min_v.y, max_v.y );
	return v;
}

ivec2
texel_coords( ivec2 tex_size, vec2 full_uv,
              vec2 uv_origin, vec2 uv_span, ivec2 overflow_policies )
{
	ivec2 bl_limit = ivec2( tex_size * uv_origin );
	// NOTE(theGiallo): start and end are inclusive, so max gets a -1
	ivec2 tr_limit = ivec2( tex_size * ( uv_origin + uv_span ) ) - 1;
	// NOTE(theGiallo): it was 'floor' originally
	// ivec2 texel_coords = ivec2( floor( full_uv * tex_size ) );
	ivec2 texel_coords = ivec2( floor( full_uv * tex_size ) );
	texel_coords = manage_overflow( texel_coords, overflow_policies,
	                                bl_limit, tr_limit );
	return texel_coords;
}

vec4
texture_bilinear( sampler2D sampler, int lod, vec2 full_uv )
{
	ivec2 tex_size = textureSize( sampler, lod );

	vec2 tx_size_in_uv = 1.0 / vec2(tex_size);
	vec2 h_tx_size_in_uv = tx_size_in_uv * 0.5;
	vec2 intra_f_uv = mod( full_uv - h_tx_size_in_uv, tx_size_in_uv );

	vec2 f_uv = full_uv - h_tx_size_in_uv;
	ivec2 tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_bl = texelFetch( textures[texture_id], tex_coords, lod );

	f_uv = full_uv + h_tx_size_in_uv;
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_tr = texelFetch( textures[texture_id], tex_coords, lod );

	f_uv = full_uv + vec2( -h_tx_size_in_uv.x, h_tx_size_in_uv.y );
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_tl = texelFetch( textures[texture_id], tex_coords, lod );

	f_uv = full_uv + vec2( h_tx_size_in_uv.x, -h_tx_size_in_uv.y );
	tex_coords =
	texel_coords( tex_size,
	              f_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 tc_br = texelFetch( textures[texture_id], tex_coords, lod );

	vec2 alpha = intra_f_uv / tx_size_in_uv;
	vec4 c = mix( mix( tc_bl, tc_br, alpha.x ),
	              mix( tc_tl, tc_tr, alpha.x ), alpha.y );
	c = mix(
	         mix( tc_bl, tc_br, alpha.x ),
	         mix( tc_tl, tc_tr, alpha.x ), alpha.y );
	return c;
}

vec4
texture_nearest( sampler2D sampler, int lod, vec2 full_uv )
{
	ivec2 tex_size = textureSize( textures[texture_id], lod );
	ivec2 tex_coords =
	texel_coords( tex_size,
	              full_uv, tex_data.uv_origin,
	              tex_data.uv_limits, tex_data.overflow_policies );
	vec4 c = texelFetch( textures[texture_id], tex_coords, lod );
	return c;
}


void main( void )
{
	vec4 c = color;
	if ( texture_id == uint(0xFFFFFFFF) )
	{
		out_color = c;
	} else
	{
		float mipmap_lvl_f =
		   mip_map_level( textures[texture_id],
		                  _uv * tex_data.uv_limits
		                  /* + tex_data.uv_origin */ );

		vec2 full_uv = _uv * tex_data.uv_limits + tex_data.uv_origin;

		vec4 tc = vec4(0);
		if ( mipmap_lvl_f > 0 &&
		     tex_data.min_filter == LEPRE_MIPMAP_NEAREST )
		{
			int mipmap_lvl = int( round( mipmap_lvl_f ) );
			tc = texture_nearest( textures[texture_id], mipmap_lvl,
			                      full_uv );
		} else
		if ( mipmap_lvl_f > 0 &&
		     tex_data.min_filter == LEPRE_MIPMAP_BILINEAR )
		{
			int mipmap_lvl = int( round( mipmap_lvl_f ) );
			tc = texture_bilinear( textures[texture_id], mipmap_lvl,
			                       full_uv );
		} else
		if ( mipmap_lvl_f > 0 &&
		     tex_data.min_filter == LEPRE_MIPMAP_LINEAR )
		{
			int mipmap_lvl = int( floor( mipmap_lvl_f ) );
			vec4 tc0 = texture_nearest(  textures[texture_id],
			                             mipmap_lvl, full_uv );

			mipmap_lvl = int( ceil( mipmap_lvl_f ) );
			vec4 tc1 = texture_nearest( textures[texture_id],
			                            mipmap_lvl, full_uv );

			tc = mix( tc0, tc1, fract( mipmap_lvl_f ) );
		} else
		if ( mipmap_lvl_f > 0 &&
		     tex_data.min_filter == LEPRE_MIPMAP_TRILINEAR )
		{
			int mipmap_lvl = int( floor( mipmap_lvl_f ) );
			vec4 tc0 = texture_bilinear( textures[texture_id],
			                             mipmap_lvl, full_uv );

			mipmap_lvl = int( ceil( mipmap_lvl_f ) );
			vec4 tc1 = texture_bilinear( textures[texture_id],
			                             mipmap_lvl, full_uv );

			tc = mix( tc0, tc1, fract( mipmap_lvl_f ) );
		} else
		if ( ( mipmap_lvl_f > 0 &&
		       tex_data.min_filter == LEPRE_BILINEAR ) ||
		     ( mipmap_lvl_f <= 0 &&
		       tex_data.mag_filter == LEPRE_BILINEAR )
		   )
		{
			tc = texture_bilinear( textures[texture_id], 0, full_uv );
		} else
		if ( mipmap_lvl_f == 0 ||
		     ( mipmap_lvl_f > 0 &&
		       tex_data.min_filter == LEPRE_NEAREST ) ||
		     ( mipmap_lvl_f <= 0 &&
		       tex_data.mag_filter == LEPRE_NEAREST )
		   )
		{
			tc = texture_nearest( textures[texture_id], 0, full_uv );
		} else
		{
			tc = vec4(0,1,1,1);
		}

		out_color = tc * c;
	}
}
